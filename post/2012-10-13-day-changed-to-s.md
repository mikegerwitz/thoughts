# "Day changed to S"

Whatever "S" may be (in this case, "13 Oct 2012"), there is always a sense
of peace and gratification that comes with witnessing that line appear in any
type of log; it shows a dedication to an art, should your days contain daylight.

<!-- more -->

