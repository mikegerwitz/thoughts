# EFF Elaborates On DCMA Ruling

In addition to my aforementioned links, the EFF has provided [a more detailed
analysis][0] of the decision.

[0]: https://www.eff.org/deeplinks/2012/11/2012-dmca-rulemaking-what-we-got-what-we-didnt-and-how-to-improve

<!-- more -->
