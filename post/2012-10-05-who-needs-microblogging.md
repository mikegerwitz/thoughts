# Who needs "microblogging"?

I don't. This is just some place safe to store random thoughts that people
probably don't care about (like most comments on most social networking
services), with the added benefit of distributed backup, a simple system and no
character limit.

<!-- more -->

All the thoughts are commit messages; in particular, this means no versioning.
That's okay, because I'm not going to go back and modify them, but I do want
dates and I do want GPG signatures (to show that it's actually me thinking this
crap).

This isn't a journal.

This will mostly be a hacker's thought cesspool.

This isn't a blog.

Though, considering how much I ramble (look at this message), certain thoughts
could certainly seem like blog entries. Don't get the two confused---one
requires only thought defecation and the other endures the disturbing task of
arranging the thought matter into something coherent and useful to present to
others.

Yeah. Enjoy. Or don't. You probably shouldn't, even if you do. If you don't,
you probably should just to see that you shouldn't.

