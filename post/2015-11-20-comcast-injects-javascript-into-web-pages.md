# Comcast injects JavaScript into web pages

It seems that Comcast has decided that it is a good idea to [inject
JavaScript into web pages][js] visited by its customers in order to inform
them of Copyright violations.

[js]: https://gist.github.com/Jarred-Sumner/90362639f96807b8315b

This is a huge violation of user privacy and trust.  Further, it shows that
an ISP (and probably others) feel that they have the authority to dictate
what is served to the user on a free (as in speech) Internet.  Why should we
believe that they won't start injecting other types of scripts that spy on
the user or introduce advertising?  What if a malicious actor compromises
Comcast's servers and serves exploits to users?

It is no surprise that Comcast is capable of doing this---they know the IP
address of the customer, so they are able to intercept traffic and alter it
in transit.  But the fact that they _can_ do this demonstrates something far
more important: _that they have spent the money on the infrastructure to do
so_!

<!-- more -->

Comcast isn't the only ISP to have betrayed users by injecting data.  One
year ago, it was discovered that [Verizon was injecting "perma-cookies" into
requests to track users][verizon].  This is only one example of the
insidious abuses that unchecked ISPs can take.

So what can you do to protect yourself?

What Comcast is doing is called a [man-in-the-middle (MITM) attack][mitm]:
Comcast sits in the middle of you and your connection to the website that
you are visiting, proxying your request.  Before relaying the website's
response to you, it modifies it.

In order to do this, Comcast needs to be able to read your communications,
and must be able to modify them: the request must be read in order to
determine how the JavaScript should be injected and what request it should
be injected into; and it must be modified to perform the injection.  It
cannot (given a properly configured web server) do so if your connection is
encrypted.  In the case of web traffic, `https` URLs with the little lock
icon in your web browser generally indicates that your communications are
encrypted, making MITM attacks
unlikely.

(We're assuming that Comcast won't ask you to install a root CA so that they
can decrypt your traffic!  But that would certainly be noticed, if they did
so on a large enough scale.)

Not all websites use SSL.  Another method is to use encrypted proxies, VPNs,
or services like like [Tor][tor].  This way, Comcast will not be able to
read or modify the communications.

See also: [HackerNews discussion][hn]; [original Reddit discussion][reddit].

[verizon]: https://www.eff.org/deeplinks/2014/11/verizon-x-uidh
[mitm]: https://en.wikipedia.org/wiki/Man-in-the-middle_attack
[hn]: https://news.ycombinator.com/item?id=10592775
[reddit]: https://www.reddit.com/r/HuntsvilleAlabama/comments/35v4sn/comcast_is_injecting_bad_javascript_to_your/
[tor]: https://tor.org/
