# U.S. House Passes CISPA

Two days ago---on the 18th--[the U.S. House of Representatives decided to pass
CISPA 288-127][0].

> The legislation passed 288-127, despite a veto threat from Pres. Barack Obama,
> who expressed serious concerns about the danger CISPA poses to civil
> liberties.

[0]: https://www.eff.org/deeplinks/2013/04/us-house-representatives-shamefully-passes-cispa-internet-freedom-advocates

<!-- more -->

As the bill moves into the senate, [civil liberties groups will continue to
oppose it][1]; I personally hope that you will do the same.

Move [information on CISPA][2] is available on the EFF's website.

[1]: https://www.eff.org/deeplinks/2012/04/voices-against-cispa
[2]: https://www.eff.org/cybersecurity-bill-faq
