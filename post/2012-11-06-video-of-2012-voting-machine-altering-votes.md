# Video of 2012 Voting Machine Altering Votes

A Reddit user [posted video of a 2012 voting machine preventing him from
selecting Barak Obama][0]. Malfunction or not, this is the type of thing that
could have possibly been caught if the software were free. Furthermore, from
reading the source code, one would be able to clearly tell whether or not it was
a bug or an intentional "feature".

[0]: http://thenextweb.com/shareables/2012/11/06/reddit-user-captures-video-of-2012-voting-machines-altering-votes/

<!-- more -->
