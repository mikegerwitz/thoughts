# Webmasters: Please, Don't Block Tor

[Tor][] is a privacy and anonymity tool that [helps users to defend
  themselves][tor-about] against traffic analysis online.
Some people, like me, use it as an important tool to help defend against
  [various online threats to privacy][sapsf].
[Others use it][tor-users] to avoid censorship,
  perhaps by the country in which they live.
Others use it because their lives depend on it---they
  may live under an oppressive regime that forbids access to certain
  information or means of communication.

[Tor]: https://www.torproject.org/
[tor-about]: https://www.torproject.org/about/overview.html.en#whyweneedtor
[tor-users]: https://www.torproject.org/about/torusers.html.en
[sapsf]: /talks/sapsf

Unfortunately, some people also hide behind Tor to do bad things,
  like attack websites or commit fraud.
Because of this,
  many website owners and network administrators see Tor as a security threat,
    and choose to block Tor users from accessing their website.

<!-- more -->

But in doing so,
  you aren't just keeping out some of the malicious users:
    you're also keeping out those who [use Tor for important, legitimate
    reasons][tor-users].
Malicious users have other means to achieve anonymity and often have the
  skill and understanding to do so.
But average Tor users aren't necessarily technology experts,
  and certainly don't have the extra (often maliciously-acquired) resources
  that bad actors do,
    so they are disprortionally affected by blocks.

A particularly unsettling problem I often encounter is that a website will
  outright prohibit access by Tor users _even on read-only resources like
  articles or information_.
I've even seen this on informational resources on United States Government
  domains!
Blocking access to interactive website features---like
  posting comments or making purchases---can
  be understandable,
    or maybe even necessary sometimes.
For example,
  Wikipedia prohibits page edits over Tor.
But Wikipedia _does not block reading_ over Tor.

If you are considering threats that may mask themselves behind Tor and you
  are running a blog, news site, or other informational resource,
    please, consider how your actions [may affect innocent
    users][tor-users].
Allow users to read over Tor,
  even if you decide to prohibit them from interacting.

For users of Tor who do find themselves stuck from time to time:
  I will often prepend `https://web.achive.org/` to the URL of a page that
  is blocked,
    which allows me to view the page in the Internet Archive's [Wayback
    Machine][].
For example,
  to view my website in the Wayback Machine,
    you'd visit `https://web.archive.org/https://mikegerwitz.com/`.

[Wayback Machine]: https://web.archive.org/
