# LibrePlanet 2019 will be March 23--24 in Boston, MA

It's already time to start thinking about LibrePlanet 2019, which will be
March 23--24 in the Greater Boston Area in MA:

<https://libreplanet.org/2019/>

This is the one event that I must make it to each year, and I encourage
everyone to attend and see the faces of many that are at the heart of the
free software community.

<!-- more -->

Consider [submitting a session][submit]! Or, if you can't make it but plan
on watching online, maybe help someone else attend by [contributing to the
travel fund][travel-fund]. The call for sessions ends October 26th.

I'll be attending again this year, and I plan on submitting a session
proposal.  I won't have the time to do [my 100+hr research talks like the
past couple years][talks], so maybe I'll fall back on something more
technical that I won't have to research.

It's still a ways off, but if you do plan on attending, do let me know so I
can say hello!

[submit]: https://my.fsf.org/lp-call-for-sessions
[travel-fund]: https://my.fsf.org/civicrm/contribute/transact?reset=1&id=60
[talks]: /talks/
