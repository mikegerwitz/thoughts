# Privacy In Light of the Petraeus Scandal

I'm not usually one for scandals (in fact, I couldn't care less who government
employees are sleeping with). However, it did bring up deep privacy
concerns---how exactly did the government get a hold of the e-mails?

The [EFF had released an article answering some questions][0] about the scandal,
which is worth a read. In particular, you should take a look at the [EFF's
Surveillance Self-Defense website][1] for an in-depth summary of the laws
surrounding government surveillance and tips on how to protect against it.

[0]: https://www.eff.org/deeplinks/2012/11/when-will-our-email-betray-us-email-privacy-primer-light-petraeus-saga
[1]: https://ssd.eff.org

I'd like to touch upon a couple things. In particular, [the article mentions][0]:

<!-- more -->

> Broadwell apparently accessed the emails from hotels and other locations, not
> her home.  So the FBI cross-referenced the IP addresses of these Wi-Fi
> hotspots "against guest lists from other cities and hotels, looking for common
> names."

To stay anonymous in this situation, one should [consider using Tor][2] to mask
his/her IP address. Additionally, remove all cookies (or use your browser's
privacy mode if it will disable storing and sending of cookies for you) and
consider that your User Agent may be used to identify you, especially if
maleware has inserted its own unique identifiers.

Also according to [the EFF article][0]:

> According to reports, Patraeus and Broadwell adopted a technique of drafting
> emails, and reading them in the draft folder rather than sending them.

That didn't work out so well. Consider [encrypting important communications][3]
using GPG/PGP so that (a) the e-mail cannot be deciphered in transit and (b) the
e-mail can only be read by the intended recipient. Of course, you are then at
risk of being asked to divulge your password, so to avoid the situation
entirely, it would be best to delete the e-mails after reading them.
Additionally, if you host your own services, it may be wise to host your own
e-mail (guides for doing this vary between operating system, but consider
looking at software like [Postfix][4] for mail delivery and maybe [Dovecot][5]
for retrieval).

Privacy isn't only for those individuals who are trying to be sneaky or cheat on
their spouses. Feel free joining the EFF in trying to reform the ECPA to respect
our privacy in this modern era; storing a document digitally shouldn't change
its fundamental properties under the law.

I'd also encourage you to read [Schneier's post on this topic][6], which
summarizes points from many articles that I did not cover here.

[2]: https://ssd.eff.org/tech/tor
[3]: https://ssd.eff.org/tech/encryption
[4]: http://www.postfix.org
[5]: http://www.dovecot.org/
[6]: http://www.schneier.com/blog/archives/2012/11/e-mail_security.html

