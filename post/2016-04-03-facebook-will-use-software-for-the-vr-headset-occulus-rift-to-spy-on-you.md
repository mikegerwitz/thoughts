# Facebook will use software for the VR headset Occulus Rift to spy on you

Anything coming out of Facebook should be [cause for concern][rms-fb].  So,
naturally, one might be concerned when they decide to get into the virtual
reality (VR) scene by [purchasing the startup Occulus VR][fb-vr], makers of
the Occulus Rift VR headset.  One can only imagine all the fun ways Facebook
will be able to track, manipulate, spy on, and otherwise screw over users
while they are immersed in a virtual reality.

[rms-fb]: https://stallman.org/facebook.html#privacy
[fb-vr]: http://www.theguardian.com/technology/2014/jul/22/facebook-oculus-rift-acquisition-virtual-reality

Sure enough, we have our first peak: [the software that Facebook has you
install for the Occulus Rift is spyware][fb-spy], reporting on what
*unrelated* software you use on your system, your location (including GPS
data and nearby Wifi networks), the type of device you're using, unique
device identifiers, your movements while using the VR headset, and more.

[fb-spy]: http://uploadvr.com/facebook-oculus-privacy/

<!-- more -->

This is absurd.  Do not play into Facebook's games through temptation of
cool new technology; reject their terms and see if there's other ways you
can use the headset without their proprietary spyware.  If not, perhaps you
should ask for a refund, and tell them why.

