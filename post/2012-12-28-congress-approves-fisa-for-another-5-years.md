# Congress Approves FISA For Another 5 Years

At a [vote of 73-23][0], Congress has voted to [extend FISA warentless spying
bill by five more years[1], even shooting down [proposed amendments][2] to the
bill.[3]

[0]: https://www.senate.gov/legislative/LIS/roll_call_lists/roll_call_vote_cfm.cfm?congress=112&session=2&vote=00236
[1]: https://www.eff.org/deeplinks/2012/12/congress-disgracefully-approves-fisa-warrantless-eavesdropping-bill-five-more
[2]: https://www.eff.org/deeplinks/2012/12/why-we-should-all-care-about-senates-vote-fisa-amendments-act-warrantless-domestic
[3]: http://arstechnica.com/tech-policy/2012/12/as-senate-votes-on-warrantless-wiretapping-opponents-offer-fixes/

<!-- more -->

Thank you to those senators that [opposed the bill][0]:

> Akaka (D-HI);
> Baucus (D-MT);
> Begich (D-AK);
> Bingaman (D-NM);
> Brown (D-OH);
> Cantwell (D-WA);
> Coons (D-DE);
> Durbin (D-IL);
> Franken (D-MN);
> Harkin (D-IA);
> Leahy (D-VT);
> Lee (R-UT);
> Menendez (D-NJ);
> Merkley (D-OR);
> Murkowski (R-AK);
> Murray (D-WA);
> Paul (R-KY);
> Sanders (I-VT);
> Schatz (D-HI);
> Tester (D-MT);
> Udall (D-CO);
> Udall (D-NM);
> Wyden (D-OR).

Unfortunately, the two senators from my own state cannot join that list.

The [EFF has sumarized the surveillance issues of 2012][4] recently on their
website.

[4]: https://www.eff.org/deeplinks/2012/12/2012-review-effs-fight-against-secret-surveillance-law

