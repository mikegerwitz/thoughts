# Self-Discovery Before the Internet

This is an autobiographical opinion piece prompted by [a HackerNews
post][hn] discussing what it was like to learn programming before Stack
Overflow (and other parts of the Internet).

[hn]: https://news.ycombinator.com/item?id=14339293

<!-- more -->

I'm not old.  I was born in 1989.  I started programming around 1999.  The
Internet sure did exist back then, but I was 10, and my parents weren't keen
on having me just go exploring.  Besides, it was dial-up---you couldn't go
search real quick; especially if someone was on the phone.  Using the
Internet was an _event_, and an exciting one at that, listening to those
dial tones, logging in using that old Prodigy dialog.  Back then you had
Dogpile and Ask Jeeves.  Most sites I'd visit by name; usually that was
GameFAQs or CNET download.com, because those are the sites my friend told me
about when he introduced me to the Internet.

I'm entirely self-taught.  I didn't know any programmers.  I didn't have
contact with any.  I told my parents that I wanted to learn how to program
and they skeptically brought me to Barnes and Noble where we picked out
Learn to Program with Visual Basic 6 by John Smiley (*gasp* yes I started as
a Windows programmer).  It came with a VB6 CD that for a while I was
convinced could only run the book examples, because I had no idea what I was
doing.  I struggled.  I tinkered.  Hacker culture was on the complete
opposite end of where I was, but by the time I discovered it years later, I
felt like I finally found myself---I finally discovered who I was.  The
struggle made me a hacker.

It's easy to half-ass it today.  It's easy to simply say "eh I can Google
it" and forego committing knowledge.  But it also makes it easy to gain
knowledge, for those who do care to do so.  It makes trivia easy.  It makes
discovery easy.  It also exposes people to subcultures quickly and
demands conformance to stereotypes and norms before one can discover
_themselves_.  Who would I be today without having to struggle for myself
rather than someone else _telling_ me who I am, and what I do?

This is more than just technical knowledge.  This is the difference between
dropping a child off in the wild or dropping them off at the local
scouts.  And at least scouts will discover themselves together.  With the
Internet, you absorb a body of existing knowledge; you _rediscover others_,
not yourself.  You often read blogs containing opinions of others, not books
or manuals.

That's not to say that you can't learn on your own.  Many still do.  Many
focus on manuals and books and source code rather than social media.  It's
sure hard, though, when everything is integrated as such.  Social media
can be beneficial---you do want communication and collaboration.  I sure as
hell want to communicate with others.  Opinions of others are deeply
important too.  Some of the best things I've read are on blogs, not in
books.  But I've already found my niche.  I've found myself.  I wasn't
tainted or manipulated---I learned in a world of proprietary software where
developing license systems was fun and emerged a free software
activist.  Because I was forced to look inward, not post on Stack Overflow
or HN or Reddit expecting a hand-guided tour or `dd` of thoughts (okay,
you're not getting that on HN).

Not everyone needs to be a passionate hacker or developer.  Really, the
world needs both.  And based on what I've seen being pumped out of schools
and universities, the self-taught are generally better off either way.  The
vast resources available to modern programmers make many tasks easier and
cheaper, though it also increases maintenance costs if all the programmer is
doing is using code snippets or concepts without actually grokking
them.  But this is what most of the world runs off of.

Let yourself struggle.  Go offline.  Sit down with a print book and get out
a pen and take notes in the margin, write out your ideas.  Getting syntax
errors in your editor or REPL?  Figure it out!  Or maybe consult the manual,
or the book you're reading.  Don't search for the solution.  When I learned
Algebra in middle school, I had little interest, and forgot all of
it.  Years later, I needed it as a foundation for other things.  I
discovered the rules for myself on pen and paper.  Not only do I remember it
now (or can rediscover on a whim), but I understand _why_ it works the way
it does.  I've had those epiphanies.  It's easy to miss the forest for the
trees when you don't gain that essential intuition to help yourself
out.  And the forest is vast and beautiful.

