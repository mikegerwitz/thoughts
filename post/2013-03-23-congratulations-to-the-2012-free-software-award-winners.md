# Congratulations to the 2012 Free Software Award Winners

Each year, the [Free Software Foundation][0] presents awards to individuals who
have made a [strong contribution to free software][1]:

[0]: http://fsf.org

> The Award for the Advancement of Free Software is given annually to an
> individual who has made a great contribution to the progress and development
> of free software, through activities that accord with the spirit of free
> software.

[1]: https://www.fsf.org/news/2012-free-software-award-winners-announced-2

<!-- more -->

This year, announced at the LibrePlanet 2013 conference, [the winner was Dr.
Fernando Perez][1]---creator of IPython. The winner of the Award for Projects of
Social Benefit was [OpenMRS][2], which is a free (as in freedom) medical records
system for developing countries.

[2]: http://openmrs.org/
