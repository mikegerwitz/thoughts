# Join me at LibrePlanet 2016 for my talk "Restore Online Freedom!"

I will be [speaking at LibrePlanet this year][lp2016] (2016) about freedom
on the Web.  Here's the session description:

[lp2016]: https://www.libreplanet.org/2016/program/

> Imagine a world where surveillance is the default and users must opt-in to
> privacy. Imagine that your every action is logged and analyzed to learn
> how you behave, what your interests are, and what you might do next.
> Imagine that, even on your fully free operating system, proprietary
> software is automatically downloaded and run not only without your
> consent, but often without your knowledge. In this world, even free
> software cannot be easily modified, shared, or replaced. In many cases,
> you might not even be in control of your own computing -- your actions and
> your data might be in control by a remote entity, and only they decide
> what you are and are not allowed to do.
>
> This may sound dystopian, but this is the world you're living in right
> now. The Web today is an increasingly hostile, freedom-denying place that
> propagates to nearly every aspect of the average users' lives -- from
> their PCs to their phones, to their TVs and beyond. But before we can
> stand up and demand back our freedoms, we must understand what we're being
> robbed of, how it's being done, and what can (or can't) be done to stop
> it.

<!-- more -->

There are a number of other [great sessions][lp2016] this year from a
[number of speakers][lp2016s], many well-known.  We also have an opening
keynote from Edward Snowden!

All [FSF associate members get free entry][fsfmember].  If you can't join
us, the conference will be streamed live.  You can also see [videos of past
talks][lpvideos] on the FSF's self-hosted [GNU MediaGoblin][goblin]
instance.

Special thanks to the FSF for covering a large portion of my travel
expenses; I otherwise might not have been able to attend.  Thank you to all
who donated to the conference scholarship fund.

[lp2016s]: https://www.libreplanet.org/2016/program/speakers.html
[fsfmember]: https://crm.fsf.org/join
[lpvideos]: https://media.libreplanet.org/
[goblin]: http://mediagoblin.org/
