# "Trademark" Bullying

There's two problems with this post from the EFF describing [The Village Voice
suing Yelp for "Best of" trademark infringement][0]: firstly, there's the
obvious observation that such a trademark should not have been permitted by the
USPTO to begin with. Secondly---why do entities insist on gaming the system in
such a terribly unethical manner? It takes a special breed of people to do such
a thing.

[0]: https://www.eff.org/deeplinks/2012/10/stupid-lawyer-tricks-and-government-officials-who-are-helping-them

<!-- more -->
