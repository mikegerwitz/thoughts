# Federal Judge Rules NSLs (National Security Letters) Unconstitutional

This news is huge and an incredible win for both the EFF and all U.S. citizens.
Today, [United States District Judge Susan Illston found the National Security
Letters' gag provisions unconstitutional][0] and---since the review procedures
violate the separation of powers and cannot be separated from the rest of the
statute---has consequently [ruled the NSLs themselves to be
unconstitutional][1]:

[0]: http://www.wired.com/threatlevel/2013/03/nsl-found-unconstitutional/
[1]: https://www.eff.org/press/releases/national-security-letters-are-unconstitutional-federal-judge-rules

> In today's ruling, the court held that the gag order provisions of the statute
> violate the First Amendment and that the review procedures violate separation
> of powers. Because those provisions were not separable from the rest of the
> statute, the court declared the entire statute unconstitutional

<!-- more -->

This is an exciting decision; let's see where it takes us.

> U.S. District Judge Susan Illston ordered the government to stop issuing
> so-called NSLs across the board, in a stunning defeat for the Obama
> administration’s surveillance practices. She also ordered the government to
> cease enforcing the gag provision in any other cases. However, she stayed her
> order for 90 days to give the government a chance to appeal to the Ninth
> Circuit Court of Appeals.[[0]]

[The issues surrounding NSLs][2] were highlighted just last week when [Google
released numbers relating to the orders that it received][3].

[2]: https://www.eff.org/issues/national-security-letters
[3]: /2013/03/google-says-the-fbi-is-secretly-spying-on-some-of-its-customers
