# FSF Condemns Partnership Between Mozilla and Adobe to Support DRM

Two days ago, the Free Software Foundation published [an announcement
strongly condemning Mozilla's partnership with Adobe][0] to implement the
[controversial W3C Encrypted Media Extensions (EME) API][1]. EME has been
strongly criticized by a number of organizations, including the [EFF][2] and
the [FSF's DefectiveByDesign campaign team][3] ("Hollyweb").

[Digital Restrictions Management][4] imposes artificial restrictions on
users, telling them what they can and cannot do; it is a system [that does
not make sense][5] and is harmful to society. Now, just about [a week after
the International Day Against DRM][6], Mozilla decides to [cave into the
pressure in an attempt to stay relevant][7] to modern web users, instead of
sticking to their [core philosophy about "openness, innovation, and
opportunity"][8].

[0]: http://www.fsf.org/news/fsf-condemns-partnership-between-mozilla-and-adobe-to-support-digital-restrictions-management
[1]: https://dvcs.w3.org/hg/html-media/raw-file/tip/encrypted-media/encrypted-media.html
[2]: https://www.eff.org/deeplinks/2013/03/defend-open-web-keep-drm-out-w3c-standards
[3]: /2013/03/defective-by-design-campaign-against-w3c-drm-standard
[4]: http://www.defectivebydesign.org/what_is_drm_digital_restrictions_management
[5]: https://plus.google.com/+IanHickson/posts/iPmatxBYuj2
[6]: http://www.defectivebydesign.org/dayagainstdrm
[7]: https://blog.mozilla.org/blog/2014/05/14/drm-and-the-challenge-of-serving-users/
[8]: http://www.mozilla.org/en-US/about/manifesto/

John Sullivan requested in the [FSF's announcement] that the community
contact Mozilla CTO Andreas Gal in opposition of the decision. This is my
message to him:

<!-- more -->

```
Date: Wed, 14 May 2014 22:57:02 -0400
From: Mike Gerwitz <mikegerwitz@gnu.org>
To: agal@mozilla.com
Subject: Firefox EME

Andreas,

I am writing to you as a free software hacker, activist, and user; notably,
I have been using Firefox for over ten years. It has been pivotal, as I do
not need to tell you, in creating a free (as in freedom), standard, and
accessible internet for millions of users. Imagine my bewildered
disappointment, then, to learn that Firefox has chosen to cave into the
pressure to [support Digital Restrictions Management through the
implementation of EME][0].

Mitchell Baker made a feeble attempt at [rationalizing this decision][0] as
follows:

  [...] Mozilla alone cannot change the industry on DRM at this point.  In
  the past Firefox has changed the industry, and we intend to do so again.
  Today, however, we cannot cause the change we want regarding DRM.  The
  other major browser vendors =E2=80=94 Google, Microsoft and Apple have already
  implemented the new system.   In addition, the old system will be retired
  shortly.  As a result, the new implementation of DRM will soon become the
  only way browsers can provide access to DRM-controlled content.

She goes on to explain how "video is an important aspect of online life"
and that Firefox would be "deeply flawed as a consumer product" if it did
not implement Digital Restrictions Management. This is precisely the FUD
that the "content owners" she describes, and corporations like Adobe, have
been pushing: Mozilla understands that the solution is not to implement DRM,
but to fight to encourage content to be published *without* being
DRM-encumbered. Unfortunately, they will now have little motivation to do
so, with every major browser endorsing EME.

She defers to a post by Andreas Gal [for more implementation details][1], in
which he mentions that the proprietary CDM virus (which will be happily
provided by Adobe) will be protected by a sandbox to prevent certain spying
activities like fingerprinting. While this is better than nothing, it's a
clear attempt by Mozilla to help make a terrible situation a little bit
better.

He goes on to say:

  There is also a silver lining to the W3C EME specification becoming
  ubiquitous. With direct support for DRM we are eliminating a major use
  case of plugins on the Web, and in the near future this should allow us to
  retire plugins altogether.=20

Let us not try to veil the problem and make things look more rosy than they
actually are: this is not a silver lining; it is not appropriate to have a
standardized way of manipulating and taking advantage of users.

It is true that Firefox was in an unfortunate position: many users would
indeed grow frustrated that they cannot watch their favorite TV shows and
movies using Firefox. But Firefox could have served, when the EME API was
used, static content that provided a brief explanation and a link for more
information on the problem. They could have educated users and encourage an
even stronger outcry.

Instead, we are working with the corrupt W3C to implement a seamlessly
shackled web. Mozilla wants to propose alternative solutions to DRM/EME, but
by implementing it, their position is weakened.

  This is a difficult and uncomfortable step for us given our vision of a
  completely open Web, but it also gives us the opportunity to actually
  shape the DRM space and be an advocate for our users and their rights in
  this debate. [1]

Such advocacy has been done and can continue to be done by Mozilla without
the implementation of EME; once implemented, the standard will be virtually
solidified---what is the incentive for W3C et. al. to find alternatives to a
system that is already "better than" the existing Flash and Silverlight
situation?

On behalf of the free software community, I strongly encourage your
reconsideration on the matter. Mozilla is valued by the free software
community for its attention to freedoms. Stand with us and fight. You're in
a powerful position to do so.

[0]: https://blog.mozilla.org/blog/2014/05/14/drm-and-the-challenge-of-serving-users/
[1]: https://hacks.mozilla.org/2014/05/reconciling-mozillas-mission-and-w3c-eme/
```

The following day, I [submitted the FSF announcement to HackerNews][9]
(surprised that it was not there already) in an attempt to bring further
coverage to the matter and hopefully spur on some discussion. And discuss
they did: it was on the front page for the entire day and, at the time of
writing, boasts 261 comments, many of them confused and angry. I sent the HN
link to Andreas in a follow-up as well.

Mozilla has a vast userbase and is in the position to fight for a DRM-free
web. Please voice your opinion and hope that they reverse their decision.

[9]: https://news.ycombinator.com/item?id=7749108
