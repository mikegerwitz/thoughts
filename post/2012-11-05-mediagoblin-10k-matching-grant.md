# MediaGoblin $10k Matching Grant

Congratulations to MediaGoblin for not only [meeting the $10k matching grant
from a generous anonymous donor][0], but also for raising $36k to date.

[MediaGoblin][1] is a "free software media publishing platform that anyone can
run"; it is a distributed, free (as in freedom) alternative to services such as
YouTube, Flickr and others, and is part of the [GNU project][2].

[0]: http://mediagoblin.org/news/we-made-10k-matching.html
[1]: http://mediagoblin.org/
[2]: http://gnu.org/

<!-- more -->
