# OLPC Tablet in Ethiopia

A story mentions how [Ethiopian kids quickly learned to read and use tablet
PCs][0] provided by the [One Laptop Per Child][1] project. This is not only a
noble feat (as we would expect from OLPC), but also an impressive one,
considering that (as the article mentions) the children did not know how to
read, even in their own language.

[0]: http://dvice.com/archives/2012/10/ethiopian-kids.php
[1]: http://one.laptop.org/

<!-- more -->

Now, while the OLPC does have [its own tablet][2], the article mentions that the
[children were given Motorola Zoom tablets][0]; I would hope that they run free
software to encourage freedom in these developing countries and to encourage the
children to hack and explore their devices in even greater detail.

[2]: http://one.laptop.org/about/xo-3
