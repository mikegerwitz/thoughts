# Trademarks in Free Software

The use of trademarks in free software has always been a curious and unclear
concept to me, primarily due to my ignorance on the topic.

Trademarks, unless abused, are intended to protect consumers' interests---are
they getting the brand that they think they're getting? If you download Firefox,
are you getting Firefox, or a derivative?

<!-- more -->

Firefox is precicely one of those things that has brought this issue to light
for me personally: the name is trademarked and derivatives must use their own
names, leading to IceCat, IceWeasel, Abrowser, etc. Even though FF is free
software, the trademark imposes additional restrictions that seem contrary to
the free software philosophy. As such, it was my opinion that trademarks should
be avoided or, if they exist, should not be exercised. (GNU, for example, is
trademarked[^0], but the FSF certainly [does not exercise it][1]; consider gnuplot,
a highly popular graphing program, which is not part of the GNU project.)

[This article][2] provides some perspective on the topic and arrives at much the
same conclusions: trademark enforcement stifles adoption and hurts the project
overall.

I recommend that trademarks not be used for free software projects, though I am
not necessarily opposed to registering a trademark "just in case" (for example,
to prevent others from maliciously attempting to register a trademark for your
project).

[1]: http://www.gnu.org/prep/standards/html_node/Trademarks.html
[2]: http://mako.cc/copyrighteous/20120902-00

[^0]: uspto.gov; serial number 85380218; reg. number 4125065.
      From what I could find from the USPTO website, it was submitted by
      Aaron Williamson of the SFLC (http://www.softwarefreedom.org/about/team/)
