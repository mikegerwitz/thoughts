# Verizon router backdoors

A [very disturbing article][0] makes mention of a Verizon TOS update for its
Internet service customers:

[0]: http://www.linuxbsdos.com/2012/10/04/is-that-a-backdoor-or-an-administrative-password-on-your-verizon-internet-router/

> Section 10.4 was updated to clarify that Verizon may in limited instances
> modify administrative passwords for home routers in order to safeguard
> Internet security and our network, the security and privacy of subscriber
> information, to comply with the law, and/or to provide, upgrade and maintain
> service.

<!-- more -->

...what? This is deeply disturbing, deeply perverted idea of security. Not only
is this a severe privacy concern (all internet traffic passes through your
router), but it's a deep *security* concern---what if a cracker is able to
figure out Verizon's password scheme, intercept the communication with your
router or otherwise?

I recommend that you (a) use your own router, (b) change its default password if
you have not yet done so and (c) disallow remote access. Furthermore, I
recommend using a free (as in freedom) firmware such as [DD-WRT][1] if supported
by your hardware.

[1]: http://dd-wrt.com/
