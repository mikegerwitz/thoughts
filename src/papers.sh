#!/bin/bash
# Generate papers HTML page
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Papers are (at least at present) written in LaTeX, whereas articles are
# simply posts.  Both are specified in $PAPERFILE.  This page generates
# abstracts for both formats, along with links to each of their output
# formats (one or more of PDF, DVI, HTML).
##

set -euo pipefail

# Refile containing paper references and metadata.
declare -r PAPERFILE=${PAPERFILE:-src/papers.rec}


# List ids of all papers in $PAPERFILE.
paper-list()
{
  recsel -CP id "$PAPERFILE"
}


# Retrieve field FIELD from paper ID in $PAPERFILE.
paper-field()
{
  local -r id=${1?Missing paper id}
  local -r field=${2?Missing paper field}

  recsel -P "$field" -e "id = '$id'" "$PAPERFILE"
}


# Read field FIELD from post metadata recfile FILE.
post-field()
{
  local -r ref=${1?Missing post name}
  local -r field=${2?Missing field name}

  recsel -P "$field" "post/$ref.meta"
}


# Generate abstract for article or paper ID.  Delegates to one of
# {post,latex}-abstract based on its type.
abstract()
{
  local -r id=${1?Missing paper id}

  local type ref
  type=$( paper-field "$id" type )
  ref=$( paper-field "$id" ref )

  case "$type" in
    post|latex)
      "$type-abstract" "$id" "$ref";;
    *)
      echo "Unknown paper type for id \`$id" >&2
      return 1
  esac
}



# Generate abstract for post REF.
post-abstract()
{
  local -r ref=${2?Missing post ref}

  local id title date abstract slug
  id=$( post-field "$ref" id )
  title=$( post-field "$ref" subject )
  date=$( post-field "$ref" date )
  abstract=$( post-field "$ref" abstract )
  slug=$( post-field "$ref" slug )

  cat <<EOF
<article class="abstract paper">
  <h2 class="title" id="$id"><a href="/$slug">$title</a></h2>

  <ul class="links">
    <li class="title">Formats:</li>
    <li><a href="/$slug">View HTML</a></li>
  </ul>

  $abstract

  <p class="date">Posted on $date.</p>
</article>
EOF
}


# Extract title from LaTeX document.  Note that this performs no actual
# processing on that title, so this will need to be e.g. run through Pandoc
# in the future if titles contain something that should be parsed (like
# dashes).
latex-title()
{
  head -n1 | sed '1s/^% //;1a\\'
}


# Produce text of LaTeX abstract (from its abstract.tex).
#
# Two minor transformations are made: Footnotes are removed by exploiting
# Pandoc's behavior of ignoring unknown/unsupported commands, since that
# doesn't look very good in the abstract output.  Emdashes have whitespace
# on either side removed to translate to my modern convention (this can be
# removed when old papers are updated).
latex-abstract-text()
{
  sed 's/\\footnote/\\void/;
       s/ \+--- \+/---/g' \
    | pandoc -flatex -thtml
}


# Generate abstract for LaTeX document (from abstract.tex) ID located at
# path REF.  REF is expected to contain `abstract.tex' and `REF.tex', along
# with the built `REF.pdf'.
latex-abstract()
{
  local -r id=${1?Missing paper id}
  local -r ref=${2?Missing paper ref}

  local -r abstract_tex="$ref/abstract.tex"
  local -r main="$ref/${ref##*/}.tex"
  local -r sans=${main%/*.tex}

  local title abstract pubdate
  title=$( latex-title < "$main" )
  abstract=$( latex-abstract-text < "$abstract_tex" )
  pubdate=$( paper-field "$id" pubdate )

  cat <<EOF
<article class="abstract paper">
  <h2 class="title" id="$id"><a href="/$sans.pdf">$title</a></h2>

  <ul class="links">
    <li class="title">Formats:</li>
    <li><a href="/$sans.pdf">View PDF</a></li>
  </ul>

  $abstract

  <p class="date">Published on $pubdate.</p>
</article>
EOF
}


# Generate papers page.
main()
{
  src/mkheader papers Papers

  local papers
  papers=$( recsel -P id src/papers.rec )

  echo '<h1>Papers / Articles</h1>'
  paper-list | while read id; do abstract "$id"; done

  cat src/footer.tpl.htm
}


main "$@"
