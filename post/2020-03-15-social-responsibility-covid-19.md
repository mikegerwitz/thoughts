# Social Responsibility Amid COVID-19 Outbreak

Most of my posts here relate somehow to software freedom,
  user privacy,
  or some other issue driven by technology.
As an activist for user freedom,
  my goal is usually to figure out ways in which to empower people using
  technology---to
    put them on equal footing with those that are in a position to exhert
      control.
    To make the vulnerable less vulnerable.

But all of that is a focused fight as part of broader goal for social
  freedom and equality.
If we take a moment to look up from out focus on technology to see the
  bigger picture,
    we can see that our activism and advocacy follow a moral framework that
    necessitates certain responsibility during this outbreak of COVID-19
    caused by the novel coronavirus.

<!-- more -->

The purpose of this post is not to give a history on the disease
  (you [can read about that elsewhere][wikipedia-covid-19])---it
  is meant to appeal to people _during the ongoing crisis_ to help save
  lives.

[wikipedia-covid-19]: https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic

I live in the United States.
  Buffalo, New York, specially, which is on the edge of Lake Erie in
  Western NY.
Yesterday,
  we received word of our first confirmed COVID-19 cases in the region,
    one of them in a neighboring town where I grew up and where my mother
    lives.
Today,
  our County executive [closed down schools until at least
  April 20th][school-close],
    and we learned of more cases.
I have two young school-aged children,
  but fortunately I have the option to work from home.
The company I work for announced today that all of its offices nationally
  will be closed at least through the end of March.

[school-close]: https://news.wbfo.org/post/erie-county-covid-19-state-emergency-schools-closed-until-april-20

But this isn't at all unexpected.
And I don't think the response is at all disproportionate.
The problem is:
  the United States has done a very poor job in making enough tests
  available to meet demand;
    consequently,
      we have no idea how many actual cases exist in most communities.
Even though we just received word of a handful of confirmed cases in my
  community,
    I suspect that (in my fairly large county) that actual number is in the
    hundreds or thousands.

I'm young---I just turned 30 this past October.
Children seem to experience extremely mild symptoms of the illness,
  so I'm not too worried about them.
My wife is pregnant and due at the end of April,
  but [the risk may not be high there either][pregnancy].
So why am I writing this post if the most I have to worry about personally
  is that I'm having trouble finding groceries and toiletries because people
  are panic-buying and hoarding?

[pregnancy]: https://www.cdc.gov/coronavirus/2019-ncov/prepare/pregnancy-breastfeeding.html?

![Graph of COVID-19 Fatality Rate By Age
  [^age-graph]](/images/tp/covid-19-fatality-rate.png)

[^age-graph]: [Source](https://www.scientificanimations.com/wiki-images/).
  Copyright © 2020 www.scientificanimations.com.  CC BY-SA 4.0.
  [Wikimedia Commons](https://en.wikipedia.org/wiki/File:Illustration_of_SARS-COV-2_Case_Fatality_Rate_200228_01-1.png).


It's not uncommon for young people to feel impervious.
Invincible.
If you know that your symptoms from COVID-19 will resemble a cold or flu,
  why bother worrying at all?

As you can see from the above graph,
  the mortality rate of the disease increases drastically in populations
  60 years of age and older.
So while me and my children may not be at high risk,
  what happens if they go visit and hug their great grandmother?
Or what of someone out shopping that may be showing no symptoms but then
  touches a surface,
    and then an elderly person touches that same surface later that day?

Or what if you _are_ actually healthy,
  and have not contracted the disease?
You may still be able to pass it from surface to surface just by being out
  and about.

!["Flatten the Curve" Graph and Animation [^flatten-curve]](/images/tp/covid-19-flatten-curve.gif)

[^flatten-curve]:
    [Source](https://thespinoff.co.nz/society/09-03-2020/the-three-phases-of-covid-19-and-how-we-can-make-it-manageable/).
    Copyright © 2020 Siouxsie Wiles and Toby Morris.  CC BY-SA 4.0.
    [Wikimedia Commons](https://en.wikipedia.org/wiki/File:Covid-19-curves-graphic-social-v3.gif)

Our actions have real consequences.

Much of the talk is now about "flattening the curve" through social
  distancing.
Events and [mass gatherings][] are being canceled.
People are being asked to work from home,
  and quarantine themselves at home when sick.
Elective surgeries are being canceled to make room in hospitals for patients
  that really need to be there.

[mass gatherings]: https://www.cdc.gov/coronavirus/2019-ncov/community/large-events/mass-gatherings-ready-for-covid-19.html

If too many people contract COVID-19 at once,
  our healthcare system will not be able to keep up.
If we can throttle the spread of the disease over a long enough period of
  time,
    patients will be more likely to get the life-saving care that they need
    and may end up recovering.

So when I think of myself as an activist for user freedom---to
  level the playing field---I
  now think of the above graphs.
If I want to be a positive change for society,
  what is the most important and most effective thing I can do right now?

It has nothing to do with software.

The best thing I can do is _stay at home_ unless it's necessary that I go
  out,
    in the hope that I can do my part to keep this disease from spreading
    ever more rapidly.
If you do go out,
  _wash your hands_ with soap and water for at least 20 seconds.
Don't touch your face without washing your hands.
Use hand sanitizer that is at least 60% alcohol.
Keep your distance from other people.
Avoid social gatherings.
Stop hoarding supplies that others need,
  and may even need more than you.
_Consider others._

I hope that you will do the same.
But at the same time,
  I also recognize that I'm in a fortunate situation,
    being able to work from home and continue receiving a paycheck.
Others may not have that option.
They may not be able to stay home with kids,
  or pay for child care.
They may miss paychecks.
They may miss payments on rent or student loans or other debts.
They may be without healthcare.
And if you are one of those people reading this,
  I hope that the federal government and your State government are able to
  provide you with relief soon.
This is going to get worse before it gets better.

For more information,
  see the [CDC's COVID-19 Resources](https://www.cdc.gov/coronavirus/2019-ncov/index.html).

