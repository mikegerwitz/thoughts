# HTML5 DRM

Two acronyms that, until very recently, would seem entirely incompatible---HTML,
which is associated with an unencumbered, free (as in freedom) representation of
a document, and [DRM][0], which [exists for the sole purpose of restricting
freedom][1].[^bias] Unfortunately, Tim Berners-Lee---the man attributed to
["inventing" the Internet][18]---mentioned in a [keynote talk at SXSW][15] that [he is
not opposed to introducing DRM into the HTML5 standard][4]:

[^bias]: (Disclaimer: I am an associate member of the [Free Software
Foundation][2] and, as such, this reference is intentionally bias; feel free
to see the [Wikipedia article on DRM][3] for more general information.)

> [Tim Berners-Lee] did not, however, present himself as an opponent of digital
> locks. During a post-talk Q&A, he defended proposals to add support for
> "digital rights management" usage restrictions to HTML5 as necessary to get
> more content on the open Web: "If we don't put the hooks for the use of DRM
> in, people will just go back to using Flash," he claimed.

<!-- more -->

Many who oppose DRM refer to it as ["digital restrictions management"][0]---a
phrase that better describes how it affects the user. The "rights" that
"digital rights management" describes are the "rights" (in terms of
copyright) of publishers and copyright holders: They wish to lock down their
content so that [you, the user, can only access it as *they* please][5]. Has
["your" device][25] ever told you that [you cannot share a book with your
friends][6][17][24]?  Has your device ever [deleted your content without your
permission][7][8]?  Does your device grant you [less privileges if you decide to
liberate yourself from it][9] through "jailbreaking"?[^jb] Does the software you
run [potentially spy on you without telling you][11], without giving you the
option to correct it? Or perhaps the games you play [require you to be online,
even in single-player mode][12].

[^jb]: I go into more detail on jailbreaking and its current legality as of
the time of writing [in a previous article of mine][10].

These are but a small handful of [examples of the many mistakes and injustices
of Digital Restrictions Management][5]. These restrictions take additional
effort---that is, development time, which also means more money---to build into
software; computers, by their very nature, do exactly as they are told, meaning
that they can only work against you if someone else tells it to (unless you tell
your computer to make your life miserable...if you're into that sort of thing).
As such, we refer to these restrictions as ["anti-features"][23].

> Corporations claim that DRM is necessary to fight copyright infringement
> online and keep consumers safe from viruses. But there's no evidence that DRM
> helps fight either of those. Instead DRM helps big business stifle innovation
> and competition by making it easy to quash "unauthorized" uses of media and
> technology.

It is this logic that [corporations][13] (and even some individuals, such as
[authors][14]) use to influence entities such as the W3C---and Tim
Berners-Lee---into [thinking that DRM is necessary][15]. The [W3C describes a
"trust infastructure"][16] that could be standardized for bringing DRM to the
web:

> It is clear that user domains (eg eBook trading, sub-rights trading, streaming
> music, etc.) each require sets of Rights Primitives that those domains wish do
> useful things with.

This is an unfortunate perspective, especially since those "useful things" are
exactly the opposite for users. The Internet strongly promotes the free,
(generally) unencumbered flow of information. To [quote W3C][19]:

> The social value of the Web is that it enables human communication, commerce,
> and opportunities to share knowledge. One of W3C's primary goals is to make
> these benefits available to all people, whatever their hardware, software,
> network infrastructure, native language, culture, geographical location, or
> physical or mental ability.

A DRM implementation flies in the face of those goals, as it is, by definition,
restrictive---how can we be encouraged to share by using systems that aim to
[prevent that very thing][0]?

Richard Stallman has already announced that the [FSF will "campaign against W3C
support for DRM"][20]; let's hope that many others will join in on this
campaign, hope that organizations like the EFF will continue to fight for our
rights, and further hope that users will [reject DRM-laden products][22]
outright. [DRM cannot exist in free software][25] and it cannot exist on a
network that facilitates free information.

[0]: http://www.defectivebydesign.org/what_is_drm
[1]: http://www.defectivebydesign.org/
[2]: http://fsf.org
[3]: https://en.wikipedia.org/wiki/Digital_rights_management
[4]: http://boingboing.net/2013/03/10/tim-berners-lee-the-web-needs.html
[5]: https://www.eff.org/issues/drm
[6]: http://www.amazon.com/gp/help/customer/display.html?nodeId=200549320
[7]: http://www.defectivebydesign.org/blog/1248
[8]: http://boingboing.net/2012/10/22/kindle-user-claims-amazon-dele.html
[9]: http://arstechnica.com/apple/2011/02/ibooks-to-jailbreakers-no-yuo/
[10]: /2013/03/white-house-supports-cell-phone-unlocking
[11]: /2013/01/re-who-does-skype-let-spy
[12]: https://www.eff.org/deeplinks/2013/03/tale-simcity-users-struggle-against-onerous-drm
[13]: http://venturebeat.com/2012/10/12/together-html5-and-drm-can-take-out-native-apps/
[14]: /2013/01/lulu-says-goodbye-to-drm
[15]: http://www.guardian.co.uk/technology/blog/2013/mar/12/tim-berners-lee-drm-cory-doctorow
[16]: http://www.w3.org/2000/12/drm-ws/
[17]: https://www.fsf.org/bulletin/e-books-must-increase-our-freedom-not-decrease-it
[18]: http://www.w3.org/People/Berners-Lee/
[19]: http://www.w3.org/Consortium/mission#principles
[20]: http://lists.libreplanet.org/archive/html/libreplanet-discuss/2013-03/msg00007.html
[21]: https://www.eff.org/deeplinks/2012/11/2012-dmca-rulemaking-what-we-got-what-we-didnt-and-how-to-improve
[22]: http://www.defectivebydesign.org/guide
[23]: https://www.fsf.org/bulletin/2007/fall/antifeatures/
[24]: https://www.gnu.org/philosophy/right-to-read.html
[25]: https://www.gnu.org/philosophy/can-you-trust.html

