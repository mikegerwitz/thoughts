# The Ethics Void: Join Me at LibrePlanet 2018!

I got word today that I'll be speaking again at this year's [LibrePlanet][]!
I was going to attend even if I were not speaking,
  but I'm very excited to be able to continue to build off of last year's
  talk and further my activism on these topics.

[LibrePlanet]: https://libreplanet.org/2018/

The title of this year's talk is _The Ethics Void_.
Here's a rough abstract:

<!-- more -->

> Medicine, legal, finance, journalism, scientific research—each of these
> fields and many others have widely adopted codes of ethics governing the
> lives of their professionals. Some of these codes may even be enshrined in
> law. And this is for good reason: these are fields that have enormous
> consequences.

> Software and technology pervade not only through these fields, but through
> virtually every aspect of our lives. Yet, when compared to other fields, our
> community leaders and educators have produced an ethics void. Last year, I
> introduced numerous topics concerning #privacy, #security, and #freedom that
> raise serious ethical concerns. Join me this year as we consider some of
> those examples and others in an attempt to derive a code of ethics that
> compares to each of these other fields, and to consider how leaders and
> educators should approach ethics within education and guidance.

(My previous talks can be found on my ["Talks" page][talks].)

For this talk,
  I want to solicit the community at various points.
I know what _I_ want to talk about,
  but what are some of the most important ethical issues to _you_?
Unfortunately there's far too much to fit into a 40-minute talk!
Feel free to send me an e-mail or reply to the [thread on GNU Social][thread].

[talks]: /talks
[thread]: https://social.mikegerwitz.com/conversation/99140
