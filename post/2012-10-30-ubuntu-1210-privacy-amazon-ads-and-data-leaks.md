# Ubuntu 12.10 Privacy: Amazon Ads and Data Leaks

The EFF [cautions that Ubuntu 12.10 leaks user information to Amazon by
default][0] rather than requiring the user to opt *into* the system.

Of course, I cannot recommend that you use Ubuntu, as it encourages the
installation of non-free device drivers, readily enables non-free software
repositories and contains non-free components in its kernel.[1] Instead,
consider a [fully free GNU/Linux distribution like Trisquel][2].

[0]: https://www.eff.org/deeplinks/2012/10/privacy-ubuntu-1210-amazon-ads-and-data-leaks
[1]: http://www.fsfla.org/svnwiki/selibre/linux-libre/
[2]: https://trisquel.info

<!-- more -->
