# Jailbreaking and DCMA---EFF Touts Victory, FSF Warns Of Failure

While the [EFF is pleased to announce][0] that the Copyright Office has [renewed
DMCA exceptions upholding jailbreaking rights for cellphones][1], the FSF
cautions that [this right has not been extended to tablets, game consoles or
even PCs with restricted boot][2].

[0]: https://www.eff.org/press/releases/eff-wins-renewal-smartphone-jailbreaking-rights-plus-new-legal-protections-video
[1]: http://www.copyright.gov/fedreg/2012/77fr65260.pdf
[2]: http://www.fsf.org/blogs/licensing/copyright-office-fails-to-protect-users-from-dmca

<!-- more -->

It should be noted that the EFF also successfully gained protection for the use
of short copyrighted clips in remixing,[0] and while this is a positive step
forward in its own, the implications of the first paragraph should not be
ignored.

