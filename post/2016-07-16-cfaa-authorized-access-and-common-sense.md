# CFAA, "Authorized" Access, and Common Sense

There is little common sense to be had with the [Computer Fraud and Abuse
  Act][cfaa] (CFAA) to begin with.
To add to the confusion,
  the Ninth Circuit Court of Appeals last week held 2-1 in [United States
  v. Nosal][uvn] that accessing a service using someone else's
  password---even if that person gave you permission to do so---[violates
  the CFAA][cfaa-passwd],
    stating that only the _owner_ of a computer can give such authorization.
This is absurd even with complete lack of understanding of what the law is:
  should your spouse be held criminally liable for paying your bills online
  using your account?

[cfaa]: https://www.eff.org/issues/cfaa
[uvn]: https://www.eff.org/cases/u-s-v-nosal
[cfaa-passwd]: https://www.eff.org/deeplinks/2016/07/ever-use-someone-elses-password-go-jail-says-ninth-circuit

Common sense says no.

<!-- more -->

In another case this week---[Facebook v. Power Ventures][fvp]---the same
  court (though a different panel of judges) stepped back from the original
  decision and stated that computer _users_ can indeed provide
  authorization.
This authorization holds even if the service's Terms of Service say
  otherwise.
Yet: the computer owner (in this case, Facebook) can revoke authorization,
  which takes precedence over any authorization provided by a user of that
  system.
So with a seemingly magical incantation,
  a benign situation can be made into a federal crime,
  just like that.

These situations highlight dangerous confusion over the interpretation of an
  already dangerously vague law.
The CFAA is the law that was used to prosecute Aaron Swartz for federal
  "crimes"---with a punishment of up to thirty-five years in prison---for
  liberating documents hosted on JSTOR.
Because of this [draconian threat][eff-punish],
  [Aaron committed suicide][aaron] on January 11th, 2013.

The CFAA already has blood on its hands;
  it needs to be reined _in_,
    not be given further broad powers.
So don't take news of the decisions in US v. Nosal and Facebook v. Power
  Ventures as canceling one-another out;
    things may appear the same for now,
      but serious problems still need to be resolved.

[cfaa-back]: https://www.eff.org/deeplinks/2016/07/ninth-circuit-panel-backs-away-dangerous-password-sharing-decision-creates-even
[fvp]: https://www.eff.org/cases/facebook-v-power-ventures
[eff-punish]: https://www.eff.org/deeplinks/2013/02/rebooting-computer-crime-part-3-punishment-should-fit-crime
[aaron]: https://www.eff.org/deeplinks/2013/01/farewell-aaron-swartz
