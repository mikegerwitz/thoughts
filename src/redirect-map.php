<?php
/**
 * Redirect old post ("thought") pages
 *
 *  Copyright (C) 2019 Mike Gerwitz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This script should only be invoked for post paths containing an uppercase
 * character.
 *
 * When this site migrated away from repo2html, ids were changed to be all
 * lowercase.  Further, some of the ids changed in other ways.  To handle
 * that latter case, a manual mapping was created.
 *
 * This script issues permanent redirects and, for security reasons, works
 * only for post pages.
 */

$req_path = strtolower( $_GET[ 'path' ] );

// Abort immediately if this is not a blog post (prevent this script from
// being used for arbitrary redirects)
if ( !preg_match( '#^/\d{4}/\d{2}/#', $req_path ) ) {
    http_response_code( 500 );
    die( 'Unsupported redirect' );
}

// Map from old to new location
$map = [
 "/2012/10/digitizing-books-is-fair-use-author-s-guild-v.-hathitrust"                        => "/2012/10/digitizing-books-is-fair-use-authors-guild-v-hathitrust",
 "/2012/10/getting-too-tired-to-hack-at-23-00"                                               => "/2012/10/getting-too-tired-to-hack-at-2300",
 "/2012/10/jailbreaking-and-dcma-eff-touts-victory-fsf-warns-of-failure"                     => "/2012/10/jailbreaking-and-dcmaeff-touts-victory-fsf-warns-of-failure",
 "/2012/10/openwireless.org"                                                                 => "/2012/10/openwirelessorg",
 "/2012/10/the-use-of-trademarks-in-free-software-has-always-been-a-curious-and-unclear"     => "/2012/10/trademarks-in-free-software",
 "/2012/10/ubuntu-12.10-privacy-amazon-ads-and-data-leaks"                                   => "/2012/10/ubuntu-1210-privacy-amazon-ads-and-data-leaks",
 "/2012/11/copyright-reform-you-re-silly"                                                    => "/2012/11/copyright-reform-youre-silly",
 "/2012/11/u.s.-copyright-alert-system"                                                      => "/2012/11/us-copyright-alert-system",
 "/2012/11/vlc-s-move-to-lgpl"                                                               => "/2012/11/vlcs-move-to-lgpl",
 "/2013/04/u.s.-house-passes-cispa"                                                          => "/2013/04/us-house-passes-cispa",
 "/2013/08/freebsd-clang-and-gcc-copyleft-vs.-community"                                     => "/2013/08/freebsd-clang-and-gcc-copyleft-vs-community",
 "/2013/08/freebsd-clang-and-gcc-copyleft-vs.community"                                      => "/2013/08/freebsd-clang-and-gcc-copyleft-vs-community",
 "/2013/08/windows-8.1-to-display-targeted-advertisements-on-local-system-searches"          => "/2013/08/windows-81-to-display-targeted-advertisements-on-local-system-searches",
 "/2014/03/re-freebsd-clang-and-gcc-copyleft-vs.-community"                                  => "/2014/03/re-freebsd-clang-and-gcc-copyleft-vs-community",
 "/2016/01/google-analytics-removed-from-gitlab.com-instance"                                => "/2016/01/google-analytics-removed-from-gitlabcom-instance",
 "/2016/08/nso-group-pegasus-trident-ios-exploits-targeting-human-rights-activist"           => "/2016/08/nso-group-pegasus-tridentios-exploits-targeting-human-rights-activist",
 "/2017/06/don-t-force-me-to-use-your-tools-on-the-web"                                      => "/2017/06/dont-force-me-to-use-your-tools-on-the-web",
 "/2018/04/when-talking-about-mobile-tracking-don-t-veil-bad-actors-with-blanket-statements" => "/2018/04/when-talking-about-mobile-tracking-dont-veil-bad-actors-with-blanket-statements",
 "/2018/10/webmasters-please-don-t-block-tor"                                                => "/2018/10/webmasters-please-dont-block-tor",
];

// Redirect either to the mapped location or to the lowercased location
$dest_path = ( isset( $map[ $req_path ] ) )
    ? $map[ $req_path ]
    : $req_path;

// Permanent redirect
http_response_code( 301 );
header( "Location: $dest_path" );
