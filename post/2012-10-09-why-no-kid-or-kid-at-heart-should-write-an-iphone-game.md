# Why no kid (or kid at heart) should write an iPhone game

I saw [this post][0] appear on HackerNews, talking about how building a game for
iOS is "fun" and "cool". The poster lures the reader in with talk of making
money and talks of a "unique sense of fulfillment" that comes with development
of these games, and then goes on to invite kids to learn how to develop games
for the iPhone (and presumably other iOS devices).

[0]: http://blog.makegameswith.us/post/33263097029/call-to-arms

This is a terrible idea.

<!-- more -->

Getting children involved with hacking is an excellent idea, but introducing
them to the evils of Apple and associating that with a feeling of pleasure does
a great disservice; all software developed for iOS must be "purchased" (even
if it's of zero cost) through a walled garden called the "App Store". The
problem with this is that [the App Store is hostile toward free
software][1]---its overly restrictive terms are incompatible with free software
licenses like the GPL. Teaching children to develop software for this crippled,
DRM-laden system is teaching them that it is good to prevent sharing, stifle
innovation and deny aid to your neighbor.

A better solution would be to suggest developing software for a completely free
mobile operating system instead of iOS, such as [Replicant][2] (a fully free
Android distribution). Even if Replicant itself were not used, Android itself,
so long as proprietary implementations and "stores" are avoided[[3]], is much
more [compatible with education][4] than iOS, since the children are then able
to freely write and distribute the software without being controlled by
malicious entities like Apple. Furthermore, they would then be able to use a
fully free operating system such as GNU/Linux to *write* the software.

Do not let fun and wealth disguise this ugly issue. Even more importantly---do
not pass this practice and woeful acceptance down to our children. I receive a
"unique sense of fulfillment" each and every day hacking free software far
away from Apple's grasp.

[1]: http://www.fsf.org/news/blogs/licensing/more-about-the-app-store-gpl-enforcement
[2]: http://replicant.us/
[3]: http://www.gnu.org/philosophy/android-and-users-freedom.html
[4]: http://www.gnu.org/education/edu-schools.html
