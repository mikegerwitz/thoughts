# National Uproar: A Comprehensive Overview of the NSA Leaks and Revelations

I am finding it difficult to keep up with the flood of reports in my little free
time, while still finding the time to brush up on relevant history. My hope is
to provide a summary of recent events and additional background---along with a
plethora of references---that will allow the reader to perform further research
and to formulate educated, personal opinions on the topics. If you do not care
for my commentary, simply scroll to the list of references at the bottom of this
article.

Many [individuals and organizations][0] have long warned of [digital privacy
issues][1], but there has been one agency in particular that has been the
subject of much scrutiny---the [National Security Agency (NSA)][2], which is a
[United States government agency][3] that has a [long history of controversial
spying tactics][4] on its country's own citizens. It is a chilling topic---one
that can easily make any person sound like they've latched onto an Orwellian
conspiracy.

[0]: /2013/01/re-who-does-skype-let-spy
[1]: https://www.schneier.com/essay-418.html "The Internet Is a Surveillance State"
[2]: https://www.eff.org/nsa-spying "The EFF on NSA Spying"
[3]: https://www.eff.org/agency/national-security-agency "The National Security Agency"
[4]: https://www.eff.org/nsa-spying/timeline "Timeline of NSA Spying"

<!-- more -->

**Wednesday, June 5th, 2013**---[the Guardian newspaper publishes a leaked
document][5][6][7] ordering Verizon to

> [...] produce to the National Security Agency (NSA) upon service of this
> Order, and continue production on an ongoing daily basis thereafter for the
> duration of this Order, [...] an *electronic copy of* the following tangible
> things: *all call detail records or "telephony metadata"* created by Verizon
> for communications (i) between the United States and abroad; or (ii) wholly
> within the United States, *including local telephone calls*.[[6]] [emphasis
> added]

The order goes on to describe "telephony metadata" to include routing
information, source and destination telephone numbers, IMSI and IMEI numbers,
and time and duration of the call; it "does not include the substantive content
of any communication"---the communication content itself.[[6]] This order was
[issued by the Foreign Intelligence Surveillance Court (FISC)][8] under [section 215
of the Patriot Act][9]. (This news comes [less than three months after United
States District Judge Susal Illston ruled NSA Letters' gag provisions
unconstitutional][10].)

This report caused a massive uproar, but [came as no surprise][11] to many
security researchers and privacy advocates. Early last year, Wired released an
article stating that [the NSA "Is Building the Country's Biggest Spy
Center"][14].  Privacy concerns were raised in November of last year by [the
Petraeus scandal][14]. In March of this year, Google released figures showing
that [the NSA is secretly spying on some of its customers][15]. Two months later,
[outrage][17] after the Associated Press discovers that [the Justice Department
collected the calling records of many of its reporters and editors][18].
Additionally, [the EFF already had cases against the NSA's actions][2]---[Jewel
v. NSA][12] and [Hepting v.  AT&T][13] both focus on unconstitutional dragnet
surveillance of innocent citizens' data and communications. These cases will be
explored in further detail throughout this article.

But the chaos didn't end there.

**Thursday, June 6th, 2013**---just one day after the Guardian reported on the
leaked Verizon order, the newspaper reports on [a leaked slideshow describing
PRISM][19], a top-secret program that "claims direct access to servers of firms
including Google, Apple and Facebook. According to the leaked document, the NSA
supposedly has the ability to collect material including e-mail, chat, video and
voice communications, photos, stored data and more.[[19]]. Responses from most
companies was immediate. In a [blog post entitled "What that...?"][20], Larry
Page---Google's CEO---put very plainly that Google does not participate in such
a program and denied any knowledge of PRISM:

> First, we have not joined any program that would give the U.S. government—or
> any other government—direct access to our servers. Indeed, the U.S. government
> does not have direct access or a "back door" to the information stored in
> our data centers. We had not heard of a program called PRISM until yesterday.
> Second, we provide user data to governments only in accordance with the
> law.[[20]] --Larry Page, Google CEO

[Mark Zuckerberg of Facebook also denied involvement][21], calling such claims
"outrageous" and encouraging governments to be "much more transparent about
all programs aimed at keep the public safe":

> I want to respond personally to the outrageous press reports about PRISM:
> Facebook is not and has never been part of any program to give the US or any
> other government direct access to our servers. We have never received a
> blanket request or court order from any government agency asking for
> information or metadata in bulk, like the one Verizon reportedly received. And
> if we did, we would fight it aggressively. We hadn't even heard of PRISM
> before yesterday. [...] We strongly encourage all governments to be much more
> transparent about all programs aimed at keeping the public safe. It's the only
> way to protect everyone's civil liberties and create the safe and free society
> we all want over the long term.[[21]] --Mark Zuckerberg, Facebook CEO

Indeed, [all companies eventually denied involvement with PRISM][22].

**Friday, June 7th, 2013**---Two days after the [initial Verizon report][5] and one day
after the publishing of [portions of the PRISM documents][19], the White House
responded to the Guardian reports with President Obama [defending his
administration][16]. Unfortunately, given the [history of the NSA surveillance
programs][4]---especially since the Bush administration after the 9/11
attacks---it may be difficult to believe that his words are the whole truth. As
such, we will use [portions of his transcript][16] to guide the remainder of this
discussion.

>  **Jackie Calmes:** Mr. President, could you please react to the reports of
>  secret government surveillance of phones and Internet? And can you also assure
>  Americans that the government — your government doesn’t have some massive
>  secret database of all their personal online information and activity?
>
>  **Obama:** [...] Now, the programs that have been discussed over the last
>  couple days in the press are secret in the sense that they’re classified, but
>  they’re not secret in the sense that when it comes to telephone calls, every
>  member of Congress has been briefed on this program.
>
>  With respect to all these programs, the relevant intelligence committees are
>  fully briefed on these programs. These are programs that have been authorized
>  by broad, bipartisan majorities repeatedly since 2006. And so I think at the
>  outset, it's important to understand that your duly elected representatives
>  have been consistently informed on exactly what we’re doing.[[16]]

There are some important notes regarding the phrasing of the President's
statement. Firstly, it is important to note that the President is *confirming the
existence of* the programs that "have been discussed over the last couple days
in the press"---that is, the [Verizon FISA Court order][5] and the [PRISM][19]
leak. However, it is also important to take a step back and note that the
President did *not* state outright that the reports tell the whole---or even the
correct---story. So what do we know?

On June 6th---a day before the White House responded to the leaks---the Director
of National Intelligence James Clapper [declassified certain information pertaining
to the "business records" provision of FISA][23], stating, "I believe it is
important for the American people to understand the limits of this targeted
counterterrorism program and the principles that govern its use". This statement
mentions that:

> Although this program has been properly classified, the leak of one order,
> without any context, has created a misleading impression of how it operates.
> [...] The program does not allow the Government to listen in on anyone's phone
> calls. The information acquired does not include the content of any
> communications or the identity of any subscriber. The only type of information
> acquired under the Court's order is telephony metadata, such as telephone
> numbers dialed and length of calls.[[23]]

The term "telephony metadata" could mean anything; the "numbers dialed" and
"length of calls" are part of it, but what does [the Court order][6]
specifically request?

> IT IS HEREBY ORDERED that [Verizon] shall produce to the [NSA] [...], and
> continue production on an ongoing daily basis [...] for the duration of this
> Order, [...] all call detail records or "telephony metadata" [...].
> Telephony metadata includes comprehensive communications routing information,
> including but not limited to [...] originating and terminating telephone
> number, [...] International Mobile Subscriber Identity (IMSI) number,
> International Mobile station Equipment Identity (IMEI) number, [...] trunk
> identifier, telephone calling card numbers, and time and duration of call.
> Telephony metadata does not include the substantive content of any
> communication [...], or the name, address, or financial information of a
> subscriber or customer.[[6]] --FISA Court order

The President made this point very clear:

> **Obama:** When it comes to telephone calls, nobody is listening to your
> telephone calls.  That’s not what this program’s about. As was indicated, what
> the intelligence community is doing is looking at phone numbers and durations
> of calls. They are not looking at people’s names, and they’re not looking at
> content. But by sifting through this so-called metadata, they may identify
> potential leads with respect to folks who might engage in terrorism. If these
> folks — if the intelligence community then actually wants to listen to a phone
> call, they’ve got to go back to a federal judge, just like they would in a
> criminal investigation. So I want to be very clear. Some of the hype that
> we’ve been hearing over the last day or so — nobody’s listening to the content
> of people’s phone calls.[[16]]

The EFF provides compelling arguments as to why [metadata is important to our
privacy][24]. One such example: "They know you spoke with an HIV testing
service, then your doctor, then your health insurance company in the same hour.
But they don't know what was discussed." The EFF further states, "the
government has given no assurances that this data will never be correlated with
other easily obtained data". So, while the President may try reassuring us by
stating that "they've got to go back to a federal judge", he certainly does
not make it clear that they may already have enough information *without* having
to do so---from this supposedly non-content metadata. They do not need to
subpoena the phone company for the name or address of the individual in most
cases, as reverse telephone directories are readily available. With that, they
then have the names of yourself, everyone you have called and GPS data.

Another argument worthy of strong consideration is posed by Daniel J.
Solove---[what if the government is wrong about your intentions][25]? How can
you go about correcting incorrect data if its very existence is hidden from the
public?

> What if the government leaks the information to the public? What if the
> government mistakenly determines that based on your pattern of activities,
> you're likely to engage in a criminal act? What if it denies you the right to
> fly? What if the government thinks your financial transactions look odd—even
> if you've done nothing wrong—and freezes your accounts? What if the government
> doesn't protect your information with adequate security, and an identity thief
> obtains it and uses it to defraud you?[[25]]

These are serious questions. Even if you---the reader---are of the type that sates
"I don't care; I have nothing to hide", then consider that, despite the government's
best efforts to secure and protect the data, [it could possibly fall prey to
enemies of the United States][25]. Consider that the [Chinese cracked into
Pentagon systems][26], taking "designs for more than two dozen major weapon systems
used by the United States military".

Of course, we are now assuming that that the NSA is (a) operating in accordance with the
Court order with respect to the privacy of communications content and (b) that
the President's statement is not intentionally omitting projects that *do*
warrantlessly wiretap innocent Americans' communications. Historically, the NSA has not
given us reason to entertain either of these thoughts.

**January 31, 2006**---[Hepting v. AT&T][13]; the EFF files a case suing AT&T on
behalf of its customers for "violating privacy law by collaborating with the
NSA in the massive, illegal program to wiretap and data-min Americans'
communications". This case included "undisputed evidence" from former AT&T technician
Mark Klein showing that [AT&T routed a copy of all Internet traffic to an NSA-controlled
room in San Francisco][27]:

> Through the "splitter cabinet," the content of all of the electronic voice
> and data communications going across the Peering Links [...] was transferred
> from the WorldNet Internet room's fiber optical circuits into the
> [NSA-controlled] SG3 Secure Room [...] including such equipment as Sun servers
> and Juniper (M40e and M160) "backbone" routers.  The list also included a
> Narus STA 6400, which is a "Semantic Traffic Analyzer."[[27]]

That is---allegedly, AT&T indiscriminately passed *all* of the traffic passing
through its San Francisco facility into the NSA-controlled "SG3 Secure Room"
where the NSA performed their *own* filtering, storage and analysis however they
pleased. This is an astounding accusation. Additionally, Klein further states
that "other such `splitter cabinets' were being installed in other cities,
including Seattle, San Jose, Los Angeles and San Diego".[[27]]

Unfortunately, Hepting was dealt a fatal blow in July 2008 when both the
government and AT&T were [awarded retroactive immunity][28] by the [FISA
Amendments Act (FAA)][29]. This startling turn was signed by President Bush in
response to the EFF's court victories in the case and "allows the Attourney
General to require the dismissal of the lawsuits over the telecoms'
participation in the warrantless surveillance program".[[13]] The case was
dismissed in June 2009 and dozens of other lawsuits.

Fortunately, the battle is not over. The EFF then filed [Jewel v. NSA][12] which
directly targets the "NSA and other government agencies on behalf of AT&T
customers to stop the illegal unconstitutional and ongoing dragnet surveillance
of their communications and communications records". This case was too based
on [the testimony of Klein][27]. Additionally, the EFF had declarations of William
Binney, Thomas Drake and Kirk Wiebe---[three NSA whistleblowers][30]. Most
interesting (and damning) for the purposes of our discussion is the [Summary of
Voluminous Evidence][31].

> I have served on the Intelligence Committee for over a decade and I wish to
> deliver a warning this afternoon.  When the American people find out how their
> government has secretly interpreted [the business records provision of
> FISA], they are going to be stunned and they are going to be angry.[^32]
> --Senator Ron Wyden

Note that the Senator is referring to precisely the same provision---business
records---that was partly declassified by James Clapper on Thursday.[[23]] Of
course, we are assuming that the NSA decides to go to the FISA Court for
permission; this apparently has not always been the case.

According to [the summary of evidence][31], the NSA stated:

> To perform both its offensive and defensive mission, NSA must "live on the
> network." [The program would be] a powerful and permanent presence on a
> global telecommunications infrastructure where protected American
> communications and targeted adversary communications will coexist.

This certainly shares some similarities with the Verizon case. But FISA stood
in the way of this goal; John Yoo explains why FISA was insufficient for such
a dragnet operation:

> [U]nder existing laws like FISA, you have to have the name of somebody, have
> to already suspect that someone's a terrorist before you can get a warrant.
> [...] it doesn't allow you as a government to use judgment based on
> probability to say: "[...] there's a high probability that some of those
> calls are terrorist communications. But we don't know the names of the people
> making those calls." You want to get at those phone calls, those e-mails, but
> under FISA you can't do that.[^33] --Jon Yoo

After the September 11th attacks, "FISA ceased to be an operative
concern".[[31]] If that statement sounds unsettling, that is because it is;
President Bush subsequently authorized the NSA to "conduct electronic
surveillance within the United States" without an order from the FISA Court
(FISC). General Hayden phrased it as such: the program "is a more [...]
`aggressive' program than would be traditionally available under FISA".[^34]
What---if anything---does this mean about any current NSA operations (including
the Verizon order)? If Bush is able to authorize such actions, what is to say
that Obama will not (and has not)?

Let us return to the statements from both Clapper[[23]] and Obama stating that
"nobody is listening to the content of your phone calls".[[16]] We can certainly
hope that this is the case, but we shall continue to draw from evidence in the
[Jewel v. NSA case][12] to see what the NSA has done in the past.

> It was the biggest legal mess I've ever encountered.[^35] --Jack Goldsmith, Justice
> Department's Office of Legal Consel

The program operated "in lieu of" court orders.[^36] Even more alarming (if such a
thing is possible), "neither the President nor Attorney General approved the specific
interceptions; rather, the decision to listen or read particular communications was
made by intelligence analysts"; the only authorization needed was by an NSA
"shift supervisor".[^37] So, let's reiterate:

> **Obama:** If these folks — if the intelligence community then actually wants to listen
> to a phone call, they've got to go back to a federal judge, just like they
> would in a criminal investigation.[[16]]

It may very well be that Obama is being truthful within context of the Verizon
order; perhaps they have learned from their mistakes with the AT&T dragnet.
Unfortunately, their secrecy is making it very difficult for the public to make
an informed analysis of the matter.

Ultimately, it is believed that Attorney General Comey's initial certifications of
the program were "based on a misimpression of those activities" due to a botched
legal analysis by Jon Yoo that was described as "at a minimum [...] factually
flawed". Yoo was the only OLC official to read into the program since its
inception in October 2001 until his leaving in May 2003.[[31]] When Comey refused
to reauthorize the program, Bush did so himself, resulting in threats of resignation
from Comey and "about two dozen Bush appointees". However, "[d]espite the illegality
of the Program, no officials resigned."[[31]].

In 2009, the New York Times published a series of articles regarding the
program, exposing a ["serious issue involving the NSA" concerning
"significant misconduct"][38]. This included a "`flagrant' overcollection
of domestic email".[[31]]

> Because each court order could single out hundreds or even thousands of phone
> numbers or e-mail addresses, the number of individual communications that
> were improperly collected could number in the millions, officials said.[[31]]

That was then; this is now, right? How can we be sure of any connection between
the NSA of a decade ago vs. the NSA of today? Well, as an average citizen with
no security clearance, I can't. However, there are some important connections that
can be made. Firstly, recall Ron Wyden's quote above stating that the public
will be "stunned" and "angry".[^32] On Thursday, June 6th, he [released this
statement on his Senate website][39]:

> The program Senators Feinstein and Chambliss publicly referred to today is one
> that I have been concerned about for years.  I am barred by Senate rules from
> commenting on some of the details at this time.  However, I believe that when
> law-abiding Americans call their friends, who they call, when they call, and
> where they call from is private information.  Collecting this data about every
> single phone call that every American makes every day would be a massive
> invasion of Americans’ privacy.[[39]] --Senator Ron Wyden

Perhaps the most obvious and direct connection is that the [government asked for
more time in Jewel v. NSA (and Shubert v. Obama) in light of the NSA
revelations][40].

> The revelations not only confirmed what EFF has long alleged, they went even
> further and honestly, we’re still reeling. EFF will, of course, be continuing
> its efforts to get this egregious situation addressed by the courts.
>
> [...] EFF and others had long alleged that, despite the rhetoric surrounding
> the Patriot Act and the FISA Amendments Act, the government was still
> vacuuming up the records of the purely domestic communications of millions of
> Americans.  And yesterday, of course, with the Verizon order, we got solid
> proof..  And it appears that the reach of this vacuum goes much further, into
> the records of our Internet service providers as well.[[41]] --Electronic
> Frontier Foundation

This brings us back to [PRISM][19]. Numerous sources reported that [the White
House confirmed][42] its existence. Indeed, if you consider the President's
original words--- "the programs that have been discussed over the last couple
days in the press are secret in the sense that they’re classified"[[16]]---this
does seem to be a verification of the project's existence. However, confusion ensued
when [companies like Google and Facebook denied involvement][43], despite what
the [leaked information seems to state][19]. Yonatan Zunger---chief architect at
Google---[reiterated the words of Larry Page][44]:

> I can also tell you that the suggestion that PRISM involved anything happening
> directly inside our datacenters surprised me a great deal; owing to the nature
> of my work at Google over the past decade, it would have been challenging --
> not impossible, but definitely a major surprise -- if something like this
> could have been done without my ever hearing of it. And I can categorically
> state that *nothing* resembling the mass surveillance of individuals by
> governments within our systems has ever crossed my plate.[[44]] --Yonatan
> Zunger, Chief Architect, Google

Questions then arose as to what exactly "PRISM" is. Marc Ambinder with The Week
reported that [PRISM is nothing more than one of many different "data collection
tools"][45] that may be used by the NSA. One day later, Marc posted another article
entitled ["Solving the mystery of PRISM"][46]

> Each data processing tool, collection platform, mission and source for raw
> intelligence is given a specific numeric signals activity/address designator,
> or a SIGAD. [...] PRISM is US-984XN. Each SIGAD is basically a collection
> site, physical or virtual; [...] PRISM is a kick-ass GUI that allows an
> analyst to look at, collate, monitor, and cross-check different data types
> provided to the NSA from internet companies located inside the United States.[[46]]

Others hypothesized that, due to the denial of involvement from various
companies[[44]], PRISM may operate by intercepting communications. The Guardian
[countered by releasing another slide from the leaked presentation][47], stating
outright that "[b]oth of these theories appear to be contradicted by internal
NSA documents".

> It clearly distinguishes Prism, which involves data collection from servers,
> as distinct from four different programs involving data collection from "fiber
> cables and infrastructure as data flows past".[[47]]

This sounds a great deal like Klein's description of the SG3 Secure Room at
AT&T[[27]] (though I do not intend to imply that they are the same thing---that is
not clear, nor does Klien state that he ever noted the word "PRISM" on any
documents). The Guardian goes on to state that "[a] far fuller picture of the exact
operation of Prism [...] is expected to emerge in the coming weeks and months".
(Is that foreshadowing or an educated guess?)

There is, of course, the other obvious hypothesis---that organizations including
Google, Facebook and Microsoft are being [deceptive or not telling the whole
truth][48]. Alternatively, maybe such operations were being done under the noses
of executives. On Friday, the New York Times published an article stating that
the technology companies ["cooperated at least a bit"][49].

> [Google, Micorsoft, Yahoo, Facebook, AOL, Apple and Paltalk] were legally
> required to share the data under the Foreign Intelligence Surveillance Act.
> [...] But instead of adding a back door to their servers, the companies were
> essentially asked to erect a locked mailbox and give the government the key,
> people briefed on the negotiations said.  Facebook, for instance, built such a
> system for requesting and sharing the information, they said.[[49]]

This does not necessarily mean that these companies had any knowledge,
specifically, of "PRISM". As the Guardian said, I will be curious to see what
information surfaces in the coming months; the gag provisions of the orders make
for an unfortunate situation for everyone involved.

Let us return to the President's statements.

> **Obama:** And I welcome this debate. And I think it's healthy for our
> democracy. I think it's a sign of maturity, because probably five years ago,
> six years ago, we might not have been having this debate.[[16]]

This is a difficult debate to have, Mr. President, when the public does not know
of the existence of these programs; we only have knowledge of these programs due
to the aforementioned leaks---courageous individuals who feel that their
government is not representative of the democracy and freedom that it supposedly
represents. This segues into another statement from the President:

> **Jackie Calmes:** Do you welcome the leak, sir? Do you welcome the leak if
> you welcome the debate?
>
> **Obama:** I don't---I don't welcome leaks, because there's a reason why these
> programs are classified. [...] But that's also why we've set up congressional
> oversight. These are the folks you all vote for as your representative in
> Congress, and they’re being fully briefed on these programs.

Unfortunately, Obama seems to have missed another critical fact. We---the
people---vote for representatives that, well, "represent" *the issues that we
care about*. Those who are strongly opposed to gun legislation will vote for
those representatives that share those feelings and will fight to oppose such
legislation. Similarly, a pro-life supporter will probably not vote for a
candidate in favor of abortion. But what if there is a candidate that shares one
opinion but not another---say, opposes gun regulation but supports abortion,
when you as a voter are a pro-life gun-owner against gun legislation? Then you
will likely vote for the issues that you feel most strongly about (or what you
feel is a fair balance between all the other issues you follow). The problem
here, Mr. President, is that we---the people---are not made aware of these
issues because they are *classified*. How many people may not have voted for
you, Mr. President, had they known that you would support dragnet surveillance
of innocent Americans?

**Sunday, June 9th, 2013**---The Guardian continues to surprise the world by
[releasing the name of the NSA whistleblower at his request][50]. Edward
Snowden, a 29-year-old former CIA technical assistant and current defense
contractor employee is responsible for what The Guardian is calling "the
biggest intelligence leak in the NSA's history". Reporting from Hong
Kong---where Snowden fled to on May 20th in the hope of resisting the
U.S. government---Glenn Greenwald, Ewen MacAskill and Laura Poitras report
on his motives.

> Three weeks ago, Snowden made final preparations [...] [a]t the NSA office in
> Hawaii where he was working, [copying] the last set of documents he intended
> to disclose.[[50]]

Snowden describes situations where he began to begin questioning his government,
such as a case where a CIA operative purposely encouraged a Swiss banker to get
intoxicated and drive drunk so that he would be arrested. "Much of what I saw
in Geneva really disillusioned me about how my government functions and what its
impact is in the world." He mentioned that the election of Obama in 2008 gave
him hope for reform, but watched in 2009 as "Obama advanced the very policies
that I thought would be reined in. [...] I got hardened."[[50]]

It is this statement from Snowden that, if accurate, suggests that Obama not
only supports Bush's initial dragnet operation[[31]], but has further expanded it.

At this point, since the news is still quite young at the time that this article
was written, the world must wait to see what action the government will attempt
to take against Snowden. Reuters had already reported the previous day that
[the government is likely to open a criminal probe into the NSA leaks][51].

> James Clapper, the director of U.S. national intelligence, condemned the leaks
> and asserted that the news articles about PRISM contained "numerous
> inaccuracies."[[51]]

Snowden is not the first to come forward as a whistleblower from the NSA---as we
discussed previously, three NSA whistleblowers came fourth previously to back the
EFF in Jewel v. NSA;[[30]] they each had the charges either cleared or dropped. That
said, [Obama has been aggressively pursuing whistleblowers][59]. Snowden
mentioned that he views his best hope of freedom as the possibility of asylum
with Iceland.[[50]] It appears that such may already be working in his favor, with
[Iclandic Legislator Birgitta Jonsdottir already starting the process to apply
for asylum][52], although it is not clear if Snowden has already applied.

There is a great deal to think about. Even though the [evidence against the NSA
dates far back][4], the recent revelations invoke emotions that are difficult to
describe. With countless individuals working to sift through the information,
the Obama administration under attack and nobody knowing if the Guardian is
sitting on even more information, the entire world will continue to watch
impatiently...and act.

While all this is going on, it would be useful to reiterate certain privacy and
security topics that have already been covered at large. Firstly, consider
checking out the EFF's [Surveillance Self-Defense][53] website, which contains
information on a number of topics including anonymity and how to respond to
court orders. Consider using [Tor for anonymity][54] online (but recognize that
it is not a full solution in itself). Consider [keeping your data to
yourself][55] rather than storing it on "cloud" services---[Richard Stallman
explains how Software as a Service (SaaS) differs in dangers from proprietary
software][56]. Consider using only [free software][57] to limit further
sacrifices in personal freedom and to limit the information that corporations
and third parties collect from you while using your computer and other devices.
Finally, if you have information that you want to leak to the press (whether or
not you are an [NSA employee][58]), you may be able to consider tools such as
[The New Yorker's Strongbox][60]; it uses [software created by Aaron Swartz][61]
shortly before his untimely death early this year.

Finally, aid senators like Rand Paul in developing [legislation to curb the powers
of the government][62]. We must also do our best to fight for the rights of
brave whistleblowers like Snowden. To end with the words of the EFF, ["we need
a new church committee and we need it now"][41].

[5]: http://www.guardian.co.uk/world/2013/jun/06/nsa-phone-records-verizon-court-order
    "NSA collecting phone records of millions of Verizon customers daily"
[6]: http://s3.documentcloud.org/documents/709012/verizon.pdf "PDF of the FISA Court order to Verizon."
[7]: http://s3.documentcloud.org/documents/709012/verizon.txt "Ibid; plain text version."
[8]: https://www.eff.org/deeplinks/2013/06/confirmed-nsa-spying-millions-americans
     "Confirmed: NSA Spying on Millions of Americans"
[9]: https://www.eff.org/deeplinks/2011/10/ten-years-later-look-three-scariest-provisions-usa-patriot-act
     "Three Scariest Provisions of thet USA Patriot Act"
[10]: /2013/03/federal-judge-rules-nsls-national-security-letters-unconstitutional
      "Federal Judge Declares National Security Letters Unconstitutional"
[11]: http://www.theatlantic.com/politics/archive/2013/06/what-we-dont-know-about-spying-on-citizens-scarier-than-what-we-know/276607/
     "Bruce Schneier comments on NSA leak"
[12]: https://www.eff.org/cases/jewel "Jewel v. NSA"
[13]: https://www.eff.org/cases/hepting "Hepting v. AT&T"
[14]: /2012/11/privacy-in-light-of-the-petraeus-scandal
      "Privacy In Light of the Petraeus Scandal"
[15]: /2013/03/google-says-the-fbi-is-secretly-spying-on-some-of-its-customers
      "Google Says the FBI Is Secretly Spying on Some of Its Customers"
[16]: http://blogs.wsj.com/washwire/2013/06/07/transcript-what-obama-said-on-nsa-controversy/
      "Obama on the NSA controversy"
[17]: https://www.eff.org/deeplinks/2013/05/congressional-outrage-over-ap-phone-records
      "Congressional outrate of AP phone records"
[18]: https://www.eff.org/deeplinks/2013/05/doj-subpoena-ap-journalists-shows-need-protect-calling-records
[19]: http://www.guardian.co.uk/world/2013/jun/06/us-tech-giants-nsa-data
[20]: http://googleblog.blogspot.com/2013/06/what.html "Larry Page denies PRISM involvement"
[21]: https://www.facebook.com/zuck/posts/10100828955847631 "Mark Zuckerberg denies PRISM involvement"
[22]: http://www.guardian.co.uk/world/2013/jun/07/google-facebook-prism-surveillance-program
[23]: http://www.dni.gov/index.php/newsroom/press-releases/191-press-releases-2013/868-dni-statement-on-recent-unauthorized-disclosures-of-classified-information
      "James Clapper---Directory of National Intelligence---declassifies
       information pertaining to the "business records" provision of FISA"
[24]: https://www.eff.org/deeplinks/2013/06/why-metadata-matters
      "The EFF describes why telephony metadata can have a significant impact on our privacy."
[25]: http://mashable.com/2013/06/08/china-hack-nsa/ "What if crackers get a hold of the NSA's databases?"
[26]: http://rt.com/usa/us-chinese-report-defense-888/ "The Chinese crack into Pentagon systems."
[27]: https://www.eff.org/file/28823 "Public unredacted Mark Klein declaration"
[28]: https://www.eff.org/pages/case-against-retroactive-amnesty-telecoms "The Case Against Retroactive Amnesty for Telecoms."
[29]: http://www.govtrack.us/congress/bills/110/hr6304/text "FISA Amendments Act (FAA)."
[30]: https://www.eff.org/press/releases/three-nsa-whistleblowers-back-effs-lawsuit-over-governments-massive-spying-program
      "Three NSA whistleblowers back the EFF in Jewel v. NSA"
[31]: https://www.eff.org/node/72021 "Summary of Voluminous Evidence, Jewel v. NSA"
[38]: http://www.nytimes.com/2009/04/16/us/16nsa.html?pagewanted=all "Officials Say U.S. Wiretaps Exceeded Law"
[39]: http://www.wyden.senate.gov/news/press-releases/wyden-statement-on-alleged-large-scale-collection-of-phone-records
      "Ron Wyden comments on the collection of Verizon phone records"
[40]: https://www.eff.org/deeplinks/2013/06/government-asks-more-time-eff-surveillance-cases
      "In Light of NSA Revelations, Government Asks for More Time in EFF Surveillance Cases"
[41]: https://www.eff.org/deeplinks/2013/06/response-nsa-we-need-new-church-commission-and-we-need-it-now
      "In Response to the NSA, We Need A New Church Committee and We Need It Now"
[42]: http://www.theweek.co.uk/us/53475/white-house-admits-it-has-access-facebook-google
      "White House admits it has "access" to Facebook, Google"
[43]: http://www.guardian.co.uk/world/2013/jun/07/google-facebook-prism-surveillance-program
      "Facebook and Google insist they did not know of Prism surveillance program"
[44]: https://plus.google.com/+YonatanZunger/posts/huwQsphBron
      "Yonatan Zunger---Chief Architect at Google---expresses his distaste of PRISM"
[45]: http://theweek.com/article/index/245311/sources-nsa-sucks-in-data-from-50-companies
      "Sources: NSA sucks in data from 50 companies"
[46]: http://theweek.com/article/index/245360/solving-the-mystery-of-prism
      "Solving the mystery of PRISM"
[47]: http://www.guardian.co.uk/world/2013/jun/08/nsa-prism-server-collection-facebook-google
      "NSA's Prism surveillance program: how it works and what it can do."
[48]: http://www.guardian.co.uk/world/2013/jun/08/obama-response-nsa-surveillance-democrats
      "Obama deflects criticism over NSA surveillance as Democrats sound alarm."
[49]: http://www.nytimes.com/2013/06/08/technology/tech-companies-bristling-concede-to-government-surveillance-efforts.html?ref=global-home&_r=2&pagewanted=all&
      "Tech Companies Concede to Surveillance Program"
[50]: http://www.guardian.co.uk/world/2013/jun/09/edward-snowden-nsa-whistleblower-surveillance
      "Edward Snowden: the whistleblower behind the NSA surveillance revelations."
[51]: http://www.reuters.com/article/2013/06/08/us-usa-security-leaks-idUSBRE95700C20130608
      "Government likely to open criminal probe into NSA leaks: officials."
[52]: http://www.forbes.com/sites/andygreenberg/2013/06/09/icelandic-legislator-im-ready-to-help-nsa-whistleblower-seek-asylum/
      "Icelandic Legislator: I'm Ready To Help NSA Whistleblower Edward Snowden Seek Asylum"
[53]: https://ssd.eff.org/ "EFF Surveillance Self-Defense."
[54]: https://www.torproject.org/ "The Tor project offers anonymity online."
[55]: http://www.guardian.co.uk/technology/2008/sep/29/cloud.computing.richard.stallman
      "Cloud computing is a trap, warns GNU founder Richard Stallman"
[56]: http://www.gnu.org/philosophy/who-does-that-server-really-serve.html
      "Who does that server really serve?"
[57]: http://www.gnu.org/philosophy/free-sw.html "What is free software?"
[58]: http://www.whistleblowers.org/index.php?option=com_content&task=view&id=984&Itemid=173
      "National Security Employees Know Your Rights"
[59]: http://www.theatlanticwire.com/politics/2011/05/obamas-war-whistle-blowers/38106/
      "Obama's War on Whistle-Blowers"
[60]: http://www.newyorker.com/strongbox/ "The New Yorker Strongbox"
[61]: http://www.newyorker.com/online/blogs/newsdesk/2013/05/strongbox-and-aaron-swartz.html
      "Strongbox and Aaron Swartz"
[62]: http://abcnews.go.com/blogs/politics/2013/06/rand-paul-bill-would-curb-nsa-on-phone-records/
      "Rand Paul Bill Would Curb NSA on Phone Records"

[^32]: Ibid.[[31]] 157 Cong. Rec. S3372--3402, S3386 (May 26, 2011) [Vol. VI, Ex. 111, p. 4286]
       (Statement of Sen.  Ron Wyden, On Patriot Act Reauthorization)
[^33]: Ibid.[[31]] PBS Frontline, Spying on the Homefront, Interview with John C. Yoo at 4
       (Jan. 10, 2007) [Vol. I, Ex. 10, p. 394]
[^34]: Ibid.[[31]] Press Briefing by Att’y Gen. Alberto Gonzalez and Gen. Michael Hayden,
       Principal Dep. Dir. for Nat’l Intelligence (Dec. 19, 2005)
[^35]: Ibid.[[31]] Preserving the Rule of Law in the Fight Against Terror:
       Hearing before the S. Comm. on the Judiciary, 110th Cong. 7 (Oct. 2, 2007)
       [Vol.  III, Ex. 42, p. 1307] (testimony of Jack Goldsmith)
[^36]: Ibid.[[31]] Press Briefing by Att’y Gen. Alberto Gonzalez and Gen. Michael Hayden, Principal Dep. Dir.
       for Nat’l Intelligence (Dec. 19, 2005)
[^37]: Ibid.[[31]] Remarks by Gen. Michael Hayden, Address to the National Press Club, Washington, D.C. (Jan. 23, 2006)
       [Vol.  IV, Ex. 73, p. 1809]
