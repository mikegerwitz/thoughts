# Always use -t with ssh-add (and always set passwords on your ssh keys)

Many people use SSH keys for the sole purpose of avoiding password entry when
logging into remote boxes. That is legtimate, especially if you frequently run
remote commands or wish to take advantage of remote tab complation, but creating
a key with an empty password is certainly the wrong approach---if an attacker
gets a hold of the key, then they have access to all of your boxes before you
have the chance to notice and revoke the key.

<!-- more -->

ssh-agent exists for this purpose. The problem is---creating an agent only to
place the key in memory indefinately is also a terrible idea. If your system
does become compromised and the attacker is either root access or access as your
user, then they can simply connect to the ssh-agent (unless it's password
protected) and start using your key. Also consider that, should you leave your
box unattended for even a moment without locking it (for whatever reason---shit
happens), an attacker could gain physical access to your PC (and an attacker may
just be a coworker looking to play a prank).

Every morning at work, I begin the day by typing ssh-add followed by an
appropriate lifetime (be it the duration of the work day, or the duration that I
think I will need the key). This way, your key is in memory when you are likely
to be physically present at the box and it is automatically removed from memory
after a given lifetime. Additionally, I like to add `ssh-add -D` to the script
that locks my PC when I walk away from my desk: that will immediately clear all
keys from memory, just in case.
