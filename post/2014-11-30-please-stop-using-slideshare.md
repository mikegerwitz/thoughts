# Please stop using SlideShare

There are many great presentations out there---many that I enjoy
reading, or that I would enjoy to read.  Unfortunately, many of them
are hosted on SlideShare, which requires me to download proprietary
JavaScript.

[JavaScript programs require the same freedoms as any other
software][0].  While SlideShare does (sometimes/always?) provide a
transcript in plain text---which is viewable without JavaScript---this
is void of the important and sometimes semantic formatting/images that
presenters put much time into; you know: the actual presentation bits.
(I'm a fan of plain-text presentations, but they each have their own
design elements).

[0]: https://www.gnu.org/software/easejs/whyfreejs.html

There are ways around this.  SlideShare's interactive UI appears to
simply be an image viewer, so it is possible to display all sides
using a fairly simple hack:

<!-- more -->

```javascript
Array.prototype.slice.call(
  document.getElementsByClassName( 'slide' ) )
    .forEach( function( slide ) {
      slide.classList.add( 'show' );

      var img = slide.getElementsByClassName( 'slide_image' )[0];
      img.src = img.dataset.full;
    } );
```

This will display all slides inline.  But there's a clear problem with
this: how is the non-JS-programmer supposed to know that?  Even
JavaScript programmers have to research the issue in order to come up
with a solution.

But ideally, I'd like to download the presentation PDF.  SlideShare
does offer a download link, but not only does it not work with
JavaScript disabled, but it requires that the user create an account.
This is no good, as it can be used to track users or discover
identities by analyzing viewing habits.  This would allow
de-anonymizing users, even if they have [taken measures to remain
anonymous][1].

(By the way: at the time that I wrote this post, the [EFF's
Surveillance Self-Defense Guide][1] is [LibreJS compatible][2] and the
JavaScript code that it runs is mostly free.)

I encourage presenters (and authors in general) to release the slides
in an [unencumbered document format][3], like PDF, HTML, OpenDocument,
or plain text.  Those formats should be hosted on their own website,
or websites that allow downloading those files without having to
execute proprietary JavaScript, and without having to log in.  If
those authors *must* use SlideShare for whatever reason, then they
should clearly provide a link to that free document format somewhere
that users can access without having to execute SlideShare's
proprietary JavaScript, such as on the first slide.  (The description
is iffy, since it is truncated and requires JavaScript to expand.)

[1]: https://ssd.eff.org/
[2]: https://www.gnu.org/software/librejs/
[3]: http://www.fsf.org/campaigns/opendocument/reject
