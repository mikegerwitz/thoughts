# All these election attack ads are utterly useless

There have been a lot of elections going on lately---local, state and national.
The majority of those ads are attack ads: immature and disrespectful; if you
want my vote, give me something positive to vote for instead of spending all of
your time and money attacking your candidate. If my vote is to go to the "least
horrible" candidate, then there is no point in voting at all.

<!-- more -->

Even more frustrating is the deceptiveness of the ads---intentional
deceptiveness, nonetheless. And these are the ads that many in the United States
will be basing the majority of, if not all, of their vote on come election time
(how many will realistically research instead of sitting in front of the TV
absorbing all of the useless bullshit that they are spoonfed?).

Frightening.
