# Tor exit node operator raided in Austria

[These things][0] mustn't be allowed to happen; they are an affront to privacy.
Tor exit node operators should not have to fear conviction for activities they
themselves did not perform.

[0]: http://www.lowendtalk.com/discussion/6283/raided-for-running-a-tor-exit-accepting-donations-for-legal-expenses

<!-- more -->
