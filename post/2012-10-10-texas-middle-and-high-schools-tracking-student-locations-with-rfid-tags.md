# Texas middle and high schools tracking student locations with RFID tags

[An article][0] describes how a school district in Texas is attempting to force
its students to wear RFID tags at all times in order to track their location to
"stem the rampant truancy devastating the school's funding".

[0]: http://rt.com/usa/news/texas-school-id-hernandez-033/

What?

<!-- more -->

This is deeply concerning. Not only does this raise serious security and privacy
concerns (as mentioned near the end of the article), but it also costed the
schools over a half a million dollars to implement. In order words: Texas
taxpayer money has been wasted in an effort to track our children.

Good thing they don't have anything [better to spend that money on.][1]

[1]: http://fedupwithlunch.com/

