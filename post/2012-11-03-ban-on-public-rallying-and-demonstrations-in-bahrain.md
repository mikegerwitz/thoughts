# Ban On Public Rallying and Demonstrations in Bahrain

The government of Bahrain found that the best solution to preventing violent
protests was to [ban all public rallying and demonstrations][0].

[0]: https://www.eff.org/deeplinks/2012/11/bahrain-goes-bad-worse

<!-- more -->
