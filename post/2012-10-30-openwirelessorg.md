# OpenWireless.org

The EFF [announces the launch of openwireless.org][0], which encourages users to
[share their network connections][1] to create a global network of freely
available wireless internet access.

This is a noble movement. This reminds me of a point in history when MIT began
password protecting their accounts, which were previously open to anyone.
Stallman, disagreeing with such a practice, [encouraged users to create empty
passwords][2]. Stallman would even give out his account information so that
remote users may log into MIT's systems, all with good intent.

[0]: https://www.eff.org/deeplinks/2012/10/why-we-have-open-wireless-movement
[1]: https://www.openwireless.org/
[2]: http://shop.fsf.org/product/free-as-in-freedom-2/

<!-- more -->

Of course, with malice rampant in today's very different world, Stallman's
actions, although noble, would be both naive and a huge security risk.
Fortunately, [opening your wireless network isn't necessarily one of these
risks][3] and, if done properly, does not equate to opening your private network
to attack.

Consider using [DD-WRT][4] as your router's firmware, if supported by your
device, as it is itself [free software][5].

[3]: https://openwireless.org/myths
[4]: http://dd-wrt.com
[5]: http://www.gnu.org/philosophy/free-sw.html
