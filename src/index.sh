#!/bin/bash
# Generate index HTML page
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The index page consists of post abstracts, some static text, and the
# static header and footer.  All post metadata files must have been built,
# along with `post/list'.
#
# This script includes the static body (see `main').
##

set -euo pipefail


# Get the file name of the Nth most recent post.  This relies on the
# existence of `post/list'.
pnfile()
{
  local -ri n=${1?Missing relative post number}

  sed "${n}q;d" post/list
}


# Read field FIELD from post metadata recfile FILE.
pmeta()
{
  local -r file=${1?Missing file name}
  local -r field=${2?Missing field name}

  recsel -P "$field" "$file"
}


# Process each numeric argument using `abstract'.  Each argument must be a
# relative post number (see `pnfile').
abstracts()
{
  while [ $# -gt 0 ]; do
    abstract "$1"
    shift
  done
}


# Generate HTML for relative post number N (see `pnfile').
abstract()
{
  local -ri n=${1?Missing relative post number}

  local file title date slug body
  file=$( pnfile "$n" )
  title=$( pmeta "$file" subject )
  date=$( pmeta "$file" date )
  slug=$( pmeta "$file" slug )
  body=$( pmeta "$file" abstract )

  cat <<EOF
<article class="abstract">
  <h2 class="title"><a href="/$slug">$title</a></h2>
  $body
  <p class="date">Posted on $date.
    <a href="/$slug">Read more &raquo;</a>
  </p>
</article>
EOF
}


# Generate index HTML page.
# TODO: Factor out static sections.
main()
{
  src/mkheader index

  cat <<EOF
<h1 id="latest-posts">Latest Posts</h1>
<section class="asideable" aria-labelledby="latest-posts">
  $( abstracts {1..2} )
</section>

<aside>
  <ul class="links">
    <li><a class="box free-sw" href="https://www.gnu.org/philosophy/free-sw.en.html">What is Free/Libre Software?</a></li>
    <li><a class="box eff-privacy" href="https://www.eff.org/issues/privacy">EFF on Privacy</a></li>
  </ul>
</aside>

<section class="highlight">
  <h1 class="title">Adopting Free Software Ideals</h1>

  <aside>
    This is a talk about practical ethics and ideals.  It contains some
    awkward discussions that free software activists like to avoid, and
    hopes to guide those seeking to adopt more free software ideals, but
    fear they may not be able to meet such high standards.  It's a talk
    about evolution and growth.
  </aside>

  <a href="/talks#afsi" class="lp-watch">Watch LibrePlanet&nbsp;2021 Talk</a>
  <br clear="both" />
</section>

<section class="compact sm">
  <h1 id="older-posts">Older Posts</h1>

  $( abstracts 3 4 )

<section class="highlight">
  <h1 class="title">The Surreptitious Assault on Privacy, Security,
    and Freedom</h1>

  <aside>
    Each of these essential rights are being surreptitiously
    assaulted; only the most technical among us even know what to look
    for, let alone how to defend ourselves. Governments, corporations,
    and groups of ill-minded individuals are spying and preying upon
    both users and bystanders with unprecedented frequency and
    breadth.
  </aside>

  <a href="/talks#sapsf" class="lp-watch">Watch LibrePlanet&nbsp;2017 Talk</a>
  <br clear="both" />
</section>

  <div></div>
  $( abstracts {5..8} )

  <a class="view-all" href="/posts">View all posts</a>
</section>
EOF

  cat src/footer.tpl.htm
}


main "$@"
