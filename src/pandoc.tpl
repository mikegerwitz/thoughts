$for(include-before)$
$include-before$

$endfor$
$if(toc)$
$toc$

$endif$

<article>
$body$

$if(tags)$
<section class="tags" aria-label="Tags">
  <h2>Tags</h2>
  <ul class="tags">
    $for(tags)$
    <li>$tags$</li>
    $endfor$
  </ul>
</section>
$endif$
</article>

$for(include-after)$

$include-after$
$endfor$
