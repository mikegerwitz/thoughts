# Google Says the FBI Is Secretly Spying on Some of Its Customers

A Wired article mentions [figures released from Google][0] regarding National
Security Letters issued by the NSA under the Patriot Act. It is too early to
comment in much detail on this matter (I would like to wait for commentary from
the EFF), but, as the article mentions:

[0]: http://www.wired.com/threatlevel/2013/03/google-nsl-range/?cid=co6199824

> Google said the number of accounts connected to National Security letters
> ranged between “1000-1999″ for each of the reported years other than 2010. In
> that year, the range was “2000-2999.”

<!-- more -->

The [EFF provides additional information, including recommendations on what to
do about such requests][1] via their Surveillance Self-Defense website. As
quoted from that website:

> And it's even worse for FISA subpoenas, which can be used to force anyone to
> hand over anything in complete secrecy, and which were greatly strengthened
> by Section 215 of the USA PATRIOT Act. The government doesn't have to show
> probable cause that the target is a foreign power or agent — only that they
> are seeking the requested records "for" an intelligence or terrorism
> investigation. Once the government makes this assertion, the court must
> issue the subpoena.

To add insult to injury:

> FISA orders and National Security Letters will also come with a gag order that
> forbids you from discussing them. Do NOT violate the gag order. Only speak to
> members of your organization whose participation is necessary to comply with
> the order, and your lawyer.

[1]: https://ssd.eff.org/foreign/fisa
