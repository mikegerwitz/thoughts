# Another crack at medical device cracking

My previous post mentioned the dangers of running non-free software on implanted
medical devices. While reading over RMS' policital notes[0], I came across [an
article mentioning how viruses are rampant on medical equipment][1].

> "It's not unusual for those devices, for reasons we don't fully understand, to
> become compromised to the point where they can't record and track the data,"
> Olson said during the meeting, referring to high-risk pregnancy monitors.

The devices often run old, unpatches versions of Microsoft's Windoze operating
system. The article also mentions how the maleware often attempts to include its
host as part of a botnet.

[0]: http://stallman.org/archives/2012-jul-oct.html#18_October_2012_%28Computerized_medical_devices_vulnerable_to_viruses%29
[1]: http://www.technologyreview.com/news/429616/computer-viruses-are-rampant-on-medical-devices/

<!-- more -->

This is deeply concerning and incredibly dangerous. As non-free software is used
more and more in equipement that is responsible for our health and safety, we
are at increased risk for not only obvious software flaws, but also for crackers
with malicious intent; harming someone will become as easy as instructing your
botnet to locate and assassinate an individual while you go enjoy a warm (or
cold) beverage.

These problems are *less likely* (not impossible) to occur in free software
beacuse the users and community are able to inspect the source code and fix
problems that arise (or hire someone that can)[2]. In particular, in the case of
the hospitals mentioned in [the article][1], they would be free to hire someone
to fix the problems themselves rather than falling at the mercy of the
corporations who supplied the proprietary software.

[2]: http://www.gnu.org/philosophy/free-sw.html
