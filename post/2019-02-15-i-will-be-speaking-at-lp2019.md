# I Will Be Speaking At LibrePlanet 2019
Please join me [this year at LibrePlanet][0] for my talk,
  titled "Computational Symbiosis: Methods that Meld Mind and Machine".

[0]: https://libreplanet.org/2019

<!-- more -->

> Words like "wizardry" and "incantation" have long been used to describe
> skillful computational feats.  But neither computers nor
> their users are performing feats of magic; for systems to think, we must
> tell them how.
>
> Today, users most often follow a carefully choreographed workflow that
> thinks _for_ them, limited by a narrow set of premeditated
> possibilities.  But there exist concepts that offer virtually no limits on
> freedom of expression or thought, blurring the distinction between "user"
> and "programmer".
>
> This session demonstrates a range of practical possibilities when
> machine acts as an extension of the user's imagination, for the technical
> and nontechnical alike.

For my previous three years' talks, see my [Talks page](/talks).

We will be gathering once again in the Stata Center at MIT.
I'm excited to see everyone there!
