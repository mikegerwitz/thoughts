# DNA Collection

Consider a recent article from the EFF [regarding "Rapid DNA Analyzers"][0].
The article poses the potetial issues involved, but also consider that any DNA
collected (if not destroyed) would violate not just your privacy, but your
entire blood line. What if DNA from immigrants were collected? Much of that
information is inherited, so generations down the line, your privacy is still
violated.

[0]: https://www.eff.org/deeplinks/2012/12/rapid-dna-analysis

<!-- more -->

I cannot comment intelligently on the matter since I haven't read deeply enough
into the proposed storage/hashing/etc policies, but those polices can be abused
and such data can be leaked. I highly oppose any sort of DNA collection outside
of personal at-home use (when the technology is available with free software)
and use by medical professionals for personal medical reasons so long as the
institution performing the test can provide stringent evidence of its
destruction. But even then, if law enforcement somehow got a hold of the DNA
before it were destroyed, then the problem still exists, so it would be best if
you had your own personal tools to analyze your own DNA and distribute only the
portions that were required (and encryption tools like [GPG][1] could be used
for distribution).

One day, but not now. Let's make those scanners affordable and run free
software.

[1]: http://www.gnupg.org/
