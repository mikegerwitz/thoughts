# Abolishing Patents

My issue with patents exceeds the [obvious case against software patents][0];
indeed, I have long pondered the problems with patents in other fields. When I
hear the phrase "patent pending" or "patented technology" touted in ads, I
have never thought positive thoughts; instead, I have thought "you are damning
this otherwise excellent work to stagnation". What if someone has an excellent
idea to improve upon that particular product? Well, they'd better be prepared to
jump through some hoops or shell out some hefty licensing fees. Or maybe it's
just easier to abandon the idea entirely and forget that it had never happened.

[0]: http://patentabsurdity.com/

<!-- more -->

However, I thought, it's not a simple case of ridding the world of patents.
How would that affect the incentive to innovate? How would people recoup
expensive R&D costs, especially in industries like pharmacy (both my parents are
pharmacists)? What about the incentive to describe your invention to the world?
Then again, nobody *has* to get a patent for their invention. It may be worth
keeping it secret if nobody can figure it out.

The answers to all of these questions appeared in one place: [The Case Against
Patents][1], which I found referenced in an article regarding the [Swedish Pirate
Party's opinions on patents, trademarks and copyright][2]. While it is still a
draft at the time of this writing, I encourage you to give it a read, as it is
very enlightening.

[1]: http://research.stlouisfed.org/wp/2012/2012-035.pdf
[2]: http://falkvinge.net/2012/10/13/what-the-swedish-pirate-party-wants-with-patents-trademarks-and-copyright/
