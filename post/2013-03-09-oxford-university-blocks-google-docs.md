# Oxford University Blocks Google Docs

Oxford University decided to [block Google Docs][0] last month due to phishing
attacks against its users. To quote the blog post:

[0]: http://blogs.oucs.ox.ac.uk/oxcert/2013/02/18/google-blocks/

> Almost all the recent attacks have used Google Docs URLs, and in some cases
> the phishing emails have been sent from an already-compromised University
> account to large numbers of other Oxford users. Seeing multiple such incidents
> the other afternoon tipped things over the edge. We considered these to be
> exceptional circumstances and felt that the impact on legitimate University
> business by temporarily suspending access to Google Docs was outweighed by the
> risks to University business by not taking such action.

<!-- more -->

This incident was brought to my attention by [a blog post by Schneier][1], in
which he referenced his [essay on "feudal security"][2] (I commented in more
detail on this essay in [my response to a previous blog post of
his][3].[^blog]) In this case, Oxford is trusting that it knows better than its
users and has the right to exercise this power over them in light of their
inexperience with handling these situations (or even recognizing them).

This may very well be the case---the Oxford IT department probably does have a
better understanding of security than many of their users. However, by blocking
access to Google Docs, they are also blocking access to millions of legitimate
articles hosted there, which is far from acceptable. Oxford is more than just a
workplace---for which many would argue these actions are acceptable; it is a
university that should encourage freedom of expression. They simply must find a
better way of dealing with these problems. If a user falls victim to a phishing
attack within Oxford, they will likely fall victim outside of it.

Would Oxford consider blocking e-mail access too (where phishing attacks are
very cheap and common)?

> We appreciate and apologise for the disruption this caused for our users.
> Nevertheless, we must always think in terms of the overall risk to the
> University as a whole, and we certainly cannot rule out taking such action
> again in future [...]

N.B.: Google Docs is proprietary and I cannot recommend its use any more than I
can recommend use of Microsoft Office.

[1]: https://www.schneier.com/blog/archives/2013/03/oxford_universi.html
[2]: https://www.schneier.com/essay-406.html
[3]: /2013/01/re-who-does-skype-let-spy

[^blog]: (I posted a link to my response on his blog, but he did not approve the comment.)

