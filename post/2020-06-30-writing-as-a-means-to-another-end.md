# Writing As a Means to Another End

To anyone who's looked at the number of posts I've made in the past few
  years on this blog,
    it may surprise you to learn that I do a lot of writing.
It's just that the majority if it is never read by anyone other than myself.
When I write---%
  as I am now---%
  I certainly _intend_ for others to read it.
But that's not usually what happens.

Writing articles is a means to an end.
But the end isn't always the written word.
Writing is a journey,
  and sometimes it leads far from where one may expect.

<!-- more -->

If I'm going to spend the time writing something,
  I want it to be thorough and compelling.
I want facts and references,
  and I want them to be good ones.
I don't want to have to go back and correct inaccuracies,
  because then I will have lead you astray.[^immutable]
I want concrete evidence to back up each and every claim I make,
  so I can prove to you (or maybe it's to myself) that I really do know what
    I'm talking about,
      without question.

[^immutable]: In fact,
    earlier versions of this blog had posts as commit messages,
    making them very difficult to change,
      since I didn't want to rewrite history.

Let's say I'm writing about a topic that I have over a decade of experience
  with.
As I formalize my thoughts and describe this particular thing,
  I'm forced to rationalize to you---the reader---everything.
And sometimes I find that,
  even though I have strong _practical_ knowledge of something,
  I may lack sufficient understanding of certain theory or consequences.
So I start digging.
And before I know it,
  I've amassed too many [yaks][] to possibly shave within a
  lifetime.[^reading-list]

[yaks]: https://projects.csail.mit.edu/gsb/old-archive/gsb-archive/gsb2000-02-11.html

[^reading-list]: This is evidenced by my (private) reading list,
  which I literally cannot finish within this lifetime at my current pace,
  and which grows faster than I can consume it.

So the solution seems simple:
  skip the formality.
Some information is better than none, right?
It'd still help others.

Ah, but it would make obvious to others that maybe I don't know what I'm
  talking about.
What others may see as an informative work,
  I see as a laying bare everything I _don't_ know.
Everything I've yet to learn.
And if I have so much to learn,
  why am I writing about it?

That's nonsense,
  of course---%
    some of the _best_ information I've gotten was from candid articles
    written by people who are _still learning_ about the topic at hand.
It's wonderful reading about their thoughts, experiences, and---%
  most importantly---%
  their _struggles_.
So why don't I do the same?
I know full well that most readers will never notice the inadequacies that
  clinch so piercingly my attention.

I believe my behavior is best represented by something called
  [Imposter Syndrome][].
At risk of getting too deep into this topic and therefore not publishing
  this post,
    I'm going to keep light on the details and let you do the
    research.[^rabbit-hole]
But what essentially happens is paradoxical---%
  my quest for knowledge only proves to me how much more I have to learn and
    how little I know,
      creating this never-ending, unstatisfiable, ravenous feedback loop.
Since writing a good article (in my mind) is predicated on having a certain
  foundation,
    and having a foundation requires its own foundation,
    this recursive process has no end.
This is thrilling,
  but the end result is that articles never get finished.
And one day when I return to find them,
  months or years later,
  I've discovered so much that the only proper way to finish them is to
  start all over again.
And so the cycle continues.
I am,
  and never will be,
  good enough for you,
    dear reader.

[Imposter Syndrome]: https://en.wikipedia.org/wiki/Impostor_syndrome

[^rabbit-hole]: And yet,
    just in providing that single Wikipedia link,
    I jumped down that familiar rabbit hole and had to pull myself out.
  "Should I publish an article claiming Imposter Syndrome unless I can
    articulate fully and fluently all relevant details?"

This problem doesn't just manifest with my writing---%
  it happens with my [free software][] projects too.
They all die for the same reason,
  or barely get off of the ground to begin with.

[free software]: https://www.gnu.org/philosophy/free-sw.en.html

I said that writing is a journey.
As it turns out,
  it's mostly a selfish one,
  at least for me.
Because the end result isn't often an article suitable for publication;
  it's a wonderful and deeply personal collection of experiences.
I have learned so much through this process of unattainable self-betterment.
I have met so many good people.
And, in retrospect, I have bested the best I thought I could be.
Yet,
  despite the evidence all around me that I am in fact a competent person,
  I can't bring myself to sincerely _believe_ it.
Despite my _own admission_ of besting the best I thought I could be,
  I can't finish typing this paragraph without wanting to delete that
  sentence.

So now,
  having observed this over the years,
  I exploit it to my benefit---%
    sometimes I write simply for the sake of starting that journey,
      knowing that it'll lead me somewhere magnificent.
And one day,
  if I can bring myself to actually bring these articles to publication,
  I'll take you on that journey with me,
    and hopefully I'll be able to share even a fraction of that magnificence
    with you.[^epilogue]

[^epilogue]: Clearly I finished this article.
  In a sitting,
    just as I expected to.
  And the reason for this is important,
    I think:
      this article is a manifestation of my inner feelings.
  I'm not trying to convince you of anything.
  I'm simply speaking from the heart,
    and there's little getting in the way of that.
  The _goal_ of this piece is to emphasize my shortcomings.
  That's easy---%
    those come cheap.
  Maybe that's something I can exploit more often too.
