# Meltdown/Spectre and the Web

The recently-released [Meltdown][] and [Spectre][] CPU timing attacks
  affect virtually every user in some way;
    the consequences are profound.
There are plenty of good write-ups on the topic,
  so I don't feel the need to re-iterate the technical details here.
(See an easily digestible one [from the Raspberry Pi][rpi] project, and an
  in-depth analysis [from Project Zero][zero].)

[Meltdown]: https://meltdownattack.com/
[Spectre]: https://spectreattack.com/
[rpi]: https://www.raspberrypi.org/blog/why-raspberry-pi-isnt-vulnerable-to-spectre-or-meltdown/
[zero]: https://googleprojectzero.blogspot.com/2018/01/reading-privileged-memory-with-side.html

What I do want to draw attention to is that these attacks [are exploitable
  via web browsers][mozilla].

[mozilla]: https://blog.mozilla.org/security/2018/01/03/mitigations-landing-new-class-timing-attack/

<!-- more -->

The reason for this is that your web browser,
  by default,
  automatically downloads and executes programs without your knowledge or
    consent.
Most commonly,
  web pages embed software in the form of JavaScript code.
Because of the features available in modern JavaScript environments,
  CPU cache timing attacks are possible.

[I spoke about the security issues][lp2016] of running these programs in your web
  browser back in 2016---it
    was a bad idea then,
      and it's still a bad idea now.
[I spoke further of privacy issues][lp2017] last year at LibrePlanet 2017.
I encourage you to use extensions like [NoScript][] to block the execution of
  JavaScript by default,
    and stop random people from treating your computer as a puppet to do
    their own bidding.

[lp2016]: https://media.libreplanet.org/u/libreplanet/collection/restore-online-freedom/
[lp2017]: https://media.libreplanet.org/u/libreplanet/m/the-surreptitious-assault-on-privacy-security-and-freedom/
[NoScript]: http://noscript.net/
