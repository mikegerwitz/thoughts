# NYC Master Keys

[Bruce Schneier summarizes in a blog post][0] a disturbing topic regarding a New
York City locksmith selling "master keys" on eBay, providing access to various
services such as elevators and subway entrances.

[A discussion about this blog post on Hacker News][1] yielded some interesting
conversation, including an [even more disturbing article describing how simple
it may be to create master keys][2] for a set of locks given only the lock, its
key and a number of attempts.

[0]: http://www.schneier.com/blog/archives/2012/10/master_keys.html
[1]: http://news.ycombinator.com/item?id=4654777
[2]: http://www.crypto.com/masterkey.html

<!-- more -->

I'll let you ponder the implications of both of these topics. Here's something
to get you started: organized crime could use these keys to effectively evade
law enforcement or break into millions of "locked" homes. Crackers could gain
intimate access to various city systems whereby they may be able to further
obstruct or infect systems. A security system is only as strong as its weakest
link. Keeping citizens in the dark about these issues gives them a dangerous and
false sense of security.
