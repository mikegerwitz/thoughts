# Re: FreeBSD, Clang and GCC: Copyleft vs. Community

I recently received a comment via e-mail from a fellow GNU hacker Antonio
Diaz, who is the author and maintainer of [GNU Ocrad][0], a [free (as in
freedom)][1] optical character recognition (OCR) program. His comment was in
response to my article entitled [FreeBSD, Clang and GCC: Copyleft vs.
Community][2], which details the fundamental difference in philosophy
between free software and "open source".

[0]: https://www.gnu.org/software/ocrad/ocrad.html
[1]: https://www.gnu.org/philosophy/free-sw.html
[2]: /2013/08/freebsd-clang-and-gcc-copyleft-vs-community

I found Antonio's perspective to be enlightening, so I asked for his
permission to share it here.

<!-- more -->

> I imagine a world where all the Free Software is GPLed. The amount and
> usefulness of Free Software grows incesantly because free projects can
> reuse the code of previous free projects. Proprietary software is
> expensive because every company has to write most of its "products" from
> scratch. Most people use Free Software, and proprietary software is mainly
> used for specialized tasks for which no free replacement exists yet.
>
> Now I imagine a world where all the Free Software is really "open source"
> (BSD license). Free Software is restricted to the operating system and
> basic aplications because the license does not guarantee reciprocity.
> Proprietary software is cheap to produce because it is built using the
> code of free projects, but it is expensive for the user (in money and
> freedom) because there is no real competition from Free Software. Most
> people use proprietary software, as Free Software is too basic for most
> tasks.
>
> I think "open source" organizations (specially BSD) are wilfully
> destroying the long-term benefits for society of the GPL, and they are
> doing it for short-term benefits like popularity and greed:
>
> "As these companies devise strategies for dealing with GPLv3, so must the
> FreeBSD community - strategies that capitalize on this opportunity to
> increase adoption of FreeBSD." "Fundraising Update [...] This has
> increased the number of people actively approaching companies to make
> large contributions."
>
> https://www.freebsdfoundation.org/press/2007Aug-newsletter.shtml
>
> Human beings have an innate sense of justice. In absence of reciprocity
> one wants to be paid, but I think that reciprocity is much better for
> society in the long term.[^3]

Antonio compels us to think toward the future: while developers releasing
their code under permissive licenses like the [Modified BSD License][4] are
still making a generous contribution to the free software community today,
it may eventually lead to negative consequences by empowering non-free
software tomorrow.

[^3]: Comment by Antonio Diaz; the only modifications made were for
formatting.

[4]: https://www.gnu.org/licenses/license-list.html#ModifiedBSD
