# Copyright Assignment Of Free Software Projects

An [e-mail today from Paolo Bonzini][0], a maintainer of GNU sed, has prompted
additional discussion regarding copyright assignment to corporate entities; in
particular, the discussion focuses on copyright assignment to the FSF under the
GNU project.

[0]: http://article.gmane.org/gmane.comp.lang.smalltalk.gnu.general/7873

<!-- more -->

An [article by Michael Kerrisk on LWN.net][1], posted a couple days earlier,
touches on the [same issue brought up by GnuTLS earlier in the month][2]. The
disagreements from the two aforementioned individuals of the GNU-maintained
projects prompt a thoughtful analysis of whether copyright assignment is
appropriate for your own free software project[1]. In contrast, consider the
[developer certificate of origin][3] policy adopted by the Linux project, under
which contributors maintain copyright for their contributions.

There are benefits and downsides to both models---if a project requires
copyright assignment (such as the GNU projects), then enforcement and license
modifications are simplified. As an example, if the Linux project wanted to move
to the GPLv3, they would have to contact each contributor (a similar move was
done recently [by the VLC project][4], except that they moved from the GPL to
the LGPL). However, the Linux project has a much smaller barrier to entry---they
need not [assign copyright of their contributions to the project (such as is the
case with GNU)][5], meaning that individuals may be more likely to contribute.

One of the major benefits touted by the FSF for copyright assignments from
contributors is [copyright enforcement][6]---another complication that would
arise from enforcing the GPL in a project such as Linux. That said, as the LWN
article mentions[2], what if [the FSF cannot find the time to enforce the
copyright on a project violation][7]? Then again, what of the flipside---do you
have the time or money to enforce violations on your own projects were they not
assigned to a corporation like the FSF?

These are interesting discussions and certainly things that should be considered
when determining how to handle both contributions and the copyright for your
entire project. Ultimately, that decision falls on you, the author/maintainer,
and your needs.

(Disclaimer: I am an associate member of the Free Software Foundation. This
article does not reflect any of my personal opinions; whether or not I would
assign copyright to the FSF for any of my projects would be determined based on
the goals and plan of that particular project.)

[1]: http://lwn.net/SubscriberLink/529522/854aed3fb6398b79/
[2]: http://lwn.net/Articles/529558/
[3]: http://elinux.org/Developer_Certificate_Of_Origin
[4]: http://mikegerwitz.com/thoughts/2012/11/VLC-s-Move-to-LGPL.html
[5]: http://git.savannah.gnu.org/cgit/gnulib.git/tree/doc/Copyright/assign.changes.manual#n64
[6]: http://www.gnu.org/licenses/why-assign.html
[7]: http://lwn.net/Articles/529777/
