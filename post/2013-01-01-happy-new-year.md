# Happy New Year

The greatest excitement in moving into a new year is the prospect of quantified
growth.

Of course, it also means another year to look forward to the health of those you
care for.

<!-- more -->
