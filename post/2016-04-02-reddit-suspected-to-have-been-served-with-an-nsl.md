# Reddit suspected to have been served with an NSL

It is suspected that Reddit has been [served with an NSL][schneier].
[National Security Letters (NSLs)][nsl] are subpoena served by the United
States federal government and often come with a gag order that prevents the
recipient from even stating that they received the letter.

[schneier]: https://www.schneier.com/blog/archives/2016/04/reddits_warrant.html
[nsl]: https://en.wikipedia.org/wiki/National_Security_Letter

<!-- more -->

[Warrant canaries][canary] are used to circumvent gag orders by stating
that requests have *not* been received, under the [legal theory][court]
that, while courts can compel persons not to speak, they can't compel them
to lie.  [Reddit's canary has died][reddit-report]---the canary is absent
from their most recent 2015 transparency report, where it was [present in
the 2014 report][reddit-report-2014].

Does this mean that you should stop using Reddit?  No; canaries are an
important transparency method.  If you are worried about your privacy, you
shouldn't disclose the information to a third party to begin with.  Note
that this includes metadata that are gathered about you when you, for
example, browse subreddits while logged in.  You can help mitigate that by
[browsing anonymously using Tor][donot], being sure never to log in during
the same session.

The website [Canary Watch][cw] is a website that tracks warrant canaries.

I'm awaiting further analysis after the weekend.

[canary]: https://en.wikipedia.org/wiki/Warrant_canary
[cw]: https://www.canarywatch.org/
[court]: https://gigaom.com/2014/10/10/are-warrant-canaries-legal-twitter-wants-to-save-techs-warning-signal-of-government-spying/
[reddit-report]: https://web.archive.org/web/20160331210850/https://www.reddit.com/wiki/transparency/2015
[reddit-report-2014]: https://web.archive.org/web/20160331204815/https://www.reddit.com/wiki/transparency/2014
[donot]: https://www.whonix.org/wiki/DoNot
