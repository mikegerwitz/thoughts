# Copyright Reform? You're silly.

Amazingly, the Republican Study Committee (RSC) had [released a report
suggesting copyright reform][0]. Of course, that's a silly thing to do when
you're in bed with organizations like the MPAA and RIAA; [the report was quickly
retracted][1].

It would have been a surprising step forward; maybe there's hope yet, assuming
the GOP can get a handle on itself.

(Disclaimer: I have no party affiliation.)

[0]: http://www.techdirt.com/articles/20121116/16481921080/house-republicans-copyright-law-destroys-markets-its-time-real-reform.shtml
[1]: http://www.techdirt.com/articles/20121117/16492521084/hollywood-lobbyists-have-busy-saturday-convince-gop-to-retract-copyright-reform-brief.shtml

<!-- more -->
