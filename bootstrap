#!/bin/bash
# Prepares build environment
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This will also download any necessary third-party files.  Note that all
# downloads are proxied over Tor (using `torify').
##

set -euo pipefail

# Source fonts (Apache 2.0)
declare -rA fonts=(
  [OpenSans-Regular.woff]=https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0d.woff
  [OpenSans-Light.woff]=https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OUuhv.woff
  [OpenSans-SemiBold.woff]=https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOUuhv.woff
)

declare -r tpimagesdir=images/tp
declare -r fontdir=fonts


# Download third-party images.  This not only keeps them out of the
# repository, but explicitly states in a reproducible manner how the images
# were manipulated (if at all).
get-images()
{
  echo 'retrieving third-party images...'

  ( cd "$tpimagesdir" && ./gen-makefile > Makefile )
  make -C "$tpimagesdir" all check
}


# Download and verify fonts and license.
get-fonts()
{
  local font src dest

  echo 'retrieving font files...'
  for font in "${!fonts[@]}"; do
    src=${fonts[$font]}
    dest="$fontdir/$font"

    test ! -f "$dest" || continue
    torify wget "$src" -O "$dest"
  done

  # Verify that we haven't been served bad files.  This should only happen
  # in the case of network failure or a malicious host, since the above URLs
  # reference the commit hash.
  echo 'verifying font files...'
  ( cd "$fontdir" && sha512sum -c SHA512SUM )
}


# Bootstrap.
main()
{
  get-images
  get-fonts
}


main "$@"
