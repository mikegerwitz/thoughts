# Don't force me to use your tools [on the Web]

There was an interesting discussion on [libreplanet-discuss][] recently
  regarding web interfaces.
Below is a rather informal off-the-cuff statement regarding the use of Web
  interfaces (specificlaly Discourse) over my own tools.

[libreplanet-discuss]: https://lists.gnu.org/archive/html/libreplanet-discuss/2017-06/msg00032.html

<!-- more -->

-----

I live a huge chunk of my life in my mail client
  (which happens to be my editor as well).
It's scripted,
  heavily customized,
  and integrated with other things.
I do task management with Org mode,
  which integrates simply but well enough with Gnus.
I can use my editor keybindings and such when composing messages.
The same goes with my IRC client.
I never have to leave home, if you will.

Contrast that with websites:
  if I have to write anything substantial,
    I often have to write it in my editor first and paste it in.

Many of us hackers don't care for flashy interfaces;
  we'd rather use the tools we've invested our lives into and know well.
  Tools that can compose and work well in pipelines.
Trying to use interfaces that reinvent the wheel poorly is painful.
And let's not be fooled---these are programs.
Especially when they're heavy on JavaScript.
There's no difference between this and someone asking me to download Foo and
  put my Emacs toy away, as cute as it is.

But I know that many people don't feel that way.
I have coworkers that think I'm crazy (respectfully so).
And I think they're crazy too. ;)
Admittedly, using your own tools is a large barrier to entry---my
  tools are useful because I've spent a great deal of time learning and
    researching and customizing.
And now I can reuse them for everything.
For your average user looking to get into activism,
  who may not even be a programmer,
  that's a bit different;
    it's easier to say "here's your single tool (Web)---go use it".

There are systems that allow for a level of integration
  (e.g. mailing lists and forums).
But they're often treated as fallbacks---as second-class citizens.
They might provide a subset of features;
  it leaves certain members of the community out---those
    who want to use their own tools.

I haven't used Discourse.
I do see "mailing list support";
  maybe that's a good sign.
But one of the phrases at the top of the features page is
  "[w]e're reimagining what a modern discussion platform should
  be".
Many of us don't want to see it reimagined.
That's the opposite of what many want.

Trying to strike a balance isn't a bad thing if that's the audience
  we're looking to attract.
But it's difficult,
  and something I struggle with a great deal.

-----

tl;dr:
  Asking someone to use an interface on the Web is asking them to use
    /your/ program instead of their own.
  Be respectful by using [Web standards for accessibility][accessibility];
    [progressive enhancement][];
    and make use of well-established standards with rich histories,
      especially if your audience makes use of them
      (e.g. mailing lists, RSS feeds, federation standards, etc).

Thank you.

[accessibility]: https://en.wikipedia.org/wiki/Web_accessibility
[progressive enhancement]: https://en.wikipedia.org/wiki/Progressive_enhancement
