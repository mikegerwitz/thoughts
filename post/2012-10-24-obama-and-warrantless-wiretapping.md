# Obama and Warrantless Wiretapping

The EFF has released an article with a [plethora of links describing warrantless
wiretapping under the Obama administration][0], spurred by Obama's response to
Jon Stewart's questioning on The Daily Show last Thursday. (Readers should also
be aware of the [NSA spy center][1] discussed earlier in the year, as is
mentioned in the EFF article.)

[0]: https://www.eff.org/deeplinks/2012/10/fact-check-obamas-misleading-answer-about-warrantless-wiretapping-daily-show
[1]: http://www.wired.com/threatlevel/2012/03/ff_nsadatacenter/

<!-- more -->

It is clear that the United States government has no intent on protecting the
freedoms of individuals and instead is actively resisting attempts to correct
the problems. While we can hope that this will change, and we can be confident
that organizations like the EFF will continue to fight for our liberties, one
immediate option is to limit as much as possible what the NSA and other agencies
can discover about you. Consider using [Tor][2] for all of your network traffic
(at the very least, use HTTPS connections to prevent agencies and ISPs from viewing
specific web pages on a particular domain; HTTPS is unnecessary if using Tor.)
PGP/GPG can be used to encrypt e-mail messages to the intended recipients. Etc.

It's unfortunate that such precautions are necessary. Privacy is important even
if you have nothing to hide; any suggestion to the contrary is absolutely
absurd.

[2]: http://torproject.org
