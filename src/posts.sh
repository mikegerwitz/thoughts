#!/bin/bash
# Generate posts HTML page
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The generated page contains the abstracts of _all_ posts; this may get
# unwieldy over time.
#
# TODO: Maybe refactor common abstract logic with `index.sh' and
# `papers.sh'?
##

set -euo pipefail

# Last generated yet (see `abstract-from').
declare -i lastyear=0


# Get the file name of the Nth most recent post.  This relies on the
# existence of `post/list'.
pnfile()
{
  local -ri n=${1?Missing relative post number}

  sed "${n}q;d" post/list
}


# Read field FIELD from post metadata recfile FILE.
pmeta()
{
  local -r file=${1?Missing file name}
  local -r field=${2?Missing field name}

  recsel -P "$field" "$file"
}


# Generate HTML for relative post number N (see `pnfile').
abstract-from()
{
  local -r file=${1?Missing post file name}

  local title date slug body
  title=$( pmeta "$file" subject )
  date=$( pmeta "$file" date )
  slug=$( pmeta "$file" slug )
  body=$( pmeta "$file" abstract )

  local -ri year=${date:0:4}

  if [ $year -ne $lastyear ]; then
    test $lastyear -eq 0 || echo '</section>'
    lastyear=$year

    cat <<EOF
<section class="compact">
<h1>$year</h1>
EOF
  fi

  cat <<EOF
<article class="abstract">
  <h2 class="title"><a href="$slug">$title</a></h2>
  $body
  <p class="date">Posted on $date.
    <a href="$slug">Read more &raquo;</a>
  </p>
</article>
EOF
}


# Generate posts page.
main()
{
  src/mkheader posts Posts

  local file
  while read file; do
    abstract-from "$file"
  done < post/list

  echo '</section><br class="end" />'

  cat src/footer.tpl.htm
}


main "$@"
