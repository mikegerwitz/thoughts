# Federal Appeals Court Declares "Defense of Marriage Act" Unconstitutional

A step in the [right direction.][0]

It should also be noted that New York State had also [legalized same sex
marriage back in July of 2011][1]---a move I was particularily proud of as a
resident of NY state.

[0]: http://www.aclu.org/lgbt-rights/federal-appeals-court-declares-defense-marriage-act-unconstitutional
[1]: http://en.wikipedia.org/wiki/Same-sex_marriage_in_New_York

<!-- more -->
