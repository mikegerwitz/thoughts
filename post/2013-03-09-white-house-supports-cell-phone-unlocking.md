# White House Supports Cell Phone Unlocking

Earlier this week, the starter of the [White House petition to "Make Unlocking
Cell Phones Legal"][0] posted a [thread on Hacker News][1] stating that the
White House had officially responded, stating:

> The White House agrees with the 114,000+ of you who believe that consumers
> should be able to unlock their cell phones without risking criminal or other
> penalties. In fact, we believe the same principle should also apply to
> tablets, which are increasingly similar to smart phones. And if you have paid
> for your mobile device, and aren't bound by a service agreement or other
> obligation, you should be able to use it on another network. It's common
> sense, crucial for protecting consumer choice, and important for ensuring we
> continue to have the vibrant, competitive wireless market that delivers
> innovative products and solid service to meet consumers' needs.

<!-- more -->

The petition---as stated in the above response---garnered over 114,000
signatures. The response is exciting news because the Library of Congress had
[removed the phone unlocking exemption][2] at the beginning of this year. (As
the EFF points out, [this may not necessarily mean that unlocking your phone is
"illegal"][3]).

However, although this response is getting a lot of attention (I was surprised
to see my local news station report on it), this is not yet cause for
celebration; it is my hope that the White House will now follow through with
this statement and act upon it appropriately.

(The [EFF has also posted their own comments on the White House's response][4].)

This is just one issue in [a string of problems that is the DMCA][5].

[0]: https://petitions.whitehouse.gov/petition/make-unlocking-cell-phones-legal/1g9KhZG7
[1]: https://news.ycombinator.com/item?id=5319577
[2]: /2013/01/phone-unlocking-once-again-illegal
[3]: https://www.eff.org/is-it-illegal-to-unlock-a-phone
[4]: https://www.eff.org/deeplinks/2013/03/white-house-supports-unlocking-phones-real-problem-runs-deeper
[5]: https://www.eff.org/wp/unintended-consequences-under-dmca

