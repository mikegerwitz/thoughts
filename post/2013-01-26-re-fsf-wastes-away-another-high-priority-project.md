# Re: FSF Wastes Away Another "High Priority" Project

A couple days ago, my attention was drawn to an article on Phoronix that
[criticized the FSF for its decision to stick with GPLv3 over GPLv2 on
LibreDWG][0] due to the number of projects that make use of it---licensed under
the GPLv2---under [a now incompatible][1] license. This article is very negative
and essentially boils down to this point (the last paragraph):

> Unless the Free Software Foundation becomes more accomodating [sic] of these
> open-source developers -- who should all share a common goal of wanting to
> expand free/open-source software -- LibreDWG is likely another project that
> will ultimately waste away and go without seeing any major adoption due to
> not working with the GPLv2.

It worth mentioning why this view is misguided (though understandable for
those who adopt the ["open source" philosophy over that of software
freedom][2]).

[0]: http://www.phoronix.com/scan.php?page=news_item&px=MTI4Mjc
[1]: http://www.gnu.org/licenses/gpl-faq.html#WhatDoesCompatMean
[2]: http://www.gnu.org/philosophy/open-source-misses-the-point.html

<!-- more -->

Let me start with [this paragraph from the Phoronix article][0]:

> The Free Software Foundation was contacted about making LibreDWG GPLv2+
> instead (since the FSF is the copyright holder), but the FSF/Richard Stallman
> doesn't the DWG library on the earlier version of their own open-source
> license.

The FSF's founding principle is that of [software freedom][3] (beginning with the
GNU project). Now, consider the reason for the creation of the GPLv3---the GPLv2
[could not sufficiently protect against][4] software patents and newer threats such
as "tivoization". These goals further the FSF's mission of ensuring---in
this case---that free software *remains* free ([a concept that RMS coined
"copyleft"][5]). It would make sense, then, that the FSF (and RMS') position is
that [it is important that we adopt the GPLv3 for our software][6].

From this perspective, it does not make sense to "downgrade" LibreDWG's
license to the GPLv2, which contains various bugs that have since been patched
in GPLv3---it is not pursuant to the FSF's goals. (Of course, not all agree with
the GPLv3; one such notable disagreement (as well as issues
stemming from copyright assignment) leaves the kernel Linux [perpetually licensed
under the GPLv2][7] since it does not contain the ["or later" clause][8]).

That is not to say that the author's concern is not legitimate---a number of
projects are licensed under the GPLv2 and therefore cannot use the newer (and
improved) versions of LibreDWG that are licensed under the GPLv3 (unless they
were to upgrade to the GPLv3, of course). Whether or not upgrading is feasible
(e.g., in the case of the kernel Linux, it is not) is irrelevant---let us
instead focus on the issue of adoption under the assumption that the project is
either unwilling or unable to make use of a library licensed under the GPLv3.

As aforementioned, [the author focuses on the issue of adoption][0]:

> LibreDWG is likely [...to] go without seeing any major adoption due to not
> working with the GPLv2

A focus on adoption is a [focus of "open source", not free software][2], the
latter of which the FSF represents. With a focus on software freedom, the goal
is to create software that respects the [users' four essential freedoms][9]; if
the software is adopted and used, great! However, freedom should never be
sacrificed in order to encourage adoption. One may argue that "downgrading" to
the GPLv2 is not sacrificing freedom because the software is still free (it is
even the GPL)---but it is important to again realize that the GPLv3 is "more
free" than the GPLv2 in the sense that it [*protects* additional freedoms][6];
so, while the GPLv2 isn't necessarily sacrificing users' freedoms directly, it
does have such an indirect effect through means of enforcement.

A reader familiar with GNU may then point out the LGPL---the Lesser General
Public License---under which popular (and very important) [libraries such as
glibc are licensed][10]. In fact, one could extend this argument to any
library---why not have LibreDWG licensed under the LGPL to avoid this problem in
its entirety, while still preserving the users' freedoms for that library in
itself? This understanding requires a brief lesson in history---the rationale
under which the LGPL was born. [To quote the GNU project][11]:

> Using the ordinary GPL is not advantageous for every library. There are
> reasons that can make it better to use the Lesser GPL in certain cases. The
> most common case is when a free library's features are readily available for
> proprietary software through other alternative libraries. In that case, the
> library cannot give free software any particular advantage, so it is better to
> use the Lesser GPL for that library.

It was for this reason that glibc was released under the LGPL---because it was
better to have the users adopt some sort of free software than none at all;
there were other alternatives that existed that users may flock to if they were
forced to liberate their own proprietary software (after all, the C API is also
standardized, so such a feat would be trivial). Now that glibc has since matured
greatly, it could be argued today that it has proved its usefulness and the LGPL
may no longer be necessary, but such a discussion is not necessarily relevant
for this conversation.

What is important is that [the FSF does not recommend the LGPL for most
libraries][11] because that would encourage proprietary software developers to
take advantage of both the hard work of the free software community and the
users of the software. Now, I cannot speak toward the alternatives to
LibreDWG---do there exist proprietary alternatives that are reasonable
alternatives to non-commercial projects? I do not have experience with the
library. However, I hope by this point the FSF's position has been rationalize
(even if you---the reader---do not agree with it).

Of course, this rationalization will still leave a sour taste in the mouth of
those "open source" developers (or perhaps even some free software developers)
that think in terms of what is "lost": these projects---which are themselves
free software and therefore beneficial to our community---cannot take advantage
of *other free software* due to this licensing issue. Since these projects had
already existed when LibreDWG was licensed under the GPLv2, the relicensing to
GPLv3 may seem unfair and, therefore, a "loss". It is difficult to counter
such an argument if the above rationale has not been sufficient; nor will I
argue that the situation is not unfortunate, should the projects be unable to
relicense. However, it must be understood that, to ensure the future of free
software, the FSF must adopt to combat today's threats and so too must other
free software projects.

The Phoronix article mentioned two projects in particular that suffer from
LibreDWG's relicensing: [LibreCAD and FreeCAD][0]. LibreCAD omits the "or later"
clause that was mentioned above, preventing them from easily migrating to the
GPLv2 (which is [against the FSF's recommendation][12]). Unless the project
requires that contributors assign copyright to the project owner, then they
would have to get permission from each contributor (or rewrite the code) in
order to change the license (which is not unheard of; [VLC had done so recently
to migrate from the GPL to the LGPL][13]); this is a significant barrier for any
project with multiple contributors, especially when your project is a derivative
work (of QCad).

The other project mention was FreeCAD, and the author of the article mentions
that the project depends on Coin3D and Open CASCADE, "both of which are
GPLv2", so [the project cannot migrate to GPLv3][0]. A quick look at Coin3D's
website shows that the software is actually licensed under the modified
(3-clause) BSD license, and so [migrating to the GPLv3 is not an issue][15]. Open
CASCADE has its own "public license" that I do not have the time to evaluate
(nor am I lawyer, so I do not wish to give such advice), so I cannot speak to
its compatibility with the GPLv3. That said, I'm unsure if it would be a barrier
toward FreeCAD's adoption of the GPLv3.

Ultimately, the moral of the story is to plan for the *future*---if you use a
project licensed under the GPL, ensure that it has the "or later" clause that
allows it to be licensed under later version of the GPL, since you can be sure
that the FSF and many other free software developers will be quick to adopt the
license. Of course, many may not be comfortable with such a licensing decision:
you effectively are giving the FSF permission to relicense you work by simply
releasing a new version of the GPL. It is your decision whether you are willing
to place this kind of trust in the organization responsible for starting the
free software movement in the first place.

Readers may now assume that I am placing the entire blame and onus on the
implementors of LibreDWG. The onus, perhaps, but not the blame---this truly is
an unfortunate circumstance that takes away from hacking a free software
project. Unfortunately, the projects are stuck in a bad place, but the FSF is
not to blame for standing firm in their ideals. Instead, this can be thought of
as a maintenance issue---rather than a source code refactoring resulting from a
library API change, we instead require a "legal code" refactoring resulting
from a "legal API" change.

[3]: http://www.fsf.org/about/
[4]: http://www.gnu.org/licenses/quick-guide-gplv3.html
[5]: http://www.gnu.org/copyleft/
[6]: http://www.gnu.org/licenses/rms-why-gplv3.html
[7]: http://lwn.net/Articles/200422/
[8]: http://www.gnu.org/licenses/gpl-faq.html#v2v3Compatibility
[9]: http://www.gnu.org/philosophy/free-sw.html
[10]: http://www.gnu.org/licenses/lgpl.html
[11]: http://www.gnu.org/licenses/why-not-lgpl.html
[12]: http://www.gnu.org/licenses/gpl-howto.html
[13]: http://mikegerwitz.com/thoughts/2012/11/VLC-s-Move-to-LGPL.html
[14]: https://bitbucket.org/Coin3D/coin/wiki/Home
[15]: http://www.gnu.org/licenses/license-list.html#ModifiedBSD
[16]: http://www.opencascade.org/getocc/license/

