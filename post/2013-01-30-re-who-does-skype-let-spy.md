# Re: Who Does Skype Let Spy?

Today, [Bruce Schneier brought attention to privacy concerns surrounding
Skype][0], a very popular ([over 600 million users][1]) VoIP service that has
since been acquired by Microsoft. In particular, [users are concerned over what
entities may be able to gain access to their "private" conversations][1]
through the service---Microsoft has refused to answer those kinds of questions.
While the specific example of Skype is indeed concerning, it raises a more
general issue that I wish to discuss: The role of free software and SaaS
(software as a service).

[0]: http://www.schneier.com/blog/archives/2013/01/who_does_skype.html
[1]: http://www.skypeopenletter.com/

<!-- more -->

To [quote Schneier][0]:

> We have no choice but to trust Microsoft. Microsoft has reasons to be
> trustworthy, but they also have reasons to betray our trust in favor of other
> interests. And all we can do is ask them nicely to tell us first.

Schneier continues to admit, in similar words, that [we are but "vassals" to
these entities and that they are our serfs][2]. His essays regarding the [power of
corporations and governments over their users][3] echo the words of Lawrence
Lessig in his [predictions of a "perfectly regulated" future made possible by
the Internet][4]. While Lessig (despite what his critics have stated in the
past) seems to have been correct in many regards, we need not jump into the
perspective of an Orwellian dystopia where we are but "vassals" to the
Party.[^5] Indeed, this is only the case---at least at present---if you choose to
participate in the use of services such as Skype, as ubiquitous as they may be.

Skype is a useful demonstration of the unfortunate situation that many users
place themselves in by trusting their private data to Microsoft. Skype itself is
proprietary---we cannot inspect its source code (easily) in order to ensure that
it is respecting our privacy. (Indeed, as a user on [the HackerNews
discussion][6] pointed out, [Skype has installed undesirable software in the
past][7].) If Skype were [free software][8], we would be able to inspect its
source code and modify it to suit our needs, ensuring that the software did only
what we wanted it to do---ensuring that Microsoft was not in control of us.

However, even if Skype were free software, there is another issue at work that
is often overlooked by users: Software as a Service (SaaS). When you make use of
services that are hosted on remote servers (often called "cloud"
services)---such as with Skype, Facebook, Twitter, Flickr, Instagram, iTunes,
iCloud and many other popular services---you are blindly entrusting your data to
them. Even if the Skype software were free (as in freedom), for example, [we
still cannot know what their servers are doing with the data we provide to
them][9]. Even if Skype's source code was plainly visible, the servers act as a
black box. Do they monitor your calls? [Does Facebook abuse your data?][10] How is
that data stored---[what happens][1] in the event of a data breach, or in the event
of a warrant/subpoena?

The only way to be safe from these providers is to [reject these services
entirely and use your own software on your own PC][9], or use software that will
connect directly to your intended recipient without going through a 3rd
party. (Never mind your ISP; that is a separate issue entirely.) If you must
use a 3rd party service, ensure that you can adequately encrypt your
communications (e.g. using GPG to encrypt e-mail communications)---something
that may not necessarily be easy/possible to do, especially if the software is
proprietary and works against you.

The EFF has published [useful information on protecting yourself against
surveillance][11], covering topics such as encryption and anonymization.

If we are to resist the worlds that [Lessig][4] and [Schneier][3] describe, then we
must [stand up for our right to privacy and demand action][12]. [Who will have
your back][13] when we're on the brink of ["perfect regulation"][4]; who will
stand up for your rights and work *with* you---not against you---to preserve
your liberties? Without this push, services like Skype empower governments and
other entities to work toward perfect regulation---to continuously spy on
everything that we do. With everyone putting their every thought and movement on
services like Facebook, [Twitter][14] and Skype, the Orwellian Thought Police have
the ability to manifest in a form that not even Orwell could have
imagined---unless it is stopped.

To help [preserve your ever-dwindling rights online][15], consider becoming a
member of or participating in the campaigns of the [Free Software
Foundation][16], [Electronic Frontier Foundation][17], the [American Civil
Liberties Union][18] or any other organizations dedicated toward free society.

(Disclaimer: I am a member of the Free Software Foundation.)

[2]: http://www.schneier.com/essay-406.html
[3]: http://www.schneier.com/essay-409.html
[4]: http://codev2.cc/
[6]: http://news.ycombinator.com/item?id=5139801
[7]: http://blogs.skype.com/garage/2011/05/easybits_update_disabled_for_s.html
[8]: http://www.gnu.org/philosophy/free-sw.html
[9]: http://www.gnu.org/philosophy/who-does-that-server-really-serve.html
[10]: https://www.eff.org/deeplinks/2013/01/facebook-graph-search-privacy-control-you-still-dont-have
[11]: https://ssd.eff.org
[12]: https://www.eff.org/deeplinks/2013/01/its-time-transparency-reports-become-new-normal
[13]: https://www.eff.org/pages/when-government-comes-knocking-who-has-your-back
[14]: https://www.eff.org/deeplinks/2013/01/google-twitters-new-transparency-report-shows-increase-government-demands-sheds
[15]: https://action.eff.org/o/9042/p/dia/action/public/?action_KEY=8750
[16]: http://www.fsf.org/register_form?referrer=5804
[17]: https://supporters.eff.org/donate
[18]: https://www.aclu.org/donate/join-renew-give

[^5]: Orwell, George. Nineteen Eighty-Four. ISBN 978-0-452-28423-4.

