# Stingrays: Cell Phone Privacy and Warrantless Surveillance

How would you feel if law enforcement showed up in your living room, demanded
your cell phone, and started writing down your call history and text messages?
How would you feel if you didn't even know that they were in your home to begin
with, let alone stealing private data? [This is precisely what is happening when
law enforcement uses "Stingrays" to locate individuals][0], collecting data of
every other individual within range of the device in the process. Even *if* you
are the subject of surveillance, this is still an astonishing violation of
privacy. (Of course, law enforcement could always demand such records from your
service provider, but such an act at the very least has a paper trail.)

[0]: https://www.eff.org/deeplinks/2012/10/stingrays-biggest-unknown-technological-threat-cell-phone-privacy

<!-- more -->
