# GNU Trick-Or-Treat---FSF Crashes Windows 8 Launch

The FSF decided to [crash the Windows 8 launch even in New York City][0],
complete with [Trisquel][1] DVDs, FSF stickers and information about their
[pledge to upgrade to GNU/Linux instead of Windows 8][2].

I find this to be a fun, excellent alternative to blatant protesting that is
likely to be better received by those who would otherwise be turned off to
negativity. At the very least, the [walking gnu][3] would surely turn heads and
demand curiosity.

[0]: http://www.fsf.org/news/activists-trick-or-treat-for-free-software-at-windows-8-launch-event-1
[1]: http://trisquel.info/
[2]: http://www.defectivebydesign.org/windows8
[3]: http://www.fsf.org/blogs/community/gnus-trick-or-treat-at-windows-8-launch

<!-- more -->

Here is the e-mail that was sent to the info at fsf.org mailing list:

>    Happy (almost) Halloween, everybody,
>
>    You've probably been noticing Microsoft's ads for their new operating
>    system -- after all, they've spent more money on them than any other
>    software launch campaign in history. In fact, everything about the
>    campaign has been meticulously planned and optimized, so you can
>    imagine journalists' surprise when an unexpected guest showed up at an
>    invite-only launch event on Thursday.
>
>    Our volunteer, Tristan Chambers, was there and caught the whole thing
>    on camera! Pictures here:
>    <http://www.fsf.org/blogs/community/gnus-trick-or-treat-at-windows-8-launch>.
>
>    Reporters and security guards at the event weren't sure how to react
>    when they were greeted by a real, live gnu. The gnu -- which, on
>    closer inspection, was an activist in a gnu suit -- had come for some
>    early trick-or-treating. But instead of candy, she had free software
>    for the eager journalists. The gnu and the FSF campaigns team handed
>    out dozens of copies of Trisquel, a fully free GNU/Linux distribution,
>    along with press releases and stickers. Once they got over their
>    confusion, the reporters were happy to see us and hear our message --
>    that Windows 8 is a downgrade, not an upgrade, because it steals
>    users' freedom, security and privacy.
>
>    Free software operating systems are the real upgrade, and they don't
>    need a zillion-dollar launch event to prove it. To show Microsoft that
>    their ads won't change our minds, we're starting an upgrade pledge:
>    switch to a free OS, or if you're already using one, help a friend
>    switch. We can pay Microsoft a chunk of change for their new,
>    proprietary OS, or we can stand up for our freedom. The choice isn't
>    as hard as Microsoft wants you to think.
>
>    Sign the pledge now! -- <http://www.fsf.org/windows8/pledge>.
>
>    Thanks for making a commitment to free software.
>
>    PS - If you'd like more details about the action, you can check out
>    our press release here:
>    <http://www.fsf.org/news/activists-trick-or-treat-for-free-software-at-windows-8-launch-event-1>.
>
>    -Zak Rogoff
>    Campaigns Manager

