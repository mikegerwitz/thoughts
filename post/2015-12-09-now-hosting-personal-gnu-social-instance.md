# Now Hosting Personal GNU Social Instance

When I started writing this blog, my intent was to post notices more
frequently and treat it more like a microblogging platform; but that's not
how it ended up.  Instead, I use this site to write more detailed posts with
solid references to back up my statements.

[GNU Social](https://gnu.org/software/social/) is a federated social
network---you can host your own instances and they all communicate with
one-another.  You can find mine at the top of this page under "Notices", or
at [https://social.mikegerwitz.com/](https://social.mikegerwitz.com/).  I
will be using this site to post much more frequent miscellaneous notices.

<!-- more -->
