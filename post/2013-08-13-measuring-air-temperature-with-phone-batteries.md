# Measuring Air Temperature With Phone Batteries

OpenSignal---a company responsible for mapping wireless signal
strength by gathering data using mobile device software---noticed [an
interest correlation between battery temperature on devices and air
temperature][0].

> Aggregating daily battery temperature readings to city level revealed a
> strong correlation with historic outdoor air temperature. With a
> mathematical transformation, the average battery temperature across a
> group of phones gives the outdoor air temperature.

[0]: http://opensignal.com/reports/battery-temperature-weather/

<!-- more -->

**Note:** Graph renderings on their website require proprietary JavaScript, but
the article does describe it in detail, so it is not necessary. In
particular, note that, from [their provided equation][0], their scaling factor
`m' implies that there is a smaller variance in battery temperature in the
graph than there is in the actual air temperature, but that there is still a
correlation.

This is an interesting find. The article further states that "[...] we have
one data point where the Android data is actually more reliable than the
traditional source."

Such data can be very useful in providing decentralized data, so long as
[issues of privacy][1] are addressed. Doing so is not terribly difficult,
but would have a number of factors. In particular, the user would need the
means to submit data anonymously, which could be done via software/networks
such as [Tor][2]. GPS location data is certainly a privacy issue when it is
tied to your mobile device, but fortunately, it's unneeded: you can trust
your users to let you know where they reside by either (a) opting into using
location services or (b) allowing them to specify a location or approximate
location of their choosing (approximations would be important since a user
may not wish to change their location manually while they travel, say, to
and from work). If enough devices submit data, then legitimate data would
drown out those who are trying to purposefully pollute the database. Such an
example can be seen with Bitcoin, in which networks will [reach a consensus
on correct blockchains][3] so long as "a majority of computing power is
controlled by nodes that are not cooperating to attack the network". Of
course, users would be able to pollute the network by sending false data as
it is, and the [data is already tarnished from various factors such as body
heat][0].

Of course, I do assume that mobile devices will contain temperature sensors
in the future; [some already do][4] (but I cannot encourage their use, as
they use [proprietary software][5]). However, this is still a clever hack (I
suppose that term is redundant). In my searching while writing this article,
I did notice [prior examples of ambient temperature readings using Android
software][6] ([proprietary][5]), but the software does not aggregate data
for purposes of determining weather patterns.

Finally, please do not download OpenSignal's app; it too is
[proprietary][5]; this discussion was purely from a conceptual standpoint
and does not endorse any software.

[1]: /2013/08/london-trashcan-spies
[2]: https://www.torproject.org/
[3]: http://en.wikipedia.org/wiki/Protocol_of_Bitcoin
[4]: http://stackoverflow.com/a/11628921
[5]: http://www.gnu.org/philosophy/free-sw.html
[6]: https://play.google.com/store/apps/details?id=androidesko.android.electronicthermometer&hl=en
