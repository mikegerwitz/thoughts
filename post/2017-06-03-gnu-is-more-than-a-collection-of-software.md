# GNU is more than a collection of software

GNU is more than just a collection of software; it is an operating system:

  [https://www.gnu.org/gnu/thegnuproject.html]()

Many hackers and activists within the free software community don't
understand this well, and it's a shame to see attacks on GNU's relevance (as
measured by programs written by GNU on a given system) going
unchallenged. Software for GNU was written by the GNU Project when a
suitable free program was not available. It wouldn't have made sense to
write everything from scratch if free programs already solved the problem.

<!-- more -->

When we say GNU/Linux, we really are referring to the GNU operating system
that just happens to be using Linux. It could be using the FreeBSD kernel
([GNU/kFreeBSD][]). It could be using a Windows kernel with a Linux API
([GNU/kWindows][]). It could be using the [Hurd][] ([GNU/Hurd][]). The
disambiguation is important, but the end result is pretty much the same.

There are many systems that use Linux that are not GNU. Android is not GNU,
for example. We shouldn't attempt to call those systems "GNU/Linux"
blindly. (Also note how it's called "Android", not "Android/Linux", or just
"Linux". Somehow GNU is controversial, though.)

So if you see someone challenging GNU's relevance because GNU/Linux contains
so much software that isn't part of a GNU package, then please provide the
above link, and kindly explain to them that their observation is correct,
because GNU is an operating system, not a collection of programs.

[GNU/kFreeBSD]: https://en.wikipedia.org/wiki/Debian_GNU/kFreeBSD
[GNU/kWindows]: https://mikegerwitz.com/2016/04/GNU-kWindows
[Hurd]: https://www.gnu.org/software/hurd/
[GNU/Hurd]: https://www.debian.org/ports/hurd/
