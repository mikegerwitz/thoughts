# LuLu Says Goodbye to DRM

On January 8th, [LuLu announced that they would be dropping DRM][0] for users
who "[download] eBooks directly from Lulu.com to the device of their choice".
This is a wise move (for [those of us who oppose DRM][1]), but unfortunately, as
John Sullivan of the Free Software Foundation noted on the fsf-community-team
mailing list, the [comments on LuLu's website][0] are not all positive:

[0]: http://www.lulu.com/blog/2013/01/drm-update/
[1]: http://defectivebydesign.org/

> This is a positive development, but unfortunately there has been a lot
> of negative reaction in the comments on their announcement.
>
> It'd be great if people could chime in and support them their move away
> from DRM.

<!-- more -->

At first glance, certain authors seem to be concerned that the absense of DRM
will lead to ["more illegal file sharing"][0]:

> [...] I’ve got copies of my non-DRM ebooks all over the torrent sites and
> thousands of downloads registered, for which I haven’t received a cent. As
> soon as you push for them to be taken down, they’re posted up again.

While it is unfortunate that those authors are not receiving compensation for
their hard work, it should be noted that this problem exists even *with*
DRM, so it is not a valid argument toward keeping it.

I applaud this move by LuLu, though I'm disappointed to see [this comment in the
original post][0]:

>  Companies like Amazon, Apple and Barnes & Noble integrate a reader’s
>  experience from purchasing to downloading and finally to reading. These
>  companies do a fantastic job in this area, and eBooks published through Lulu
>  and distributed through these retail sites will continue to have the same
>  rights management applied as they do today.

They do not do it well; no DRM is good DRM.

