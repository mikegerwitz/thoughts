# Snowden Statement at Moscow Airport; Accepts Asylum Offers

**See Also:** [National Uproar: A Comprehensive Overview of the NSA Leaks and
Revelations][0]; I have not yet had the time to devote to writing a thorough
follow-up of recent events and will likely wait until further information and
leaks are presented.

[Edward Snowden][1]---the whistleblower responsible for [exposing various NSA
dragnet spying programs][0], among other documents---has been [stuck in the
Moscow airport][2] for quite some time while trying to figure out how he will
travel to countries offering him asylum, which may involve traveling through
territories that may cooperate with the United States' extradition requests.

[0]: /2013/06/national-uproar-a-comprehensive-overview-of-the-nsa-leaks-and-revelations
[1]: https://en.wikipedia.org/wiki/Edward_Snowden (Now with his own Wikipedia page)
[2]: http://www.guardian.co.uk/world/2013/jul/01/edward-snowden-escape-moscow-airport

<!-- more -->

Snowden [issued a statement today to Human Rights groups at Moscow's
Sheremetyevo airport][3], within which he mentioned:

> I announce today my formal acceptance of all offers of support or asylum I
> have been extended and all others that may be offered in the future. With, for
> example, the grant of asylum provided by Venezuela’s President Maduro, my
> asylee status is now formal, and no state has a basis by which to limit or
> interfere with my right to enjoy that asylum. [...] I ask for your assistance
> in requesting guarantees of safe passage from the relevant nations in securing
> my travel to Latin America, as well as requesting asylum in Russia until such
> time as these states accede to law and my legal travel is permitted. I will be
> submitting my request to Russia today, and hope it will be accepted
> favorably.[3]

Snowden had previously [withdrawn his request for political asylum in Russia][4]
after [Vladmir Putin stated that he could stay][5] only if he stopped "bringing
harm to our American partners"---something which [Snowden does not believe that
he is doing][6]. Although Venezuela has offered Snowden asylum, as [explained by
the Guardian][6], "he remains unable to travel there without travel
documents". Even if he does obtain travel documents, there are still
worries---earlier this month, the [Bolivian president's plane was diverted with
suspicion that Snowden was on board][7], showing that certain countries may be
willing to aid the U.S. in his extradition or otherwise prevent him from
traveling.

My focus on these issues will seldom be on Snowden himself---I would prefer to
focus primarily on what he sacrificed his life to bring to light. But it is
precisely this sacrifice that makes it important to ensure that Snowden does not
fall out of the picture (though it does not appear that he will any time soon).
The Guardian also seems to have adopted the strategy of slowly providing more
information on the leaks over time---such as the recent revelation that
[Microsoft cooperated with the NSA's Prisim program to provide access to
unencrypted contents of Outlook.com, Hotmail, Skype and SkyDrive services][8]; I
will have more on that later.

I end this with a photograph taken yesterday of [Richard Stallman with Julian
Assange holding up a picture of Snowden][9] that brings a smile to my face.

[3]: http://wikileaks.org/Statement-by-Edward-Snowden-to.html
[4]: http://www.guardian.co.uk/world/2013/jul/02/edward-snowden-nsa-withdraws-asylum-russia-putin
[5]: http://www.guardian.co.uk/world/2013/jul/01/putin-snowden-remain-russia-offer
[6]: http://m.guardiannews.com/world/2013/jul/12/edward-snowden-accuses-us-illegal-campaign
[7]: http://www.guardian.co.uk/world/2013/jul/05/european-states-snowden-morales-plane-nsa
[8]: http://www.guardian.co.uk/world/2013/jul/11/microsoft-nsa-collaboration-user-data
[9]: http://twitpic.com/d279tx
