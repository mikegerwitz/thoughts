# Google Analytics Removed from GitLab.com Instance

*This was originally written as a guest post for GitLab in November of 2015,
but they [decided not to publish it][gitlab-merge].*

[gitlab-merge]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1094

Back in May of of 2015, I [announced GitLab's liberation of their Enterprise
Edition JavaScript][ggfs] and made some comments about GitLab's course and
approach to software freedom.  In liberating GitLab EE's JavaScript, all
code served to the browser by GitLab.com's GitLab instance was [Free (as in
freedom)][free-sw], except for one major offender: Google Analytics.

[ggfs]: https://about.gitlab.com/2015/05/20/gitlab-gitorious-free-software/
[free-sw]: https://www.gnu.org/philosophy/free-sw.html

Since Google Analytics was not necessary for the site to function, users
could simply block the script and continue to use GitLab.com
[ethically][free-sw].  However, encouraging users to visit a project on
GitLab.com while knowing that it loads Google Analytics is a problem both
for users' freedoms, and for their privacy.

<!-- more -->

GitLab is more than service and front-end to host Git repositories; it has a
number of other useful features as well.  Using those features, however,
would mean that GitLab.com is no longer just a mirror for a project---it
would be endorsed by the project's author, requiring that users visit the
project on GitLab.com in order to collaborate.  For example, if an author
were to use the GitLab issue tracker on GitLab.com, then she would be
actively inviting users to the website by telling them to report issues and
feature requests there.

We cannot realistically expect that anything more than a minority of
visitors will know how to block Google Analytics (or even understand that it
is a problem).  Therefore, if concerned authors wanted to use those features
of GitLab, they had to use another hosted instance of GitLab, or host their
own.  But the better option was to encourage GitLab.com to remove Google
Analytics entirely, so that _all_ JavaScript code served to the users is
[Free][free-sw].

GitLab has chosen to actively
[work with the Free Software movement][ggfs]---enough so that they are now
considered an [acceptable host for GNU projects][gitlab-gnu-criteria]
according to [GNU's ethical repository criteria][gnu-repo-criteria].  And
they have chosen to do so again---headed by Sytse Sijbrandij (GitLab
Inc. CEO), Google Analytics has been removed from the GitLab.com instance
and replaced with [Piwik][piwik].

## More Than Just Freedom
This change is more than a commitment to users' freedoms---it's also a
commitment to users' privacy that cannot be understated.  By downloading and
running Google Analytics, users are being infected with some of the most
[sophisticated examples of modern spyware][ga-wikipedia]: vast amounts of
[personal and behavioral data][ga-google] are sent to Google for them to use
and share as they wish.  Google Analytics also tracks users across [many
different websites][ga-popularity], allowing them to discover your interests
and behaviors in ways that users themselves may not even know.

GitLab.com has committed to using [Piwik][piwik] on their GitLab instance,
which [protects users' privacy][piwik-privacy] in a number of very important
ways: it allows users to opt out of tracking, anonymizes IP addresses,
retains logs for limited time periods, respects [DoNotTrack][eff-dnt], and
more.  Further, all logs _will be kept on GitLab.com's own servers_, and is
therefore governed solely by
[GitLab.com's Privacy Policy][gitlab-privacy]; this means that other
services will not be able to use these data to analyze users' behavior on
other websites, and advertisers and others will know less about them.

Users should not have to try to [anonymize themselves][eff-ssd] in
order to maintain their privacy---privacy should be a default, and a
respected one at that.  GitLab has taken a strong step in the right
direction; I hope that others will take notice and do the same.

*Are you interested in helping other websites liberate their JavaScript?
 Consider [joining the FSF's campaign][freejs], and
 [please liberate your own][whyfreejs]!*

[eff-dnt]: https://www.eff.org/dnt-policy
[eff-ssd]: http://ssd.eff.org/
[freejs]: https://fsf.org/campaigns/freejs
[ga-google]: https://www.google.com/analytics/standard/features/
[ga-popularity]: http://w3techs.com/technologies/overview/traffic_analysis/all
[ga-wikipedia]: https://en.wikipedia.org/wiki/Google_Analytics
[gitlab-featurse]: https://about.gitlab.com/features/
[gitlab-gnu-criteria]: https://lists.gnu.org/archive/html/repo-criteria-discuss/2015-11/msg00012.html
[gitlab-privacy]: https://about.gitlab.com/privacy/
[gnu-repo-criteria]: https://www.gnu.org/software/repo-criteria.html
[mtg]: http://mikegerwitz.com/
[piwik]: https://piwik.org/
[piwik-privacy]: https://piwik.org/privacy/
[whyfreejs]: https://www.gnu.org/software/easejs/whyfreejs.html
