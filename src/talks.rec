id: afsi
title: Adopting Free Software Ideals
location: LibrePlanet 2021
date: 2021-03-20
locimg: lp-2021
abstract: Adopting free software ideals can be confusing and challenging for
+ individuals, filled with cognitive dissonance and questioning of
+ practicality.  Am I a bad person if I use non-free software?  What
+ example should I set as a free software activist or advocate?  How does
+ that relate to responsibilities of developers and distributors?
+
+ This is a talk about practical ethics and ideals.  It is personal,
+ drawing upon my experiences and evolution over the past fifteen
+ years.  It contains some awkward discussions that free software
+ activists like to avoid, and hopes to guide those seeking to adopt
+ more free software ideals, but fear they may not be able to meet such
+ high standards.  It's a talk about evolution and growth.
+
+ But complacency in the face of conflict can also dilute our
+ ideals.  So this is also a talk about balancing ideals in the context
+ of one's own unique circumstances, while at the same time preserving a
+ strong message about software freedom.
video_url: https://media.libreplanet.org/u/libreplanet/m/adopting-free-software-ideals/
link: https://media.libreplanet.org/u/libreplanet/m/adopting-free-software-ideals-audio-only/ Listen to Audio
link: /talks/afsi.pdf Slides
link: /projects/afsi/ Source Code
event_link: https://libreplanet.org/2021

id: sapsf
title: The Surreptitious Assault on Privacy, Security, and Freedom
location: LibrePlanet 2017
date: 2017-03-26
locimg: lp-2017
abstract: Privacy, security, and personal freedom: one cannot be had without the
+ others. Each of these essential rights are being surreptitiously
+ assaulted; only the most technical among us even know what to look for,
+ let alone how to defend ourselves. Governments, corporations, and groups
+ of ill-minded individuals are spying and preying upon both users and
+ bystanders with unprecedented frequency and breadth. For those of us who
+ do understand these issues, it would be irresponsible not to fight for
+ the rights of others and continue to bring these assaults to light.
+
+ This talk will survey the most pressing issues of today, including
+ topics of government surveillance and espionage; advertisers and data
+ analytics; the Internet of Things; corporate negligence; public policy
+ and the crypto wars; dangers of a non-free Web and untrusted, ephemeral
+ software; pervasive monitoring; remote servers, services, and “the
+ cloud”; modern vehicles; the fight against decentralization and free
+ software; societal pressures and complacency with the status quo; and
+ more.
+
+ Attendees will walk away with a broad understanding of these topics; an
+ overview of mitigations; and dozens of resources for further research
+ and discussion with others. No prior knowledge of security or
+ cryptography are necessary.
video_url: https://media.libreplanet.org/u/libreplanet/m/the-surreptitious-assault-on-privacy-security-and-freedom/
link: /talks/sapsf.pdf Slides
link: /projects/sapsf/plain/sapsf.bib Bibliography
link: /projects/sapsf/ Source Code
event_link: https://libreplanet.org/2017


id: cs4m
title: Computational Symbiosis: Methods That Meld Mind and Machine
location: LibrePlanet 2019
date: 2019-03-24
locimg: lp-2019
abstract: Words like "wizardry" and "incantation" have long been used to describe
+ skillful computational feats.  But neither computers nor
+ their users are performing feats of magic; for systems to think, we must
+ tell them how.
+
+ Today, users most often follow a carefully choreographed workflow that
+ thinks _for_ them, limited by a narrow set of premeditated
+ possibilities.  But there exist concepts that offer virtually no limits on
+ freedom of expression or thought, blurring the distinction between "user"
+ and "programmer".
+
+ This session demonstrates a range of practical possibilities when
+ machine acts as an extension of the user's imagination, for the technical
+ and nontechnical alike.
video_url: https://media.libreplanet.org/u/libreplanet/m/computational-symbiosis-methods-that-meld-mind-and-machine/
link: /talks/cs4m.pdf Slides
link: /projects/cs4m/ Source Code
event_link: https://libreplanet.org/2019


id: ethics-void
title: The Ethics Void
location: LibrePlanet 2018
date: 2018-03-25
locimg: lp-2018
abstract: Many communities have widely adopted codes of ethics governing the
+ moral conduct of their members and professionals. Some of these codes may
+ even be enshrined in law, and for good reason—certain conduct can have
+ enormous consequences on the lives of others.
+
+ Software and technology pervade virtually every aspect of our lives. Yet,
+ when compared to other fields, our community leaders and educators have
+ produced an ethics void. Last year, I introduced numerous topics concerning
+ privacy, security, and freedom that raise serious ethical concerns. Join me
+ this year as we consider some of those examples and others in an attempt to
+ derive a code of ethics that compares to the moral obligations of other
+ fields, and to consider how leaders and educators should approach ethics
+ within education and guidance.
video_url: https://media.libreplanet.org/u/libreplanet/m/the-ethics-void/
link: /talks/ethics-void.pdf Slides
link: /projects/ethics-void/ Source Code
event_link: https://libreplanet.org/2018


id: online-freedom
title: Restore Online Freedom!
location: LibrePlanet 2016
date: 2016-03-20
locimg: lp-2016
abstract: Imagine a world where surveillance is the default and users must
+ opt-in to privacy. Imagine that your every action is logged and analyzed to
+ learn how you behave, what your interests are, and what you might do
+ next. Imagine that, even on your fully free operating system, proprietary
+ software is automatically downloaded and run not only without your consent,
+ but often without your knowledge. In this world, even free software cannot
+ be easily modified, shared, or replaced. In many cases, you might not even
+ be in control of your own computing—your actions and your data might be in
+ control by a remote entity, and only they decide what you are and are not
+ allowed to do.
+ 
+ This may sound dystopian, but this is the world you’re living in right
+ now. The Web today is an increasingly hostile, freedom-denying place that
+ propagates to nearly every aspect of the average users’ lives—from their PCs
+ to their phones, to their TVs and beyond. But before we can stand up and
+ demand back our freedoms, we must understand what we’re being robbed of, how
+ it’s being done, and what can (or can’t) be done to stop it.
video_url: https://media.libreplanet.org/u/libreplanet/m/restore-online-freedom/
link: https://media.libreplanet.org/u/libreplanet/m/restore-online-freedom-14bf/ Slides
link: /projects/online-freedom/ Source Code
event_link: https://libreplanet.org/2016
