# International Day Against DRM 2016

Today is the [10th annual International Day Against DRM][day-drm]---a day
  where activists from around the world organize events in protest against
  [Digital Restrictions Management][drm].

[day-drm]: https://www.defectivebydesign.org/dayagainstdrm
[drm]: https://www.defectivebydesign.org/what_is_drm_digital_restrictions_management

<!-- more -->

DRM is a scheme by which tyrants use [antifeatures][] to lock down what
  users are able to do with their systems, often cryptographically.
For example,
  your media player might tell you how many times you can listen to a song,
    or watch a video, or read a book;
  it might [delete books][1984] that you thought you owned;
  it might require that you are [always online][always-on] when playing a
    game, and then stop working when you disconnect, or when they decide to
    stop supporting the game.
If you try to circumvent these locks,
  then you might be [called a pirate][pirate] and be thrown in prision under
  the ["anti-circumvention" privisons of the Digital Millenium Copyright Act
  (DMCA)][dmca].
These are all things [that have been long predicated][right-to-read], and
  are only expected to get worse with time.

That is, unless we take a stand and fight back.

I had the pleasure of participating in
  the [largest ever protest against the W3C][w3c-protest] and their attempts
  to introduce DRM as a _web standard_ via the [Encrypted Media Extensions
  (EME)][eme] proposal.[^photos]
This event was organized beautifully by Zak Rogoff of the [Free Software
  Foundation][fsf] and began just outside the Strata Center doors where the
  W3C was _actively meeting_,
    and then continued to stop outside the Google and Microsoft offices,
    both just blocks away.
We were [joined outside Microsoft][eff-protest] by Danny O'Brien,
  the EFF's International Director,
  who stepped out of the W3C meeting to address the protesters.

Afterward, most of us [traveled to the MIT Media Lab][media-lab] where
  Richard Stallman---who joined us in the protest---sat on a panel along
  with Danny O'Brien, Joi Ito of the MIT Media Lab, and Harry Halpin of the
  W3C.
The W3C was invited to participate in a discussion on EME, but they never
  showed.
As a demonstration of the severity of these issues,
  [Harry Halpin vowed to resign from the W3C][hh-resign] if the EME proposal
  ever became a W3C Recommendation.

I can say without hesitation that the protest and following discussion were
  some of the most powerful and memorable events of my life---there is no
  feeling like being a part of a group that shares such a fundamental
  passion (and distaste!) for something important.

And it _is_ very important.

[DRM is pervasive][dbd]---the Web is just one corner where it rears its ugly
  head.
The [International Day Against DRM][day-drm] gives you and others an
  excellent opportunity to hold your own protests, demonstrations, and events
  to raise these issues to others---and to do so as part of an
  _international group_;
  to send a strong, world-wide message:
  a message that it is _not_ acceptable to act as tyrants and treat users as
    slaves and puppets through use of digital handcuffs and [draconian
    punishments for circumventing them][dmca].

[^photos]: The EFF has some [great photots][eff-protest]; I'm the one in the
           hoodie between the giant GNU head and Zak Rogoff.

[antifeatures]: https://www.fsf.org/bulletin/2007/fall/antifeatures/
[lp2016]: https://libreplanet.org/2016/
[w3c-protest]: https://www.defectivebydesign.org/from-the-web-to-the-streets-protesting-drm
[eme]: https://w3c.github.io/encrypted-media/
[eff-protest]: https://w3c.github.io/encrypted-media/
[w3c]: https://www.w3.org/
[fsf]: https://fsf.org/
[media-lab]: https://motherboard.vice.com/read/we-marched-with-richard-stallman-at-a-drm-protest-last-night-w3-consortium-MIT-joi-ito
[hh-resign]: https://www.defectivebydesign.org/blog/w3c_staff_member_pledges_resignation_if_drm_added_web_standards
[dmca]: https://www.eff.org/issues/dmca
[dbd]: https://www.defectivebydesign.org/
[1984]: https://www.defectivebydesign.org/amazon-kindle-swindle
[always-on]: https://en.wikipedia.org/wiki/Always-on_DRM
[right-to-read]: https://www.gnu.org/philosophy/right-to-read.en.html
[pirate]: https://www.eff.org/deeplinks/2015/02/go-prison-sharing-files-thats-what-hollywood-wants-secret-tpp-deal
