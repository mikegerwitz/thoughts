# When Talking About Mobile Tracking, Don't Veil Bad Actors With Blanket Statements

It's difficult to have useful conversations about mobile tracking when
  someone says "your phone / mobile device tracks you";
  such statements don't often lead to constructive conversation because they
    are too vague and therefore easily dismissed as sensationalism or
    paranoia.
  And they are all too often without substance because,
    while users do have legitimate concerns,
    they aren't necessarily aware of the specific problems contributing to
      those concerns.

<!-- more -->

A mobile device is nothing more than a small computer that you carry around
  with you.
The networks that you connect to can spy on you---your
  cellular network, bluetooth, wifi, etc.
To help mitigate these threats,
  you can disable those communications until you are in a safe place that
  you don't mind others knowing about.
We can only have confidence that these connections have been disabled by
  physical means,
    like a hardware switch or a bag that acts like a Faraday cage.
[iOS deceives users][ios-deceive] when they ask to disable those communications
  for example.

The software running on your device often spies on you:
  the operating system itself often spies;
  the apps you install often spy.
This is the fault of the individual _authors_---_they_
  are the problem.
Consider using free/libre software that empowers you and serves _you_ rather
  than its creators;
    it's much harder to hide secrets in free software.
On Android,
  consider using only free software available in [F-Droid][].
We also need fully free mobile operating systems,
  like [Replicant][] and hopefully Purism's Librem 5 that is still under
  development.
Don't be fooled into thinking the Android on most phones is free
  software---only
    its core (AOSP) is.

Call out those that do harm---don't
  veil and protect them using statements like "your phone tracks you".
Talk about the specific issues.
Demand change and have the courage to reject them entirely.
This involves inconvenience and sacrifice.
But if we're strong now,
  then in the near future perhaps we won't have to make any sacrifices,
  much like the fully free GNU/Linux system desktops we have today.

Fore more information on tracking,
  see my [LibrePlanet 2017 and 2018 talks](/talks) "The Surreptitious Assault on Privacy,
  Security, and Freedom" and "The Ethics Void", respectively.

[F-Droid]: https://f-droid.org
[ios-deceive]: https://web.archive.org/web/20170922011748/https://support.apple.com/en-us/HT208086
[Replicant]: https://replicant.us
