#!/bin/bash
# Generate talks HTML page
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Talks are organized along with abstract in $TALKFILE.  Abstracts are
# assumed to be Markdown-formatted and are run through Pandoc.  A link to
# the talk video is provided, along with any supplemental links provided via
# $TALKFILE (e.g. slides, source code, bibliography).
##

set -euo pipefail

# Recfile containing talk abstracts and metadata.
declare -r TALKFILE=${TALKFILE:-src/talks.rec}


# List ids of all talks in $TALKFILE.
talk-list()
{
  recsel -CP id "$TALKFILE"
}


# Retrieve field FIELD from talk identified by ID in $TALKFILE.
talk-field()
{
  local -r id=${1?Missing talk id}
  local -r field=${2?Missing talk field}

  local result status
  result=$( recsel -P "$field" -e "id = '$id'" "$TALKFILE" )
  status=$?

  [ $? -a -n "$result" ] && echo "$result"
}


# Produce string to handle past or future dates.
present-relative()
{
  local -r date=${1?Missing date}

  local -i now udate
  now=$(date +%s)
  udate=$( date --date="$date" +%s )

  if [ "$now" -lt "$udate" ]; then
    echo 'Will be presented'
  else
    echo Presented
  fi
}


# Generate abstract for talk.
abstract()
{
  local -r id=${1?Missing talk id}

  local title location locimg date abstract url links
  title=$( talk-field "$id" title )
  location=$( talk-field "$id" location )
  locimg=$( talk-field "$id" locimg )
  date=$( talk-field "$id" date )
  abstract=$( talk-field "$id" abstract )
  url=$( talk-field "$id" video_url || echo "#$id" )
  event_link=$( talk-field "$id" event_link )
  links=$( talk-field "$id" link || true )

  local abstract_html
  abstract_html=$( pandoc -fmarkdown -thtml5 <<< "$abstract" )

  local watch_title=
  if [[ ! "$url" =~ ^# ]]; then
    watch_title="Watch $location Talk"
  fi

  present=$( present-relative "$date" )

  cat <<EOF
<article class="abstract talk">
  <h2 class="title" id="$id">$title</h2>

  <ul class="links">
    <li><a class="video $locimg" href="$url">$watch_title</a></li>
    $(
      while read lurl ltitle; do
        printf "<li><a href="%s">%s</a></li>\n" "$lurl" "$ltitle"
      done <<< "$links"
    )
  </ul>

  $abstract_html

  <p class="date">$present on $date at
    <a href="$event_link">$location</a>.</p>
</article>
EOF
}


# Generate talks page.
main()
{
  src/mkheader talks Talks

  local talks
  talks=$( recsel -P id src/talks.rec )

  echo '<h1>Talks</h1>'
  talk-list | while read id; do abstract "$id"; done

  cat src/footer.tpl.htm
}


main "$@"
