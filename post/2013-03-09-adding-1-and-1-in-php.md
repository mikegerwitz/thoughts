# Adding 1 and 1 in PHP

An amusing demonstration; it is my hope that [readers will not take this PHP
library seriously][0]. This is likely a parody of the over-engineering that
often takes foot in Object-Oriented development (a game of "how many GoF[^4]
design patterns can we use in this project" anyone?).

[0]: https://github.com/Herzult/SimplePHPEasyPlus

<!-- more -->

That is not to say that "OOP is bad" (just as object-oriented developers often
consider procedural code bad, when they may just be terrible at writing
procedural code). Indeed, I wrote [an ECMAScript framework for Classical OOP
(ease.js)][1].  The problem is that, with the excitement and misunderstandings
that surround "good" object-oriented design, designers are eager to
over-abstract their implementations (I have been guilty of the same thing).
Object oriented programming is often taught to novice CS students (often with
the reign of Java in schools)---teaching practices that can be good principles
when properly applied and in moderation---which [I have also seen contribute to
such madness][2].

Abstractions are highly important, but only when necessary and when they lead to
more concise representations of the problem than would otherwise occur (note
that some problems are inherently complicated and, as such, a concise
representation may not seen concise). I'm a strong advocate of DSLs when
abstractions begin to get in the way and increase the verbosity of the code
(languages with strong macro systems like lisp help eliminate the need for
DSLs written from scratch)---design patterns exist because of deficiencies in
the language: They are "patterns" of code commonly used to achieve a certain
effect.

[Criticisms against OOP are abundant][3], just as every other paradigm.

[1]: http://easejs.org
[2]: http://c2.com/cgi/wiki?TextbookOo
[3]: http://c2.com/cgi/wiki?ArgumentsAgainstOop

[^4]: Design Patterns: Elements of Reusable Object-Oriented Software. ISBN
    0-201-63361-2. Gamma, Helm, Johnson and Vlissides (the "Gang of Four").

