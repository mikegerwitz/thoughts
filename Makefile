# Builds thoughts (well, not quite like that)
#
#  Copyright (C) 2013, 2018, 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This project is a static site generator.  This Makefile was written to
# have deep knowledge of every aspect of the site so that it can be
# incrementally built, and so that all relevant portions will be properly
# rebuilt any time something changes.
#
# Source files are automatically identified through either wildcards or
# Makefile generation with one important exception: things in src/.  The
# reason is that src/ contains a number of things we don't want published,
# and the distinction is too messy to codify.  Of course, another option is
# to clean that up, but I don't mind being explicit for now.
##

.DELETE_ON_ERROR:

postsrc := $(wildcard post/*.md)
pmeta   := $(postsrc:.md=.meta)
phtml   := $(postsrc:.md=.html)
pmk     := $(pmeta:.meta=.mk)

www-root = www-root
cgit-root = cgit-root

# articles in TeX
texticles = $(wildcard papers/*/)
www-paper = $(patsubst papers/%/, $(www-root)/papers/%.pdf, $(texticles))
paper-abstracts = $(patsubst papers/%/, papers/%/abstract.tex, $(texticles))

images = $(wildcard images/*.*) $(wildcard images/tp/*.*)
www-images = $(patsubst images/%, $(www-root)/images/%, $(images))

cssfonts := $(shell build-aux/lsfonts)
www-fonts := $(patsubst fonts/%, $(www-root)/fonts/%, $(cssfonts))

# Manually maintain both for simplicity and to ensure something does not get
# unintentionally published.
srcpages = src/index.html src/about.html src/papers.html src/posts.html \
           src/talks.html src/404.html src/about/inside.html \
           src/about/githubbub.html \
           src/about/resume.html $(wildcard src/about/resume/*)
www-pages = $(patsubst src/%, $(www-root)/%, $(srcpages))

www-files = $(www-pages) $(www-root)/style.css $(www-root)/rss.xml $(www-paper) \
            $(www-images) $(www-fonts) $(www-root)/redirect-map.php

pgdeps := src/mkheader inline.css src/h12title src/header.tpl.htm src/footer.tpl.htm

RSS_N=10
export WWW_URL


.PHONY: default clean webroot cgitroot

default: webroot

%.meta: %.html src/post2meta src/post2html
	src/post2meta $< > $@
src/talks.html: src/talks.rec src/talks.sh
src/papers.html: src/papers.rec src/papers.sh $(paper-abstracts)
%.html %.xml: %.sh post/list $(pgdeps) $(phtml)
	$< > $@
%.html: %.md src/post2html $(pgdeps) src/pandoc.tpl
	src/post2html $< > $@
%.html: %.htm src/mkheader inline.css src/h12title src/header.tpl.htm src/footer.tpl.htm
	src/mkheader about @__PAGE_TITLE__@ \
	  | cat - $< src/footer.tpl.htm \
	  | src/h12title @__PAGE_TITLE__@ \
	  > $@

# special outputs
src/rss.xml: src/rss.sh post/list $(phtml)
	head -n$(RSS_N) post/list | xargs $< > $@

posts: $(pmeta) $(phtml)
post/list: $(pmeta)
	ls post/*.meta | sort -rn > $@

# Rules for generating the final webroot from the posts are themselves
# generated.  This also appends dependencies to www-posts.
.PHONY: www-posts
post/%.mk: post/%.meta build-aux/mkmk
	build-aux/mkmk $(www-root) $< > $@

# Inline CSS for header (for initial page load).  Blocks marked with
# `/*inline*/' are included.
inline.css: style.css
	awk '/\/\*inline\*\/$$/,/^\}/{sub(/ *\/\*inline\*\//, ""); print}' $< \
	  | sed 's/^ \+//g; s/ *\([{}>()]\) */\1/g; s/^ *\([^:]\+:\) */\1/g' \
	  | tr -d '\n' > $@

# Note the conditional include only for webroot.  This is needed for two
# reasons:
#   1. To avoid including them on `clean' (see GNU Make manual, which is
#      where this snippet originated from); and
#   2. Because otherwise including the makefiles causes every pmete to be
#      built, which is unnecessary for all but `webroot'.
#
# The alternative (and perhaps more proper means) to #2 would be to run mkmk
# as part of the meta target.  This was originally done until a solution to
# `clean' was needed; this handles both situations well.
ifeq ($(MAKECMDGOALS),webroot)
include $(pmk)
endif

webroot: www-posts $(www-files)
$(www-root)/style.css: style.css
	install -Dma+r $< $@
$(www-root)/%.sh: src/%.sh
	install -Dma+rx $< $@
$(www-root)/%: src/%
	install -Dma+r $< $@
$(www-root)/fonts/%: fonts/%
	install -Dma+r $< $@
$(www-root)/papers/%: papers/%
	install -Dma+r $< $@
$(www-root)/images/%: images/%
	install -Dma+r $< $@


# (La)TeX papers are expected to have their own Makefiles as well as an
# abstract.tex that will be generated if missing
papers/%/abstract.tex:
	awk '/^\\begin\{abstract\}/,/\\end\{abstract\}/' papers/$*/$*.tex | sed '1d;$$d' > $@
papers/%.pdf: papers/%/abstract.tex
	$(MAKE) -C $(dir $<) pdf
	cp $(dir $<)/$*.pdf $@

docs/papers/.list: thoughts articles
	echo "$(articles) $(texticles)" | tr ' ' '\n' | tools/doclist >$@


cgitroot: $(cgit-root)/head.html $(cgit-root)/header.html $(cgit-root)/footer.html \
          $(cgit-root)/cgitrc
$(cgit-root)/%: src/cgit/%
	install -Dma+r $< $@

clean:
	rm -rf $(www-root) $(pmeta) $(phtml) $(pmk) $(cgit-root)
