# Windows 8.1 to display targeted advertisements on local system searches

It is very disturbing that [Microsoft decided that it would be a good idea
to display targeted ads on local searches][0]---that is, if you search for a
file on your PC named "finances", you may get ads for finance software,
taxes, etc. If you search for "porn", well, you get the idea.

> Bing Ads will be an integral part of this new Windows 8.1 Smart Search
> experience. Now, with a single campaign setup, advertisers can connect
> with consumers across Bing, Yahoo! and the new Windows Search with highly
> relevant ads for their search queries. In addition, Bing Ads will include
> Web previews of websites and the latest features like site links, location
> and call extensions, making it easier for consumers to complete tasks and
> for advertisers to drive qualified leads.[[1]]

[0]: http://www.computerworld.com/s/article/9241524/Steven_J._Vaughan_Nichols_Microsoft_Bing_bang_bungles_local_search
[1]: http://community.bingads.microsoft.com/ads/en/bingads/b/blog/archive/2013/07/02/new-search-ad-experiences-within-windows-8-1.aspx

<!-- more -->

While that is certainly obnoxious, consider the larger issue of privacy
(which seems to be in the news a lot lately[[2]][[3]]): Late last year, there
was an uproar in the Free Software community when [Ubuntu decided to query
Amazon---enabled by default---on local searches][4] using their new Unity
interface. The problem is that your personal queries are being sent to a
third party---queries that you generally would expect to be private. If I
run a `find' or `grep' command on my system, I certainly do not expect it to
report to Amazon or Microsoft what I am searching for.

And to make matters even worse, Microsoft is exploiting this information to
allow advertisers to target you. [Ironic.][5]

[Do not use Windows 8][6] (or any other proprietary software, for that
matter).

[2]: /2013/08/facebook-knows-about-you-even-if-you-are-not-a-member
[3]: /2013/06/national-uproar-a-comprehensive-overview-of-the-nsa-leaks-and-revelations
[4]: http://www.fsf.org/blogs/rms/ubuntu-spyware-what-to-do
[5]: http://www.scroogled.com/email/
[6]: https://www.fsf.org/windows8
