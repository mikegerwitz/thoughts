# Defective By Design Campaign Against W3C DRM Standard

[As I had mentioned late last week][0], RMS had mentioned that Defective By
Design (DBD) would be campaigning against the [introduction of DRM into the W3C
HTML5 standards][1]. (Please see [my previous mention of this topic][0] for a
detailed explanation of the problem and a slew of references for additional
information.) Well, [this campaign is now live and looking for
signatures][2]---50,000 by May 3rd, which is the [International Day Against
DRM][3]:

> Hollywood is at it again. Its latest ploy to take over the Web? Use its
> influence at the World Wide Web Consortium (W3C) to weave [Digital
> Restrictions Management (DRM)][4] into HTML5 -- in other words, into the very
> fabric of the Web.
>
>  [...]
>
> Help us reach 50,000 signers by May 3rd, 2013, the [International Day Against
> DRM][3]. We will deliver the signatures to the W3C (they are right down the
> street from us!) and [make your voice heard[[1].

[0]: /2013/03/html5-drm
[1]: https://www.eff.org/deeplinks/2013/03/defend-open-web-keep-drm-out-w3c-standards
[2]: http://www.defectivebydesign.org/no-drm-in-html5
[3]: http://www.defectivebydesign.org/dayagainstdrm
[4]: http://www.defectivebydesign.org/what_is_drm

<!-- more -->

To summarize the issue as [stated by the EFF][5]:

> W3C is there to create comprehensible, publicly-implementable standards that
> will guarantee interoperability, not to facilitate an explosion of new
> mutually-incompatible software and of sites and services that can only be
> accessed by particular devices or applications. But EME is a proposal to bring
> exactly that dysfunctional dynamic into HTML5, even risking a return to the
> ["bad old days, before the Web"][5] of deliberately limited
> interoperability.
>
> it would be a terrible mistake for the Web community to leave the door open
> for Hollywood's gangrenous anti-technology culture to infect W3C standards.

So please---[sign the petition now][2]!

[5]: http://www.anybrowser.org/campaign/index.html

