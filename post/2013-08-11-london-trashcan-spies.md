# London Trashcan Spies

We're not talking about kids hiding out in trashcans talking on
walkie-talkies and giggling to each other.

[Ars has reported on London trashcans][0] rigged to collect the [MAC
addresses][1] of mobile devices that pass by. Since we do not often see
mobile devices carrying themselves around, we may as well rephrase this as
"collect the MAC addresses of people that pass by":

> During a one-week period in June, just 12 cans, or about 10 percent of the
> company's fleet, tracked more than 4 million devices and allowed company
> marketers to map the "footfall" of their owners within a 4-minute
> walking distance to various stores.

[0]: http://arstechnica.com/security/2013/08/no-this-isnt-a-scene-from-minority-report-this-trash-can-is-stalking-you/
[1]: http://en.wikipedia.org/wiki/MAC_address

<!-- more -->

Your device's---er, *your*---MAC address is a unique identifier that, in
the case of wireless networks, is used by the networks to state that a
message is intended specifically for you---something that is necessary since
wireless devices communicate through open air and, therefore, your device is
[also able to pick up the communications of other devices][2]:

> In IEEE 802 networks such as Ethernet, token ring, and IEEE 802.11, and in
> FDDI, each frame includes a destination Media Access Control address (MAC
> address). In non-promiscuous mode, when a NIC receives a frame, it
> normally drops it unless the frame is addressed to that NIC's MAC address
> or is a broadcast or multicast frame.

Therefore, in such networks, a MAC address is required for communication. So
why does your device freely give away such a unique identifier that can be
used to track you? Consider that, when wireless is enabled (and, as [the Ars
article][0] mentions, sometimes [even when it's not][3]), your device
generally scans your surroundings in order to provide you with a list of
networks to connect to. This list is generally populated when various access
points broadcast their own information to advertise themselves so that you
can select them to connect. However, some access points are hidden---they do
not broadcast their information, which helps to deter unwanted or malicious
users. To connect to these access points, you generally provide the name
that the access point administrator has given to it (e.g. "mysecretap").

Let's say you disconnect from mysecretap. Since the access point (AP) is not
broadcasting itself, how does your device know when it is available again?
It must attempt to ping it and see if it gets a response. With this ping is
your MAC address. Since many devices conveniently like to connect
automatically to known access points when they become available, it is
likely that your device is pinging rather frequently.

But what if you do not use hidden access points? Well, it is likely that the
same issue still stands---what if the access point that you connected to was
once listed but then becomes hidden? (Maybe the administrator of the access
point allowed broadcasts for a period of time to allow people to connect
easily, but then hid it at a later time.) Your device would need to account
for that, and therefore, to be helpful, likely broadcasts pings for any
access point you have connected to recently (where "recently" would depend
on your device).

Now, back to the [NSA][5]-wannabe-trashcans: At this point, all an observer
must do is lay in wait for those broadcasts and record the MAC addresses. By
placing these devices at various locations, you could easily track the
movements of individuals, including their speed, destinations, durations of
their visits, visit frequencies, favorite areas, dwellings, travel patterns,
etc. Since devices may broadcast a whole slew of recent access points that
it connected to, you could also see areas that the owner may have been to
(oh, I see that you connected to the free wifi in that strip joint). You
[could be evil][6].

Turn off wireless on your device when you are not using it---especially when
you are traveling. Ensure that your device [does not continue pinging access
points when wireless is disabled][3].

Better yet, fight back. Consider exploring how to spoof your MAC address,
perhaps randomly generating one every so often. Consider the possibilities
of activist groups that may pollute these spy databases by gathering a list
of unique MAC addresses of passerbys for the purpose of rebroadcasting them
at random intervals---which you could even do using long-range antennas
targeted at these devices.[^7] If done properly to mimic models of common
travel patterns, the data that these spy devices gather would become
unreliable.[^8]

Surveillance by any entity---be it [governments][5], corporations,
individuals or otherwise---is not acceptable.

[2]: http://en.wikipedia.org/wiki/Promiscuous_mode
[3]: http://arstechnica.com/gadgets/2013/08/review-android-4-3-future-proofs-the-platform-with-multitude-of-minor-changes/3/#p15
[4]: http://arstechnica.com/security/2013/08/diy-stalker-boxes-spy-on-wi-fi-users-cheaply-and-with-maximum-creep-value/
[5]: /2013/06/national-uproar-a-comprehensive-overview-of-the-nsa-leaks-and-revelations
[6]: http://renewlondon.com

[^7]: Disclaimer: Please research your local laws.

[^8]: Of course, it is important that such an activity in itself does not
violate a person's privacy, and so such collection must be done in a manner
that cannot in itself identify the person's travel patterns (e.g. by
not storing information on what access point the data was collected from).

