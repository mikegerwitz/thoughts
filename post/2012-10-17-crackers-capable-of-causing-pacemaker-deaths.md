# Crackers capable of causing pacemaker deaths

[This article][0] demonstrates why medical devices must contain free software:
crackers are able to, with this particular type of pacemaker, exploit the device
to trigger a fatal electric shock to its host from as far as 30 feet away (the
article also mentions rewriting the firmware, which could of course be used to
schedule a deadly shock at a predetermined time). These issues would not exist
with free software, as the user and the community would be able to study the
source code and fix any defects (or hire someone who can) before placing it in
their bodies.

[0]: http://www.scmagazine.com.au/News/319508,hacked-terminals-capable-of-causing-pacemaker-mass-murder.aspx

<!-- more -->

(Note that this article mistakenly uses the term "hacker" when they really
mean "cracker".)

The aforementioned article is an excellent supplement to [a discussion on free
software in pacemakers][1]. In particular, I had pointed out within this
discussion [a talk by Karen Sandler of the GNOME Foundation regarding this
issue][2] at OSCON 2011, in which she mentions potential issues of proprietary
software in pacemakers and the difficulty she faced in attempting to get the
source code for one that she was considering for herself.

The discussion on HackerNews also yielded [an article by the SFLC][3] detailing
this issue.

(Please do not use YouTube's proprietary video player to view the mentioned
YouTube video.)

[1]: http://news.ycombinator.com/item?id=3959547
[2]: https://www.youtube.com/watch?v=nFZGpES-St8
[3]: https://www.softwarefreedom.org/news/2010/jul/21/software-defects-cardiac-medical-devices-are-life-/
