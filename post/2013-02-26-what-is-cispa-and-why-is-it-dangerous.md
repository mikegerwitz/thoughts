# What is CISPA and Why is it Dangerous?

The EFF has put together an excellent [FAQ on CISPA][0], the "cybersecurity"
bill that was reintroduced to congress earlier this month.

[0]: https://www.eff.org/deeplinks/2013/02/cispas-back-faq-what-it-and-why-its-still-dangerous

<!-- more -->
