# California Proposition 35 Concerns

The EFF [points out problems with California's Proposition 35][0], which would,
among other things, [require registered sex offenders to "disclose Internet
activities and identities"][1]:

[0]: https://www.eff.org/deeplinks/2012/11/eff-urges-no-vote-california-proposition-35
[1]: http://voterguide.sos.ca.gov/propositions/35/

<!-- more -->

> [...] Proposition 35 would force individuals to provide law enforcement with
> information about online accounts that are wholly unrelated to criminal
> activity – such as political discussion groups, book review sites, or blogs.
> In today’s online world, users may set up accounts on websites to communicate
> with family members, discuss medical conditions, participate in political
> advocacy, or even listen to Internet radio. An individual on the registered
> sex offender list would be forced to report each of these accounts to law
> enforcement within 24 hours of setting it up – or find themselves in jail.
> This will have a powerful chilling effect on free speech rights of tens of
> thousands of Californians.

