# DMR: "Very early C compilers and language"

An interesting article by Dennis Ritchie discussing [early C compilers][0]
recovered from old DECtapes. The source code and history are fascinating reads.
The quality of the code (the "kludgery"[1], as he puts it) to me just brings
smiles---I appreciate seeing the code in its original glory.

It is also saddening reading the words of such a great man who is no longer with
us; perhaps it helps to better appreciate his legacy.

[0]: http://cm.bell-labs.com/cm/cs/who/dmr/primevalC.html
[1]: http://www.catb.org/~esr/jargon/html/K/kludge.html

<!-- more -->

