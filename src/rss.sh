#!/bin/bash
# Generate RSS feed from given post metadata files
#
#  Copyright (C) 2019 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# All posts must be provided on the command line as a path to each
# individual metadata file, in the order in which they should appear in the
# feed output.
##

set -euo pipefail

# Website root URL.
declare -r www=${WWW_URL:-https://mikegerwitz.com}


# Look up metadatum FIELD in metafile FILE.
pmeta()
{
  local -r file=${1?Missing metafile name}
  local -r field=${2?Missing field name}

  recsel -P "$field" "$file"
}


# Generate RSS item for each post in provided arguments.
# See `gen-item'.
gen-items()
{
  while [ $# -gt 0 ]; do
    gen-item "$1"
    shift
  done
}


# Generate RSS item for post in metadata file FILE.  The abstract will be
# used for the item description.
gen-item()
{
  local -r file=${1?Missing file name}

  local subject author slug date
  subject=$( pmeta "$file" subject )
  author=$( pmeta "$file" author )
  slug=$( pmeta "$file" slug )
  date=$( pmeta "$file" date )

  # TODO: entire content?
  local abstract
  abstract=$( pmeta "$file" abstract )

  cat <<EOF
<item>
  <title><![CDATA[$subject]]></title>
  <author><![CDATA[$author]]></author>
  <link>$www/$slug</link>
  <pubDate>$date</pubDate>
  <description><![CDATA[
    $abstract
    <p><a href="$www/$slug">(Read full post)</a></p>
  ]]></description>
</item>
EOF
}


# Output usage information and exit with EX_USAGE.
usage()
{
  cat <<EOF
Usage: $0 postmeta...
Generate RSS feed from provide post metadata.
Example: $0 post/2018-01-01-foo.meta post/2018-02-01-bar.meta

At least one postmeta must be provided.
EOF

  exit 64  # EX_USAGE
}


# Output RSS feed from all post files provide via arguments.
main()
{
  test $# -gt 0 || usage

  cat <<EOF
<?xml version="1.0"?>
<rss version="2.0">
  <channel>
    <title>Mike Gerwitz's Thoughts and Ramblings</title>
    <link>$www</link>
    <description>
      Posts and articles from a free software hacker and activst with a focus on user privacy and security
    </description>
    $( gen-items "$@" )
  </channel>
</rss>
EOF
}


main "$@"
