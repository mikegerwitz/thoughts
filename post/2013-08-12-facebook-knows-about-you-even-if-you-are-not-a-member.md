# Facebook knows about you even if you are not a member

An article about [the scope of Facebook's data collection][0] speaks for
itself; this really does not come as a surprise, but is nonetheless
unsettling.

[0]: http://www.groovypost.com/news/facebook-shadow-accounts-non-users/

<!-- more -->

Encourage your friends, colleagues and acquaintances to use services like
[Diaspora][1] that are respectful of your data instead. Better yet: explain
to those individuals the problems of social media services and ask that they
respectfully leave you out of it.

[1]: https://joindiaspora.com/

