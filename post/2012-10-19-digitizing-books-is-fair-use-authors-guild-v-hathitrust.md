# Digitizing Books Is Fair Use: Author's Guild v. HathiTrust

A New York court ruled that "digitizing" books for researched and disabled
individuals is lawful.[[0]]

[0]: https://www.eff.org/deeplinks/2012/10/authors-guild-vhathitrustdecision

<!-- more -->
