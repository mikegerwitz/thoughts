# FreeBSD, Clang and GCC: Copyleft vs. Community

A useful perspective explaining why [FreeBSD is moving away from GCC in
favor of Clang][0]; indeed, they are moving away from GPL-licensed software
in general. While this is [not a perspective that I personally agree
with][1], it is one that I will respect for the project. It is worth
understanding the opinions of those who disagree with you to better
understand and formulate your own perspective.

[0]: http://unix.stackexchange.com/a/49970
[1]: /2012/11/vlcs-move-to-lgpl

But I am still a free software activist.

<!-- more -->

According to the [FreeBSD FAQ][2]:

> The goal of the FreeBSD Project is to provide a stable and fast general
> purpose operating system that may be used for any purpose without strings
> attached.

As is mentioned in [the aforementioned article][0], the BSD community does not
hold the same opinions on what constitutes "without strings
attached"---the BSD community [considers the restriction on the user's
right to make proprietary use of the software to be a "string"][2],
whereas the free software community under [RMS][3] believes that [the
ability to make a free program proprietary is unjust][4]:

> Making a program proprietary is an exercise of power. Copyright law today
> grants software developers that power, so they and only they choose the
> rules to impose on everyone else—a relatively small number of people make
> the basic software decisions for all users, typically by denying their
> freedom. When users lack the freedoms that define free software, they
> can't tell what the software is doing, can't check for back doors, can't
> monitor possible viruses and worms, can't find out what personal
> information is being reported (or stop the reports, even if they do find
> out). If it breaks, they can't fix it; they have to wait for the developer
> to exercise its power to do so. If it simply isn't quite what they need,
> they are stuck with it. They can't help each other improve it.

The [Modified BSD License][5] is a GPL-compatible Free Software
license---that is, software licensed under the Modified BSD license meets
the requirements of the [Free Software Definition][6]. The additional
"string" that the BSD community is referring to is the concept of
[copyleft][7]---Richard Stallman's copyright hack and one of his most
substantial contributions to free software and free society. To put it into
the [words of the FSF][7]:

> Copyleft is a general method for making a program (or other work) free,
> and requiring all modified and extended versions of the program to be free
> as well.

Critics often adopt the term ["viral" in place of "copyleft"][8] because
of the requirement that all derivatives must contain the same copyleft
terms---the derivative must itself be Free Software, perpetually (until, of
course, the copyright term expires and it becomes part of the public domain,
[if such a thing will ever happen at this rate][9]). In the case of the
Modified BSD license---being a more permissive license that is non-copyleft
and thus allows proprietary derivatives---derivative works that include both
BSD- and GPL-licensed code essentially consume the [Modified BSD license's
terms][10], which are a subset of the [GPL's][11]. Of course, this is not
pursuant to [FreeBSD's goals][2] and so they consider this to be a bad
thing: There are "strings attached".

This is more demonstrative of the ["open source" philosophy than that of
"Free Software"][12] (yes, notice the bias in my capitalization of these
terms).

[Copyleft is important][7] because it ensures that all users will forever
have the [four fundamental freedoms associated with Free Software][6]. The
GPL incorporates copyleft; BSD licenses do not. Consider why this is a
problem: Imagine some software Foo licensed under [the Modified BSD
license][10]. Foo is free software; it is licensed under a [free software
license (Modified BSD)][5]. Now consider that someone makes a fork---a
derivative---of Foo, which we will call "Foobar". Since [the Modified BSD
license is not copyleft][10], the author of Foobar decides that he or she
does not wish to release its source code; this is perfectly compliant with
the Modified BSD license, as it does not require that source code be
distributed with a binary (it only requires---via its [second
clause][10]---that the copyright notice, list of conditions and disclaimer be
provided).

The author has just taken Foo and made it proprietary.

The FreeBSD community is okay with this; [the free software community is
not][4]. There is a distinction between these two parties: When critics of
copyleft state that they believe the GPL is "less free" than more
permissive licenses such as the BSD licenses, they are taking into
consideration the freedoms of developers and distributors; the GPL, on the
other hand, explicirly *restricts* these parties' rights in order to protect
the *users* because those parties are precisely those that seek to *restrict
the users' freedoms*; we cannot provide such freedoms to developers and
distributors without sacrificing the rights of the vulnerable users who
generally do not have the skills to protect themselves from being taken
advantage of.[^13] Free software advocates have exclusive, unwaivering
loyalty to users.

As an example of the friction between the two communities, consider a
concept that has been termed ["tivoization"][14]:

> Tivoization means certain “appliances” (which have computers inside)
> contain GPL-covered software that you can't effectively change, because
> the appliance shuts down if it detects modified software. The usual
> motive for tivoization is that the software has features the manufacturer
> knows people will want to change, and aims to stop people from changing
> them. The manufacturers of these computers take advantage of the freedom
> that free software provides, but they don't let you do likewise.

This [anti-feature][15] is a type of [Digital Restrictions Management
(DRM)][16] that exposes a [loophole in the GPL that was closed in
Section 3 of the GPLv3][14], which [requires that][11]:

> When you convey a covered work, you waive any legal power to forbid
> circumvention of technological measures to the extent such circumvention
> is effected by exercising rights under this License with respect to the
> covered work, and you disclaim any intention to limit operation or
> modification of the work as a means of enforcing, against the work's
> users, your or third parties' legal rights to forbid circumvention of
> technological measures.

Unfortunately, not everyone has agreed with this move. A number of
[developers of the kernel Linux expressed their opposition of GPLv3][17]. In
response to the aforementioned GPLv3 provision, they stated:

> While we find the use of DRM by media companies in their attempts to reach
> into user owned devices to control content deeply disturbing, our belief
> in the essential freedoms of section 3 forbids us from ever accepting any
> licence which contains end use restrictions. The existence of DRM abuse is
> no excuse for curtailing freedoms.

Linus Torvalds---the original author of the kernel Linux---also [expressed
his distaste toward the GPLv3][18]; the kernel is today still licensed under
the GPLv2.

[The BSD camp has similar objections][19]:

> Appliance vendors in particular have the most to lose if the large body of
> software currently licensed under GPLv2 today migrates to the new license.
> They will no longer have the freedom to use GPLv3 software and restrict
> modification of the software installed on their hardware. High support
> costs ("I modified the web server on my Widget 2000 and it stopped
> running...") and being unable to guarantee adherence to specifications in
> order to gain licensing (e.g. FCC spectrum use, Cable TV and media DRM
> requirements) are only two of a growing list of issues for these
> users. --Justin Gibbs, VP of The FreeBSD Foundation

My thoughts while reading the above where echoed by Gibbs further on in his
statement: "[T]he stark difference between the BSD licensing philosophy and
that of the Free Software Foundation are only too clear." For the FreeBSD
community, this is a very serious issue and their argument is certainly a
legitimate concern on the surface. However, it is an argument that the Free
Software community would do well to reject: Why would we wish to sacrifice
users' freedoms for any reason, let alone these fairly absurd ones. In
particular, a support contract could dictate that only unmodified software
will be provided assistance and even mandate that the hardware indicate
changes in software: like breaking the "void" sticker when opening a
hardware component. Moreover, how frequently would such a situation
actually happen relative to their entire customer base? My guess is: fairly
infrequently. The second issue is a more complicated one, as I am not as
familiar on such topics, but a manufacturer can still assert that the
software that it provides with its devices is compliant. If the compliance
process forbids any possibility of brining the software into
non-compliance---that is, allowing the user to modify the software---then
the hardware manufacturer can choose to not use free software (and free
software advocates will subsequently reject it until standards bodies grow
up).

As I mentioned at the beginning of this article: this is a view that I will
respect for the project. I disagree with it, but FreeBSD is still free
software and we would do well not to discriminate against it simply because
someone else may decide to bastardize it and betray their users by making it
proprietary or providing [shackles][16]. However, provided the licensing;
option for your own software, you should choose the GPL.

**Colophon:** The title of this article is a play on [RMS' "Copyright vs.
Communty"][20], which is a title to a speech he frequently provides
worldwide. His speech covers how copyright works against the interests of
the community; here, BSD advocates aruge that [copyleft][7] works against
the interests of *their* community and their users; I figured that I would
snag this title as a free software advocate before someone else opposing
copyleft did.

[2]: http://www.freebsd.org/doc/faq/introduction.html#FreeBSD-goals
[3]: http://en.wikipedia.org/wiki/Richard_Stallman
[4]: http://www.gnu.org/philosophy/freedom-or-power.html
[5]: http://www.gnu.org/licenses/license-list.html#ModifiedBSD
[6]: http://www.gnu.org/philosophy/free-sw.html
[7]: http://www.gnu.org/copyleft/
[8]: http://en.wikipedia.org/wiki/Copyleft#Viral_licensing
[9]: http://www.gnu.org/philosophy/misinterpreting-copyright.html
[10]: http://en.wikipedia.org/wiki/BSD_licenses
[11]: http://www.gnu.org/licenses/gpl.html
[12]: http://www.gnu.org/philosophy/open-source-misses-the-point.html

[^13]: Technically, the GPL exercises restrictions only on distributors; a
      developer can integrate GPL'd code into their proprietary software so
      long as they do not distribute it [(as defined in the GPL)][11]. However,
      developers often have to cater to distributors, since software will
      generally be distributed; if it is not, then it is not relevant to this
      discussion.

[14]: http://www.gnu.org/licenses/rms-why-gplv3.html
[15]: http://www.fsf.org/blogs/community/antifeatures
[16]: http://www.defectivebydesign.org/what_is_drm_digital_restrictions_management
[17]: http://lwn.net/Articles/200422/
[18]: http://en.wikipedia.org/wiki/Linux_kernel
[19]: http://www.freebsdfoundation.org/press/2007Aug-newsletter.shtml
[20]: http://www.gnu.org/philosophy/copyright-versus-community.html
