#
# cgit config
# see cgitrc(5) for details

root-title=Projects
root-desc=Free Software projects, configurations, and playthings
#root-readme=/var/gitrepos/README.html

logo-link=https://mikegerwitz.com/
virtual-root=/projects/

enable-http-clone=1
clone-url=https://mikegerwitz.com/projects/$CGIT_REPO_URL
snapshots=tar.gz tar.bz2 zip

enable-index-owner=0
enable-index-links=1
enable-commit-graph=1
enable-log-filecount=1
enable-log-linecount=1

repository-sort=name
branch-sort=age
max-stats=quarter

head-include=/var/gitrepos/head.html
header=/var/gitrepos/header.html
footer=/var/gitrepos/footer.html
css=/projects/static/cgit.css
logo=

source-filter=/usr/lib/cgit/filters/syntax-highlighting.py
about-filter=/usr/lib/cgit/filters/about-formatting.sh

readme=:README.md
readme=:README

section-sort=0

section=libs / frameworks

repo.url=easejs
repo.name=GNU ease.js
repo.path=/var/gitrepos/easejs.git
repo.desc=Classical object-oriented framework for JavaScript
#repo.logo=/images/heckert-gnu.png

repo.url=liza
repo.path=/var/gitrepos/liza.git
repo.desc=Data collection, validation, and processing framework for JavaScript [employer project]

repo.url=hoxsl
repo.path=/var/gitrepos/hoxsl.git
repo.desc=Higher-order logic for XSLT 2.0

repo.url=shspec
repo.path=/var/gitrepos/shspec.git
repo.desc=BDD framework for shell


section=languages / compilers

repo.url=tame
repo.name=TAME
repo.path=/var/gitrepos/tame.git
repo.desc=The Algebraic Metalanguage [employer project]

repo.url=liza-proguic
repo.path=/var/gitrepos/liza-proguic.git
repo.desc=Declarative DSL for Liza programs [employer project]

repo.url=literate-xsl
repo.path=/var/gitrepos/literate-xsl.git
repo.desc=Literate documentation weaver for XSLT 2.0

repo.url=ulambda
repo.path=/var/gitrepos/ulambda.git
repo.desc=[STALLED] Self-hosting Scheme compiling to JavaScript (very incomplete proof-of-concept)


section=misc

repo.url=night
repo.path=/var/gitrepos/night.git
repo.desc=Nighttime hacks: playful creativity as a form of relaxation

repo.url=git-shortmaps
repo.path=/var/gitrepos/git-shortmaps.git
repo.desc=Simple one--three-character bash aliases for Git with tab completion

repo.url=dotfiles
repo.path=/var/gitrepos/dotfiles.git
repo.desc=Personal system configuration and miscellaneous scripts

repo.url=promscripts
repo.path=/var/gitrepos/promscripts.git
repo.desc=Scripts for generating Prometheus metrics


section=talks

repo.url=afsi
repo.name=afsi
repo.path=/var/gitrepos/afsi.git
repo.desc=Adopting Free Software Ideals (LibrePlanet 2021)

repo.url=sapsf
repo.name=SAPSF
repo.path=/var/gitrepos/sapsf.git
repo.desc=The Surreptitious Assault on Privacy, Security, and Freedom (LibrePlanet 2017)

repo.url=cs4m
repo.name=cs4m
repo.path=/var/gitrepos/cs4m.git
repo.desc=Computational Symbiosis: Methods That Meld Mind and Machine (LibrePlanet 2019)

repo.url=ethics-void
repo.name=The Ethics Void
repo.path=/var/gitrepos/ethics-void.git
repo.desc=The Ethics Void (LibrePlanet 2018)

repo.url=online-freedom
repo.name=Restore Online Freedom!
repo.path=/var/gitrepos/online-freedom.git
repo.desc=Restore Online Freedom! (LibrePlanet 2016)


section=papers

repo.url=coope
repo.name=COOPE
repo.path=/var/gitrepos/coope.git
repo.desc=Classical Object-Oriented Programming with ECMAScript (2012)

repo.url=cptt
repo.name=cptt
repo.path=/var/gitrepos/cptt.git
repo.desc=Discussion on Compilers: Principles, Techniques, and Tools (2013)


section=websites

repo.url=repo2html
repo.path=/var/gitrepos/repo2html.git
repo.desc=Repository-agnostic HTML generator (used to generate mikegerwitz.com)

repo.url=thoughts
repo.path=/var/gitrepos/thoughts.git
repo.desc=Thoughts and Ramblings (mikegerwitz.com)



section=on-hold

repo.url=guile-mime
repo.path=/var/gitrepos/guile-mime.git
repo.desc=MIME library and globs2 parser for Guile

repo.url=lasertank-js
repo.path=/var/gitrepos/lasertank-js.git
repo.desc=Clone of the classic 1990s game LaserTank

repo.url=pkgsh
repo.path=/var/gitrepos/pkgsh.git
repo.desc=Library to aid in the factoring of large programs written in shell


section=archive

repo.url=epmanners
repo.path=/var/gitrepos/epmanners.git
repo.desc=Teach LaTeX some manners to respect \everypar

repo.url=gsgp
repo.path=/var/gitrepos/gsgp.git
repo.desc=GNU Screen Gaming Platform

repo.url=jstonic
repo.path=/var/gitrepos/jstonic.git
repo.desc=Miscellaneous library built upon GNU ease.js

repo.url=rectest
repo.path=/var/gitrepos/rectest.git
repo.desc=Browser-based recollection test

repo.url=lvspec
repo.path=/var/gitrepos/lvspec.git
repo.desc=LaTeX specification library for LoVullo Associates with a focus on Liza/TAME [employer project]

