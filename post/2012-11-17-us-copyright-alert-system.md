# U.S. "Copyright Alert System"

[The EFF warns][0] of [the "Copyright Alert System"][1]---a government
endorsed spy system---that will launched shortly to monitor peer-to-peer
networks for so-called "infringing" activity.

[0]: https://www.eff.org/deeplinks/2012/11/us-copyright-surveillance-machine-about-be-switched-on
[1]: http://www.copyrightinformation.org/alerts

<!-- more -->
