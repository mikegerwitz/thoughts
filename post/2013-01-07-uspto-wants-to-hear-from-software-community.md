# USPTO Wants To Hear From Software Community

The [USPTO wants to hear from the software community][0]. Interesting, but the
problem is that the "software community" includes more than just those who
find software patents to be an abomination.

I have [mentioned issues with software patents in a previous post][1], but one
resource that may be worth looking at direclty is ["The Case Against
Patents"][2] [pdf].

[0]: http://www.groklaw.net/article.php?story=20130104012214868
[1]: http://mikegerwitz.com/thoughts/2012/10/Abolishing-Patents.html
[2]: http://research.stlouisfed.org/wp/2012/2012-035.pdf

<!-- more -->
