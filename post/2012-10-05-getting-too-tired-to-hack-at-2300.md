# Getting too tired to hack? At 23:00?

This has been normal since becoming a father. I can't complain---I love being a
father. Of course, I also love hacking. I also love sleep. Knowing that my son
is going to wake me up a 6:00 in the morning has a slight influence in a
situation like this.

<!-- more -->

I'd like to just suffer through it, but being a fiancé also has another
obligation: going to bed when your significant other decides that it's bed time
(and by "bed time" I mean sleep). I still manage to fit it in somehow.
