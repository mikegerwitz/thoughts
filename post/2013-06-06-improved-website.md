# Improved Website

The old WordPress website has been replaced entirely by the "thoughts" site
(which was previously located at /thoughts). This website is generated from its
git repository---available on the Projects page---which is freely licensed.
There is some content that existed on the old site that is still useful; should
that content be transferred to this site, a redirect will be set up (assuming
that it hadn't already been lost to the search engines).

Since all this content is static, there is no discussion system. I am still
debating whether or not I will add this in the future. Until that time, feel
free to contact me via e-mail.

<!-- more -->
