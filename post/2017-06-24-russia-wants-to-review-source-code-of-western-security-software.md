# Russia wants to review source code of Western security software

Reuters [released an article][0] entitled "Under pressure, Western tech
  firms bow to Russian demands to share cyber secrets".
Should Russia be permitted to do so?
Should companies "bow" to these demands?

I want to draw a parallel to another highly controversial case regarding
  access to source code:
    the [Apple v. FBI][2] case early last year.
For those who don't recall,
  one of the concerns was the government trying to compel Apple to make
  changes to iOS to permit brute forcing the San Bernardino attacker's
  PIN;
    this is a [violation of First Amendment rights][3] (compelled speech),
      and this afforded Apple strong support from even communities that
      otherwise oppose them on nearly all other issues.
The alternative was to have the FBI make changes to the software instead of
  compelling Apple to do so,
    which would require access to the source code of iOS.

[0]: http://www.reuters.com/article/us-usa-russia-tech-insight-idUSKBN19E0XB
[2]: https://en.wikipedia.org/wiki/FBI%E2%80%93Apple_encryption_dispute

<!-- more -->

Becuase of the hostility toward the FBI in this case,
  even many in the [free software community][4] took the stance that the FBI
  being able to modify the software would set terrible precedent.
But that's missing the point a bit.
Being able to modify software doesn't give you the right to install it on
  others' devices;
    the FBI would have had to compell Apple to release their signing keys
    as well---_that_ is a dangerous precedent.
If the government compelled Apple to made changes themselves,
  _that_ is dangerous precedent.

"Cyber secrets" in the above title refers to source code to software written
  by companies like Cisco, IBM, SAP, and others;
    secrets that can only exist in proprietary software that
      [denies users the right to inspect, modify, and share][1] the software
      that they are running.

For those who agree with the free software philosophy,
  it's important to remove consideration of _who_ is trying to exercise their
  [four freedoms][1].
In the case of the FBI,
  from a free software perspective,
  of course they should be able to modify the software---we
    believe that _all_ software should be free!
      (But that doesn't mean they should be able to install it on _someone
      else's_ device.)
In the context of this article by Reuters:
  Russia doesn't have to ask to examine software that is free/libre.
  And if they did, it shouldn't be a concern;
    restricting who can use and examine software is [a slippery slope][5].

Unfortunately, not all software is free/libre.
But if we extend the free software philsophy---there
  should be no _ethical_ concerns with a foreign power wanting to inspect
  proprietary source code.
But proprietary software might have something of concern to hide:
  it might be something malicious like a backdoor,
  or it might be something like a lack of security or poor development
  practices;
    [proprietary software exists only to keep secrets][6], after all.

If Russia has to ask to inspect source code for security software,
  you probably do too.
And if that's the case,
  the security being provided to you is merely a facade.
It's not Russia to be suspicious of for asking---it's
  the companies that keep secrets to begin with.

[1]: https://www.gnu.org/philosophy/free-software-even-more-important.html
[3]: https://www.eff.org/deeplinks/2016/03/deep-dive-why-forcing-apple-write-and-sign-code-violates-first-amendment
[4]: https://www.gnu.org/philosophy/free-sw.en.html
[5]: https://www.gnu.org/philosophy/programs-must-not-limit-freedom.html
[6]: https://www.gnu.org/proprietary/proprietary.html
