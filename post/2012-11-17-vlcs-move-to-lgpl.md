# VLC's Move to LGPL

Jean-Baptiste Kempf of the VLC project explains that "most of the code of VLC"
has been [relicensed under the LGPL][0], moving *away from* the GPL. Some of the
reasons for the move include "competition, necessity to have more professional
developers around VLC and AppStores".[1] (With the "AppStore" comment,
Jean-Baptiste is likely referring to issues regarding free software in Apple's
App Store, which [the FSF has discussed on their website][2].)

This is unfortunate; using the LGPL in place of the GPL is [not encouraged for
free software projects][3] because, while it ensures the freedom of the project
itself, it does not encourage the development of free software that *uses* the
project---the LGPL allows linking with proprietary software. Let's explore the
aforementioned reasons in a bit more detail.

[0]: http://www.jbkempf.com/blog/post/2012/I-did-it
[1]: http://www.jbkempf.com/blog/post/2012/How-to-properly-relicense-a-large-open-source-project
[2]: http://www.fsf.org/news/blogs/licensing/more-about-the-app-store-gpl-enforcement
[3]: http://www.gnu.org/licenses/why-not-lgpl.html

<!-- more -->

Firstly, let us consider the issue of competition. In one of the [discussions on
Hacker News][4], I pointed out the distinction between "open source" and Free
Software:

  [...]
  It is important to understand the distinction between "open source" and "free
  software". Open source focuses on the benefits of "open" code and development
  and how it can create superior software. Free Software focuses on the ethical
  issues---while free software developers certainly want contributors, the
  emphasis is on the fact that the software respects your freedom and, for that,
  it's far superior to any other proprietary alternative; free software users
  constantly make sacrifices in functionality and usability, and we're okay with
  that.

  [http://www.gnu.org/philosophy/open-source-misses-the-point.html][5]
  [...]

In this sense, why should competition be considered for software freedom, unless
it is between two free software projects, encouraging innovation in conjunction
*with* freedom? In such a case, one wouldn't change the software license from
the GPL to the LGPL, because the LGPL is less pursuant toward those freedoms.
Therefore, VLC instead adopts the ["open source"][5] development model, as it
cares more for competition.

The next concern was to "have more professional developers around VLC".[1] Is
this to imply that free software hackers cannot be professional developers? I
certainly am. Consider projects like the kernel Linux---many companies have
contributed back to that project, which is licensed under the GPLv2. If the goal
is to have more people contributing to your project, then a license like the GPL
is certainly best, as it puts a legal obligation on the distributor to release
the source code, which the parent project may then incorporate. Now, the LGPL
also forces this (except for linked software); since the only [differences
between the GPL and the LGPL][6] deal with the linking exception, this means
that the author is either (a) mistaken in the concern or (b) wishes for more
*proprietary* development around VLC.  Alternatively, the author may be
concerned that the GPL introduces compatibility issues between whatever other
"open source" license developers wish to use when linking VLC code, but
again---that means that VLC is devaluing freedom. Risky business, but this is
the model that BSD follows (permitting proprietary derivatives of the entire
software---not just linking---and receiving contributions back from proprietary
software makers.)

Finally, let us consider the issue of Apple's App Store. This is issue is
certainly of strong concern---Apple's products are very popular and yet they do
not even make an attempt to respect the users' freedoms either with their
software or with any of the software they allow on their "App Store".[2]
However, Jean-Baptiste has made a fatal mistake---we should not be changing our
licenses to suit Apple! In effect, that is giving Apple even more power over
free software by allowing them to exert control not only over their users, but
also over the developers of the users' favorite software! We should instead
express our condolences with those users and suggest instead that they adopt a
device or operating system that respects their freedom, or that they jailbreak
their devices (which is [still legal][7]).

I'll end this commentary with an additional response of mine from the
[aforementioned Hacker News thread][4]:

> The freedoms represent an ethical issue---that software developers have
> unprecedented control over their users. Why should I, as a hacker, be able
> to tell you what you can and cannot do with your device? Furthermore, it
> raises deep privacy issues---what kind of data am I collecting and why
> should I have that data?
>
> I entered the free software movement slowly (I began software development on
> Windows as a young boy and was trained to think that bossing the user around
> was a good thing; I thought it was fun to write DRM system and
> anti-features). I began using GNU/Linux while still rationalizing my use of
> proprietary software through Wine or by dual-booting into Windows. I then
> saw the benefits of the "open source" development model. It wasn't until I
> spent the time researching the reasons behind the free software movement
> that things began to click. I was able to look back on everything I learned
> as a developer for Windows and see that I enjoyed the thought of controlling
> my users. I enjoyed the power I got from programming---programming was
> empowerment, and the only way to squeeze the money out of those unsuspecting
> users was to do it forcefully.
>
> People have fundamentally different philosophies when it comes to
> programming. Do all proprietary software developers do so out of greed? On
> some level, sure---they're not contributing that code so that others may
> benefit from it. But are they doing it for the purpose of controlling their
> users? Not necessarily, but they still are, even if they have the best of
> intentions. Is someone who creates proprietary educational software for
> children in third world companies "evil"? Certainly not. The problem is that
> they're denying them an additional right---the right to modify that
> software, learn from it and use their devices as they please.
>
> Of course, we often see proprietary software used unethically, often times
> for vendor lock-in or greed; corporations are worried that if they lighten
> their grip on their users, that the users may run, or worse, do something
> [il]legal. I don't believe that is the place of software developers. I
> remember, back when I used Windows, I was obsessed with magic/illusion. I
> purchased a ton of videos online teaching me various magic tricks, but the
> videos were laced with DRM (which, at the time, as a Windows developer, I
> applauded). The problem was, that I then upgraded my hardware. My videos no
> longer worked. I contacted them for a new key, and could view them again.
> Then I got a new PC. And now I use GNU/Linux. I can no longer watch those
> videos that I purchased because of this unnecessary, artificial restriction.
> Was I going to distribute those videos? No. Did that prevent others from
> stripping the restrictions and distributing it anyway? Certainly not. I was
> being punished for others' actions and the others weren't any worse off from
> the restrictions, because they understood how to defeat them.
>
> Of course, DRM's only one of the many issues (and DRM cannot exist in free
> software, because the community would simply remove the anti-feature). What
> if I were using some software---let's say Photoshop---and it crashed on me
> in the middle of my work. Crap. Well, if I were using GIMP, I would run gdb
> on the core dump (assuming a segfault) and inspect the problem. I would try
> to repeat it. I could, if I wanted to, get my hands on the source code, fix
> the problem and distribute that fix to others. If I didn't have the time or
> ability, others could fix the problem for me, and we have the right to share
> those changes. We have the right to benefit from those changes. With
> Photoshop, we'd better start waiting. What if I was able to magically come
> up with a fix, perhaps by modifying the machine code? Hold on---I'm not
> allowed to do that! And I'm certainly not allowed to distribute that fix to
> others. And I'm certainly not allowed to give my son a copy for his PC if he
> wanted to do an art project for school.
>
> The FSF provides a great deal of information on their philosophy:
> <http://www.gnu.org/philosophy/>. You could also gain a great deal of
> insight by reading up on the history:
> <http://shop.fsf.org/product/free-as-in-freedom-2/> or by reading RMS'
> essays: <http://shop.fsf.org/product/signed-fsfs/>.
>
> And ultimately, you may find that you do not agree with our
> philosophy---many don't. That's certainly your right, and I respect that.
> What I cannot respect, and will not respect, is when that philosophy is used
> to exert control over others.
>
> (As a final note: many say we control developers through our "viral"
> licenses. But keep in mind that we're trying to protect the users *from*
> developers. This means taking power away from developers. This is
> intentional.)

[4]: http://news.ycombinator.com/item?id=4787965
[5]: http://www.gnu.org/philosophy/open-source-misses-the-point.html
[6]: http://www.gnu.org/licenses/lgpl.html
[7]: https://www.eff.org/press/releases/eff-wins-renewal-smartphone-jailbreaking-rights-plus-new-legal-protections-video

