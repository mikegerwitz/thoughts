# GNU/kWindows

There has been a lot of talk lately about a most unique combination:
  [GNU][gnu]---the [fully free/libre][free-sw] operating system---and
  Microsoft Windows---the [freedom-denying, user-controlling,
  surveillance system][woe].
There has also been a great deal of misinformation.
I'd like to share my thoughts.

[gnu]: https://gnu.org/gnu/gnu.html
[free-sw]: https://gnu.org/philosophy/free-sw.html
[woe]: https://www.gnu.org/proprietary/malware-microsoft.en.html

<!-- more -->

Before we can discuss this subject,
  we need to clarify some terminology:
We have a [free/libre][free-sw] operating system called [GNU][gnu].
Usually, it's used with the kernel Linux, and is together called the
  [GNU/Linux (or GNU+Linux) operating system][gnulinux].
But that's not always the case.
For example, GNU can be run with its own kernel, [The GNU Hurd][hurd]
  (GNU/Hurd).
It might be run on a system with a BSD kernel (e.g. GNU/kFreeBSD).
But now, we have a situation where we're taking GNU/Linux, removing Linux,
  and adding in its place a Windows kernel.
This combination is referred to as GNU/kWindows (GNU with the Windows kernel
  added).[^kwindows]

GNU values users' freedoms.
Windows [does exactly the opposite][woe].

When users talk about the operating system "Linux", what they are referring
  to is the [GNU operating system][gnu] with the kernel Linux added.
If you are using the GNU operating system in some form, then many of the
  programs you are familiar with on the command line are GNU programs:
    `bash`, `(g)awk`, `grep`, `ls`, `cat`, `bc`, `tr`, `gcc`, `emacs`, and
    so on.
But GNU is a fully free/libre Unix replacement, [not just a collection of GNU
  programs][gnu].
Linux is the kernel that supports what the operating system is trying to do;
  it provides what are called system calls to direct the kernel to perform
  certain actions, like fork new processes or allocate memory.
This is an important distinction---not only is calling all of this software
  "Linux" incorrect, but it discredits the project that created a fully
  free/libre Unix replacement---[GNU][gnu].

This naming issue is so widespread that
  [most users would not recognize what GNU is][gnu-noheard], even if they
  are _using_ a [GNU/Linux][gnulinux] operating system.
I recently read an article that referred to GNU Bash as "Linux's Bash";
  this is simply a slap in the face to all the hackers that have for the
  past 26 years been writing what is one of today's most widely used
  shells on Unix-like systems (including on [Apple's][apple] proprietary
  Mac OSX), and all the other GNU hackers.

Microsoft and Canonical have apparently been working together to write a
  subsystem that translates Linux system calls into something Windows will
  understand---a compatibility layer.
So, software compiled to run on a system with the kernel Linux will work on
  Windows through system call translation.
Many articles are calling this "Linux on Windows".
This is a fallacy: the kernel Linux is not at all involved!
What we are witnessing is the [_GNU_ operating system][gnu] running with
  a Windows kernel _instead_ of Linux.

This is undoubtedly a technical advantage for Microsoft---Windows users want
  to do their computing in a superior environment that they might be
  familiar with on [GNU/Linux][gnulinux] or other Unix-like operating
  systems, like [Apple's][apple] freedom-denying Mac OSX.
But thinking about it like this is missing an essential concept:

When users talk about "Linux" as the name of the operating system, they
  avoid talking about [GNU][gnu].
And by avoiding mention of GNU,
  they are also avoiding discussion of the core principles upon which GNU is
  founded---the belief that all users deserve
  [software granting _four essential freedoms_][free-sw]:
    the freedom to use the program for any purpose;
    the freedom to study the program and modify it to suit your needs (or
      have someone do it on your behalf);
    the freedom to share the program with others;
    and the freedom to share your changes with others.
We call software that respects these four freedoms
  [_free/libre software_][free-sw].

Free software is absolutely essential:
  it ensures that _users_,
    who are the most vulnerable,
    are in control of their computing---not software developers or
    corporations.
Any program that denies users any one of their [four freedoms][free-sw] is
  _non-free_ (or _proprietary_)---that is, freedom-denying software.
This means that any non-free software, no matter its features or
  performance, will [_always_ be inferior to free software][oss] that
  performs a similar task.

Not everyone likes talking about freedom or the
  [free software philosophy][free-sw].
This disagreement resulted in the
  ["open source" development methodology][oss],
  which exists to sell the benefits of free software to businesses *without*
  discussing the essential ideological considerations.
Under the "open source" philosophy,
  if a non-free program provides better features or performance,
  then surely it must be "better",
  because they have outperformed the "open source" development methodology;
    non-free software isn't always considered to be a bad thing.

So why would users want to use GNU/kWindows?
Well, probably for the same reason that they want GNU tools on Mac OSX:
  they want to use software they want to use, but they also want the
  technical benefits of GNU that they like.
What we have here is the ["open source" philosophy][oss]---because if the
  user truly valued her freedom, she would use a
  [fully free operating system like GNU/Linux][gnulinux-distros].
If a user is _already_ using Windows (that is, before considering
  GNU/kWindows), then she does gain some freedom by installing GNU:
    she has more software on her system that respects her freedoms,
    and she is better off because of that.

But what if you're using GNU/Linux today?
In that case,
  it is a major downgrade to switch to a GNU/kWindows system;
    by doing so, you are [surrendering your freedom to Microsoft][woe].
It does not matter how many shiny features Microsoft might introduce into
  its [freedom-denying surveillance system][woe];
    an [operating system that respects your freedoms][gnulinux-distros] will
    _always_ be a superior choice.
We would do our best to dissuade users from switching to a GNU/kWindows
  system for the technical benefits that GNU provides.

So we have a couple different issues---some factual, some philosophical:

Firstly,
  please don't refer to GNU/kWindows as "Linux on Windows", or any variant
  thereof;
    doing so simply propagates misinformation that not only confounds the
    situation, but discredits the thousands of hackers working on the
    [GNU operating system][gnu].
It would also be best if you avoid calling it "Ubuntu on Windows";
  it isn't a factually incorrect statement---you are running Ubuntu's
  distribution of GNU---but it still avoids mentioning the
  [GNU Project][gnu].  If you want to give Ubuntu credit for working with
  Microsoft, please call it "Ubuntu GNU/kWindows" instead of "Ubuntu".
By mentioning GNU,
  users will ask questions about the project,
  and might look it up on their own.
They will read about [the free software philosophy][free-sw],
  and will hopefully begin to understand these issues---issues that they
  might not have even been aware of to begin with.

Secondly,
  when you see someone using a GNU/kWindows system,
  politely ask them why.
Tell them that there is a _better_ operating system out there---the
  [GNU/Linux operating system][gnu]---that not only provides those technical
  features,
  but also provides the feature of _freedom_!
Tell them what [free software][free-sw] is,
  and try to relate it to them so that they understand why it is important,
  and even practical.

It's good to see more people benefiting from GNU;
  but we can't be happy when it is being sold as a means to draw users into
    an otherwise [proprietary surveillance system][woe],
    without so much as a mention of our name,
    or [what it is that we stand for][gnu].

[^kwindows]: This name comes from [Richard Stallman][rms], founder of the
             [GNU Project][gnu].

[hurd]: https://gnu.org/software/hurd/
[oss]: http://www.gnu.org/philosophy/open-source-misses-the-point.html
[gnulinux]: https://www.gnu.org/gnu/linux-and-gnu.html
[gnulinux-distros]: https://www.gnu.org/distros/free-distros.html
[apple]: https://stallman.org/apple.html
[rms]: https://www.fsf.org/about/staff-and-board
[gnu-noheard]: https://gnu.org/gnu/gnu-users-never-heard-of-gnu.html

---
featured: true
---
