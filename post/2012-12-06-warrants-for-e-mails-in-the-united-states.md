# Warrants For E-mails in the United States

The [Senate Judiciary Committee passed an amendment][0] that requires that they
receive a warrant before spying on our e-mails.

This is excellent; let us hope that it becomes law.

[0]: https://www.eff.org/deeplinks/2012/12/deep-dive-updating-electronic-communications-privacy-act

<!-- more -->
